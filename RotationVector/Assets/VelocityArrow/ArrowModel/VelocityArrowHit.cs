﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IVelocityArrowHit
{
    void VelocityArrowHit(RaycastHit hit);
}

[RequireComponent(typeof(ArrowDrawBase))]
public class VelocityArrowHit : MonoBehaviour
{
    static int _velocityArrowLayer = -1;
    public static int VelocityArrowLayer
    {
        get
        {
            if (_velocityArrowLayer < 0)
                _velocityArrowLayer = LayerMask.NameToLayer("VelocityArrow");
            if (_velocityArrowLayer < 0)
                Debug.LogError("please add a layer : VelocityArrow");
            return _velocityArrowLayer;
        }
    }

    ArrowDrawBase _arrawBase;
    void Start()
    {
        _arrawBase = GetComponent<ArrowDrawBase>();
    }

    void Update()
    {
        float dis = _arrawBase.GetArrowLength();
        Ray ray = new Ray(transform.position, _arrawBase.GetArrowDir());
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, dis, VelocityArrowLayer))
        {
            IVelocityArrowHit arrowHit = hitInfo.collider.GetComponent<IVelocityArrowHit>();
            if (arrowHit != null)
            {
                arrowHit.VelocityArrowHit(hitInfo);
            }
        }
    }
}

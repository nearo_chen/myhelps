﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowDrawBase : MonoBehaviour
{
    public Transform ArrowModel;
    [SerializeField] Transform arrowHead;

    public void SetArrow(Vector3 direction, float scale)
    {
        ArrowModel.transform.localRotation = Quaternion.LookRotation(direction);
        ArrowModel.transform.localScale = new Vector3(1, 1, scale);
    }

    public float GetArrowLength()
    {
        return (arrowHead.position - ArrowModel.position).magnitude;
    }

    public Vector3 GetArrowDir()
    {
        return ArrowModel.transform.forward;
    }

    void Start()
    {

    }

    void Update()
    {

    }
}

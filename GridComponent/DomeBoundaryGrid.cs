﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "DomeBoundaryGrid.cs"
 * @desc    draw dome boundary with line.
 * @author  Nearo Chen
 * @history 2016/03/17
 *
 */

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DomeBoundaryGrid : MonoBehaviour
{
    public int slice = 8;
    public int sliceY = 2;
    public float startX = 0;
    public float startY = 0;
    public float startZ = 0;
    public float radius = 1;
    public Color mainColor = new Color(0f, 1f, 0f, 1f);

    //// I use minStep to prevent 'step=0' will cause infinity forloop.
    const float MinStep = 0.1f;
    static Material lineMaterial = null;

    public static void DrawGrid(Color color, int slice, int sliceY, float startX, float startY, float startZ, float radius)
    {
        DrawGrid(Matrix4x4.identity, color, slice, sliceY, startX, startY, startZ, radius);
    }

    public static void DrawGrid(Matrix4x4 matrix, Color color, int slice, int sliceY, float startX, float startY, float startZ, float radius)
    {
        CreateLineMaterial();
        //// set the current material
        lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);
        GL.Color(color);

        Vector3 posStart, posEnd;

        ////y loop
        for (int y = 0; y < sliceY; y++)
        {
            float ratioY = y / (float)sliceY * Mathf.PI * 0.5f;

            ////draw circle
            for (float a = 0; a < slice; a++)
            {
                float ratio = a / (float)slice * Mathf.PI * 2f;
                posStart = new Vector3(
                    startX + Mathf.Sin(ratio) * Mathf.Cos(ratioY) * radius,
                    startY + Mathf.Sin(ratioY) * radius,
                    startZ + Mathf.Cos(ratio) * Mathf.Cos(ratioY) * radius);
                posStart = matrix.MultiplyPoint(posStart);
                GL.Vertex3(posStart.x, posStart.y, posStart.z);

                ratio = (a + 1f) / (float)slice * Mathf.PI * 2f;
                posEnd = new Vector3(
                    startX + Mathf.Sin(ratio) * Mathf.Cos(ratioY) * radius,
                    startY + Mathf.Sin(ratioY) * radius,
                    startZ + Mathf.Cos(ratio) * Mathf.Cos(ratioY) * radius);
                posEnd = matrix.MultiplyPoint(posEnd);
                GL.Vertex3(posEnd.x, posEnd.y, posEnd.z);

                ////draw y line
                GL.Vertex3(posStart.x, posStart.y, posStart.z);

                ratio = a / (float)slice * Mathf.PI * 2f;
                float nextRatioY = (y + 1) / (float)sliceY * Mathf.PI * 0.5f;
                posEnd = new Vector3(
                    startX + Mathf.Sin(ratio) * Mathf.Cos(nextRatioY) * radius,
                    startY + Mathf.Sin(nextRatioY) * radius,
                    startZ + Mathf.Cos(ratio) * Mathf.Cos(nextRatioY) * radius);
                posEnd = matrix.MultiplyPoint(posEnd);
                GL.Vertex3(posEnd.x, posEnd.y, posEnd.z);
            }
        }
        GL.End();
    }

    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            //// Unity has a built-in shader that is useful for drawing
            //// simple colored things.
            var shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            //// Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            //// Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            //// Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    void OnRenderObject()
    {
        Matrix4x4 mat = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        DrawGrid(mat, mainColor, slice, sliceY, startX, startY, startZ, radius);
    }
}

﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "PyramidBoundaryGrid.cs"
 * @desc    draw pyramid boundary with line.
 * @author  Nearo Chen
 * @history 2016/03/17
 *
 */

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PyramidBoundaryGrid : MonoBehaviour
{
    public int slice = 8;
    public int sliceY = 2;
    public float startX = 0;
    public float startY = 0;
    public float startZ = 0;
    public float radius = 1;
    public float height = 1;
    public Color mainColor = new Color(0f, 1f, 0f, 1f);

    // I use minStep to prevent 'step=0' will cause infinity forloop.
    const float MinStep = 0.1f;
    static Material lineMaterial = null;

    public static void DrawGrid(Matrix4x4 matrix, Color color, int slice, int sliceY, float startX, float startY, float startZ, float radius, float height)
    {
        CreateLineMaterial();
        //// set the current material
        lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);
        GL.Color(color);

        Vector3 pos;

        ////y loop
        for (int y = 0; y <= sliceY; y++)
        {
            float currentY = height * ((float)y / (float)sliceY) + startY;
            float currentRadius = radius * Mathf.Lerp(1, 0, (float)y / (float)sliceY);

            ////draw circle
            for (float a = 0; a < slice; a++)
            {
                float ratio = a / (float)slice * Mathf.PI * 2f;
                pos = new Vector3(startX + Mathf.Sin(ratio) * currentRadius, currentY, startZ + Mathf.Cos(ratio) * currentRadius);
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);

                ratio = (a + 1f) / (float)slice * Mathf.PI * 2f;
                pos = new Vector3(startX + Mathf.Sin(ratio) * currentRadius, currentY, startZ + Mathf.Cos(ratio) * currentRadius);
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);
            }

            if (y < sliceY)
            {
                ////draw y line
                for (float a = 0; a < slice; a++)
                {
                    float ratio = a / (float)slice * Mathf.PI * 2f;
                    pos = new Vector3(startX + Mathf.Sin(ratio) * currentRadius, currentY, startZ + Mathf.Cos(ratio) * currentRadius);
                    pos = matrix.MultiplyPoint(pos);
                    GL.Vertex3(pos.x, pos.y, pos.z);

                    float nextRadius = radius * Mathf.Lerp(1, 0, (float)(y + 1f) / (float)sliceY);
                    float step = height / (float)sliceY;
                    pos = new Vector3(startX + Mathf.Sin(ratio) * nextRadius, currentY + step, startZ + Mathf.Cos(ratio) * nextRadius);
                    pos = matrix.MultiplyPoint(pos);
                    GL.Vertex3(pos.x, pos.y, pos.z);
                }
            }
        }
        GL.End();
    }

    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            //// Unity has a built-in shader that is useful for drawing
            //// simple colored things.
            var shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            //// Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            //// Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            //// Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    void OnRenderObject()
    {
        Matrix4x4 mat = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        DrawGrid(mat, mainColor, slice, sliceY, startX, startY, startZ, radius, height);
    }
}

﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "BoxBoundaryGrid.cs"
 * @desc    draw box boundary with line.
 * @author  Nearo Chen
 * @history 2016/03/17
 *
 */

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BoxBoundaryGrid : MonoBehaviour
{
    //// modify from :
    //// http://answers.unity3d.com/questions/482128/draw-grid-lines-in-game-view.html    

    public float step = 0.2f;
    public float startX = 0;
    public float startY = 0;
    public float startZ = 0;
    public float gridSizeX = 1;
    public float gridSizeY = 0.05f;
    public float gridSizeZ = 1;
    public Color mainColor = new Color(0f, 1f, 0f, 1f);

    // I use minStep to prevent 'step=0' will cause infinity forloop.
    const float MinStep = 0.1f;
    static Material lineMaterial = null;
    
    public static void DrawGrid(Color color, float step, float startX, float startY, float startZ, float gridSizeX, float gridSizeY, float gridSizeZ)
    {
        DrawGrid(Matrix4x4.identity, color, step, startX, startY, startZ, gridSizeX, gridSizeY, gridSizeZ);
    }

    public static void DrawGrid(Matrix4x4 matrix, Color color, float step, float startX, float startY, float startZ, float gridSizeX, float gridSizeY, float gridSizeZ)
    {
        CreateLineMaterial();
        //// set the current material
        lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);
        GL.Color(color);

        Vector3 halfGrid = new Vector3(gridSizeX * 0.5f, gridSizeY * 0.5f, gridSizeZ * 0.5f);
        Vector3 pos;
        float curStep = Mathf.Max(step, MinStep);

        //// y loop
        for (float y = 0; y <= gridSizeY; y += curStep)
        {
            float currentY = y + startY;

            //// draw x line
            for (float z = 0; z <= gridSizeZ; z += curStep)
            {
                float currentSizeX = startX + gridSizeX;
                float currentZ = z + startZ;
                pos = new Vector3(startX, currentY, currentZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);

                pos = new Vector3(currentSizeX, currentY, currentZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);
            }

            // draw z line            
            for (float x = 0; x <= gridSizeX; x += curStep)
            {
                float currentSizeZ = startZ + gridSizeZ;
                float currentX = x + startX;
                pos = new Vector3(currentX, currentY, startZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);

                pos = new Vector3(currentX, currentY, currentSizeZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);
            }
        }

        //// draw Y line
        for (float z = 0; z <= gridSizeZ; z += curStep)
        {
            for (float x = 0; x <= gridSizeX; x += curStep)
            {
                float currentZ = z + startZ;
                float currentX = x + startX;
                float currentSizeY = startY + gridSizeY;

                pos = new Vector3(currentX, startY, currentZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);

                pos = new Vector3(currentX, currentSizeY, currentZ);
                pos -= halfGrid;
                pos = matrix.MultiplyPoint(pos);
                GL.Vertex3(pos.x, pos.y, pos.z);
            }
        }
        GL.End();
    }

    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            var shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            //// Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            //// Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            //// Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    void OnRenderObject()
    {
        Matrix4x4 mat = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        DrawGrid(mat, mainColor, step, startX, startY, startZ, gridSizeX, gridSizeY, gridSizeZ);
    }
}
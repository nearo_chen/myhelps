const byte LED = 13;
char val;

void setup() 
{
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
  Serial.println("Welcon to Arduino!");
}

void loop() 
{
  if(Serial.available())
  {
    val = Serial.read();
    switch(val)
    {
      case '1':
      digitalWrite(LED, HIGH);
      Serial.println("LED ON");
      break;
      case '2':
      digitalWrite(LED, LOW);
      Serial.println("LED OFF");
      break;
    }
  }
}

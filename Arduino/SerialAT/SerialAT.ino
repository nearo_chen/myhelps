#include <SoftwareSerial.h>
SoftwareSerial BT(12, 11);//(藍芽的傳送，藍芽的接收)
char val;

void setup() 
{
  Serial.begin(9600);
  Serial.println("Welcon to Arduino AT!");
  BT.begin(9600);  
}

void loop() 
{
  if(Serial.available())
  {
    val = Serial.read();
    BT.print(val);
  }
  
  if(BT.available())
  {
    val = BT.read();
    Serial.print(val);
  }
}

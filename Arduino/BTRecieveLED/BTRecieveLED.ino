#include <SoftwareSerial.h>
SoftwareSerial BT(12, 11);//(藍芽的傳送，藍芽的接收)

char val;

void setup() 
{
  pinMode(LED_BUILTIN, OUTPUT);
  BT.begin(9600);
  BT.println("Welcon to Arduino BT!");
}

void loop() 
{
  if(BT.available())
  {
    val = BT.read();
    switch(val)
    {
      case '1':
      digitalWrite(LED_BUILTIN, HIGH);
      BT.println("LED ON");
      break;
      case '0':
      digitalWrite(LED_BUILTIN, LOW);
      BT.println("LED OFF");
      break;
    }
  }
}

#include <SoftwareSerial.h>
SoftwareSerial BT(12, 11);//(藍芽的傳送，藍芽的接收)
byte relayPin = 2;//繼電器
char val;
bool pairSuccess=false;

void setup() 
{
  pinMode(relayPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  BT.begin(9600);
  BT.println("Welcon to Arduino BT!");

  digitalWrite(relayPin, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);//設定為常開
}

String s = "";
void loop() 
{
  if(BT.available())
  {
    val = BT.read();
    switch(val)
    {
      case '1':      
      digitalWrite(relayPin, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
      BT.println("LED ON");
      break;
      case '0':
      digitalWrite(relayPin, LOW);
      digitalWrite(LED_BUILTIN, LOW);
      BT.println("LED OFF");
      break; 
      case 'a':
        if(!pairSuccess)
        {
          BT.println("ArduinoGunArduinoGunArduinoGun");//對應C#端的辨識字串IdentifyCode(有時候第一次傳送字串會少字元所以打3次)
        }
      break;
      case 'b':
        if(!pairSuccess)
        {
          BT.println("ArduinoGun Paired Success");//一定要回傳Paired Success，才能確定連線成功
          pairSuccess = true;
        }
      break;
      case 'c':
        if(pairSuccess)
        {
          pairSuccess = false;
        }
      break;
    }
  }
}

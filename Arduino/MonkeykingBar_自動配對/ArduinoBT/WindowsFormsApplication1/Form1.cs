﻿using MyArduino;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        ArduinoTest arduinoTest = new ArduinoTest();

        public Form1()
        {
            InitializeComponent();
        }

        private void Auto_Click(object sender, EventArgs e)
        {
            arduinoTest.AutoInit(ArduinoTest.IdentifyCode);
        }

        private void Close_Click(object sender, EventArgs e)
        {
            arduinoTest.Dispose();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            arduinoTest.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            arduinoTest.Update();
        }

        private void Switch_Click(object sender, EventArgs e)
        {
            if(Switch.Text =="On")
            {
                arduinoTest.Send("1");
                Switch.Text = "Off";
            }
            else
            {
                arduinoTest.Send("0");
                Switch.Text = "On";
            }
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            arduinoTest.InitPort(MyArduino.ArduinoBT.COMPORT.COM6);
        }

        private void CopyDLL_Click(object sender, EventArgs e)
        {

        }
    }
}

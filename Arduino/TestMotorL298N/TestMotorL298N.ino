/*Motor*/
int ENB = 9;
int IN3 = 8;
int IN4 = 7;
int ENA = 10;
int IN1 = 12;
int IN2 = 11;

void Motor_Init()
{
  pinMode(ENB, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
}

void Motor_ENB(bool isOn)
{
  if(isOn)
  {
    analogWrite(ENB, 255);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }
  else
  {
    analogWrite(ENB, LOW);
  }
}

void Motor_ENA(bool isOn)
{
  if(isOn)
  {
    analogWrite(ENA, 255);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  }
  else
  {
    analogWrite(ENA, LOW);
  }
}

void setup()
{
  Motor_Init();
}

void loop() {
Motor_ENA(true);
Motor_ENB(true);
}

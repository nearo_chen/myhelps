﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "TronBrickEmmBeats.cs"
 * @desc    A component on brick, which can control emmisive by BeatsVisualizer's beat data.
 * @author  nearo chen
 * @history 2016/03/03
 *
 */

using UnityEngine;
using System.Collections;

public class TronBrickEmmBeats : MonoBehaviour
{
    int beatsLevel = -1;
    Renderer _renderer;
    Color baseColor;

    float minPower;
    float maxPower;

    float currentPower;
    float fallofSpeed;

    public void Init(int beatsLevel, float falloffSpeed, float min, float max)
    {
        this.beatsLevel = beatsLevel;
        fallofSpeed = falloffSpeed;
        minPower = min;
        maxPower = max;
        MusicBeatsManager.Instance.AddBeatCallback(BeatCallback);
    }

    // Use this for initialization
    void Start()
    {
        _renderer = GetComponent<Renderer>();
        baseColor = _renderer.material.GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update()
    {
        if (!MusicBeatsManager.Instance.IsStart() || beatsLevel < 0)
            return;

        //if (MusicBeatsManager.Instance.IsBeats(beatsLevel))
        //{
        //    currentPower = maxPower;
        //}
        //else
        {
            currentPower = Mathf.Lerp(currentPower, minPower, Time.deltaTime * fallofSpeed);
        }

        Color finalColor = baseColor * currentPower;
        _renderer.material.SetColor("_EmissionColor", finalColor);

        transform.localScale = Vector3.one + Vector3.up * currentPower;
    }

    public void BeatCallback(float timeSec, bool[] isBeatsOnLevel)
    {
        if (isBeatsOnLevel[beatsLevel])
            currentPower = maxPower;
    }
}

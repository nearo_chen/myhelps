﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "FloatWindow.cs"
 * @desc    the spectrum compute unit.
 * @author  nearo chen
 * @history 2016/03/03
 *
 */

using UnityEngine;
using System.Collections;

class FloatWindow
{
    private float[] floats;
    int index = -1;
    int size = 0;
    int count = 0;

    public FloatWindow(int size)
    {
        this.size = size;
        floats = new float[size];
    }

    public float average()
    {
        float sum = 0;
        for (int index = 0; index < size && index < count; index++)
        {
            sum += floats[index];
        }
        if (size < count)
        {
            return sum / size;
        }
        else
        {
            return sum / count;
        }
    }

    public void add(float value)
    {
        index++;
        if (index >= size)
        {
            index = 0;
        }
        floats[index] = value;
        count++;
    }

    public float getPrevious(int offset)
    {
        if (offset > size)
        {
            //// throw new IllegalArgumentException("offset must be <= size");
            Debug.LogError("[BeatsDetection][FloatWindow] : offset must be <= size");
        }
        int floatIndex = index - offset;
        if (floatIndex < 0)
        {
            floatIndex += size;
        }
        return floats[floatIndex];
    }

    public float getMiddle()
    {
        return getPrevious((size + 1) / 2);
    }
}

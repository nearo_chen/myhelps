﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "BeatsDetection.cs"
 * @desc    management the BeatsDetector
 * @author  nearo chen
 * @history 2016/03/03
 *
 */

using UnityEngine;
using System.Collections.Generic;

public class BeatsDection
{
    int sampleRate;

    // spectrum
    // private int mCaptureSizeRange = 1024;//must be power of 2( 128 , 256 , 512, 1024)
    // protected int mObserveSpectrumNum = 128; //must not over half size of mCaptureSizeRange, because of the FFT -> spectrum (we catch 2kHz range = mCaptureSizeRange/4)
    // private float[] model = null;
    int spectrumSize;
    private float[] previousSpectrum = null;
    private List<BeatDetector> beatDetectorsList = new List<BeatDetector>();

    private bool[] beatsOutput;

    public BeatsDection(int spectrumSize, int sampleRate)
    {
        this.spectrumSize = spectrumSize;
        this.sampleRate = sampleRate;
    }

    public void AddBeatDetector(int startHz, int endHz, float decayThreshold)
    {
        // mCaptureSizeRange is 441000Hz
        int lowIndex = freqToIndex(startHz, spectrumSize, sampleRate);
        int highIndex = freqToIndex(endHz, spectrumSize, sampleRate);

        BeatDetector beatDetector = new BeatDetector(lowIndex, highIndex, decayThreshold);
        beatDetectorsList.Add(beatDetector);
        
        Debug.Log("[BeatsDection][AddBeatDetector] : lowIndex : " + lowIndex + " , highIndex : " + highIndex + " , sampleRate : " + sampleRate);
    }

    //// ***********************************************************************************************************************************************
    //// Detect beats from spectrum
    //// ***********************************************************************************************************************************************
    public void ProcessBeats(float[] spectrum)
    {
        if (previousSpectrum == null)
        {
            previousSpectrum = new float[spectrum.Length];
            System.Array.Copy(spectrum, 0, previousSpectrum, 0, spectrum.Length);
        }

        foreach (BeatDetector beatector in beatDetectorsList)
        {
            beatector.detect(previousSpectrum, spectrum);
        }

        System.Array.Copy(spectrum, 0, previousSpectrum, 0, spectrum.Length);

        // process output
        if (beatsOutput == null)
        {
            beatsOutput = new bool[beatDetectorsList.Count];
        }
        int count = 0;
        foreach (BeatDetector beatector in beatDetectorsList)
        {
            if (!beatsOutput[count])//因為可能外部會有一個frame就計算好幾次beat，所以會被洗掉，false在考慮是否為beat，然後在IsBeat那邊取得後改為false
                beatsOutput[count] = beatector.isBeat() && beatector.isBigBeat();
            count++;
        }
    }
    //// ***********************************************************************************************************************************************

    public int GetDetectorsAmount()
    {
        return beatDetectorsList.Count;
    }

    public bool IsBeats(int detectorID)
    {
        if (beatsOutput == null)
            return false;
        if(beatsOutput[detectorID])
        {
            beatsOutput[detectorID] = false;
            return true;
        }
        return false;
    }

    private static int freqToIndex(int hz, int spectrum_size, int sampleRate)
    {
        // Debug.Log("[BeatsDection] [AudioSettings.outputSampleRate] : " + sampleRate);

        // float index = (float)Hz * (float)(mCaptureSizeRange / 2) / (float)(AudioSettings.outputSampleRate / 1000);
        float index = (float)hz * (float)spectrum_size / (float)sampleRate;
        return Mathf.FloorToInt(index + 0.5f);
    }
}

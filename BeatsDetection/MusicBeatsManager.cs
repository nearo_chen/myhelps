﻿#define USE_NAUDIO

/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "MusicBeatsManager.cs"
 * @desc    processing the beats data to visualization component.
 * @author  nearo chen
 * @history 2016/03/03
 *
 */

using UnityEngine;
using System.Collections;
using MusicPlayer;


public class MusicBeatsManager : MySingleton<MusicBeatsManager>
{
#if USE_NAUDIO
    MP3Player musicSource;
#else
    AudioSource musicSource;
    const int SPECTRUMSIZE = 512;
    float[] spectrum;
#endif

    BeatsDection beatsDetection;
    bool[] beatsData;

#if USE_NAUDIO
    public void StartDetect(MusicPlayer.MP3Player source)
    {
        musicSource = source;
        beatsDetection = new BeatsDection(MP3Player.SPECTRUMSIZE, musicSource.SampleRate);
        musicSource.AddFFTCalculatedCallback(FFTCalculatedCallback);
        _initBeatDetector();
        Debug.Log("[MusicBeatsManager] StartDetect USE_NAUDIO");
    }
#else
    public void StartDetect(AudioSource source)
    {
        beatsDetection = new BeatsDection(SPECTRUMSIZE, AudioSettings.outputSampleRate);
        spectrum = new float[SPECTRUMSIZE];
        _initBeatDetector();
        Debug.Log("[MusicBeatsManager] StartDetect Use Unity AudioSource");
    }
#endif

    public void StopDetect()
    {
        musicSource = null;
        beatsDetection = null;

        Debug.Log("[MusicBeatsManager] StopDetect");
    }

    public int GetDetectorsAmount()
    {
        return beatsDetection.GetDetectorsAmount();
    }

    //public bool IsBeats(int detectorID)
    //{
    //    return beatsDetection.IsBeats(detectorID);
    //}

    public bool IsStart()
    {
        return musicSource != null;
    }

    void _initBeatDetector()
    {
        Debug.Log("[MusicBeatsManager][_initBeatDetector]");
        //// base on 'http://www.drumnet.tw/sound6.htm#20'
        //// 樂器與人聲的頻率範圍: 'http://www.uuuwell.com/mytag.php?id=60574'
        //// 音頻頻率範圍
        //// 低頻段（30—150HZ）；
        //// 中低頻（150—500HZ）；
        //// 中高頻段（500—5000HZ）；
        //// 高頻段（5000—20kHZ）。

        float decay = 0.02f;

        //// We create 5 level for 5 materials
        //beatsDetection.AddBeatDetector(0, 150, decay);
        //beatsDetection.AddBeatDetector(150, 500, decay);
        //beatsDetection.AddBeatDetector(500, 1000, decay);
        //beatsDetection.AddBeatDetector(1000, 2000, decay);
        //beatsDetection.AddBeatDetector(2000, 4000, 0.002f);

        //beatsDetection.AddBeatDetector(0, 150, decay);
        //beatsDetection.AddBeatDetector(150, 300, decay);
        //beatsDetection.AddBeatDetector(300, 450, decay);
        //beatsDetection.AddBeatDetector(450, 600, decay);
        //beatsDetection.AddBeatDetector(600, 750, decay);
        //beatsDetection.AddBeatDetector(750, 900, decay);
        //beatsDetection.AddBeatDetector(900, 1050, decay);
        //beatsDetection.AddBeatDetector(1050, 1200, decay);
        //beatsDetection.AddBeatDetector(1200, 1350, decay);
        //beatsDetection.AddBeatDetector(1350, 1500, decay);
        //beatsDetection.AddBeatDetector(1500, 2000, decay);
        //beatsDetection.AddBeatDetector(2000, 3000, decay);
        //beatsDetection.AddBeatDetector(3000, 5000, decay);
        //beatsDetection.AddBeatDetector(5000, 10000, decay);
        //beatsDetection.AddBeatDetector(10000, 20000, decay);
        //beatsDetection.AddBeatDetector(20000, 30000, decay);


        //beatsDetection.AddBeatDetector(0, 128, decay);
        //beatsDetection.AddBeatDetector(128, 256, decay);
        //beatsDetection.AddBeatDetector(256, 512, decay);
        //beatsDetection.AddBeatDetector(512, 1024, decay);
        //beatsDetection.AddBeatDetector(1024, 2048, decay);
        //beatsDetection.AddBeatDetector(2048, 4096, decay);
        //beatsDetection.AddBeatDetector(4096, 8192, decay);
        //beatsDetection.AddBeatDetector(8192, 16000, decay);
        //beatsDetection.AddBeatDetector(16000, 32000, decay);

        beatsDetection.AddBeatDetector(0, 256, decay);
        //beatsDetection.AddBeatDetector(6000, 12000, decay);
    }

#if USE_NAUDIO
    void FFTCalculatedCallback(float[] spectrum)
    {
        if (!IsStart())
            return;
        beatsDetection.ProcessBeats(spectrum);

        //設定回呼 
        if (_isBeats == null)
            _isBeats = new bool[GetDetectorsAmount()];
        for (int a = 0; a < GetDetectorsAmount(); a++)
            _isBeats[a] = beatsDetection.IsBeats(a);
        if (_beatCallback != null)
            _beatCallback((float)musicSource.CurrentTime.TotalSeconds, _isBeats);
    }
#else
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsStart())
            return;

        musicSource.GetSpectrumData(spectrum, 0, FFTWindow.Hamming);
        beatsDetection.ProcessBeats(spectrum);
    }
#endif

    bool[] _isBeats;
    public delegate void BeatCallback(float timeSec, bool[] isBeatsOnLevel);
    BeatCallback _beatCallback;

    /// <summary>
    /// 註冊接收beat觸發
    /// </summary>
    public void AddBeatCallback(BeatCallback callback)
    {
        _beatCallback += callback;
    }
}

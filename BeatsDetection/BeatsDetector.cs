﻿/**
 *
 * HTC Corporation Proprietary Rights Acknowledgment
 * Copyright (c) 2016 HTC Corporation
 * All Rights Reserved.
 *
 * The information contained in this work is the exclusive property of HTC Corporation
 * ("HTC").  Only the user who is legally authorized by HTC ("Authorized User") has
 * right to employ this work within the scope of this statement.  Nevertheless, the
 * Authorized User shall not use this work for any purpose other than the purpose
 * agreed by HTC.  Any and all addition or modification to this work shall be
 * unconditionally granted back to HTC and such addition or modification shall be
 * solely owned by HTC.  No right is granted under this statement, including but not
 * limited to, distribution, reproduction, and transmission, except as otherwise
 * provided in this statement.  Any other usage of this work shall be subject to the
 * further written consent of HTC.
 *
 * @file    "BeatDetector.cs"
 * @desc    processing the spectrum, detect spectrum suddenly change big, then we have a beat.
 * @author  nearo chen
 * @history 2016/03/03
 *
 */

using UnityEngine;
using System.Collections;

public class BeatDetector
{
    //// BeatsHistory mBeatsHistory = new BeatsHistory();
    private static int bufferSize = 7;
    private int highIndex;
    private int lowIndex;
    private float maxFlux;
    private FloatWindow fluxWindow;
    private FloatWindow peakWindow;
    private float peakAverage;
    private int iteration = 0;
    private int lastPeakIteration;
    private float peakDecayPerIteration;
    private float peakSaveLevel;
    private float peakReportLevel;
    private IBeatDetectorListener beatDetectorListener;
    private float threshold;
    private float flux;
    private float peak;
    private bool savePeak;
    private bool reportPeak;

    public BeatDetector(int lowIndex, int highIndex, float decayThreshold)
    {
        this.lowIndex = lowIndex;
        this.highIndex = highIndex;
        fluxWindow = new FloatWindow(bufferSize);
        peakWindow = new FloatWindow(bufferSize);
        peakAverage = 0f;
        peakDecayPerIteration = decayThreshold;
        peakSaveLevel = 0.75f;
        peakReportLevel = 0.25f;
    }

    public interface IBeatDetectorListener
    {
        void onBeatDetected(BeatDetector beatDetector, float flux, float peak, bool big);
    }

    public int getHighIndex()
    {
        return highIndex;
    }

    public int getLowIndex()
    {
        return lowIndex;
    }

    public float detect(float[] previousSpectrum, float[] spectrum)
    {
        //// BeatsLevel beatsLevel = BeatsLevel.none;

        float currentFlux = computeFlux(previousSpectrum, spectrum);
        if (currentFlux > maxFlux)
        {
            maxFlux = currentFlux;
        }
        fluxWindow.add(currentFlux);
        float averageFlux = fluxWindow.average();
        float boostValue = 2f;
        threshold = averageFlux * boostValue;
        flux = fluxWindow.getPrevious(1);

        savePeak = false;
        reportPeak = false;
        peak = flux - threshold;
        if (peak > 0)
        {
            //// compare with prev time and next time
            float previousFlux = fluxWindow.getPrevious(2);
            float nextFlux = currentFlux;
            if (previousFlux < flux && nextFlux < flux)
            {
                float decayedPeakAverage = getDecayedPeakAverage();
                if (peak > decayedPeakAverage * peakSaveLevel)
                {
                    savePeak = true;
                    //// beatsLevel = BeatsLevel.big;
                }
                if (peak > decayedPeakAverage * peakReportLevel)
                {
                    reportPeak = true;
                    //// beatsLevel = BeatsLevel.small;
                }
            }
        }
        if (savePeak)
        {
            peakWindow.add(peak);
            peakAverage = peakWindow.average();
            lastPeakIteration = iteration;
        }

        if (reportPeak && beatDetectorListener != null)
        {
            beatDetectorListener.onBeatDetected(this, flux, peak, savePeak);
        }

        iteration++;

        // ---------------------
        //// mBeatsHistory.record(beatsLevel);

        return flux;
    }

    public float getDecayedPeakAverage()
    {
        int iterationsSincePeak = iteration - lastPeakIteration;
        return peakAverage - (iterationsSincePeak * peakAverage * peakDecayPerIteration);
    }

    public void setBeatDetectorListener(IBeatDetectorListener listener)
    {
        this.beatDetectorListener = listener;
    }

    public float getMaxFlux()
    {
        return maxFlux;
    }

    public float getThreshold()
    {
        return threshold;
    }

    public float getFlux()
    {
        return flux;
    }

    public float getPeak()
    {
        return peak;
    }

    public bool isBeat()
    {
        //// if (mBeatsHistory.GetOldBeat() != BeatsLevel.none)
        //// return true;

        return reportPeak;
    }

    public bool isBigBeat()
    {
        //// if (mBeatsHistory.GetOldBeat() == BeatsLevel.big)
        ////  return true;

        return savePeak;
    }

    public float getNormalizedPeak()
    {
        return peak / peakAverage;
    }

    private float computeFlux(float[] previousSpectrum, float[] spectrum)
    {
        float flux = 0;
        for (int index = lowIndex; index <= highIndex; index++)
        {
            float value = spectrum[index] - previousSpectrum[index];
            value = (value + Mathf.Abs(value)) / 2;
            flux += value;
        }
        return flux;
    }
}

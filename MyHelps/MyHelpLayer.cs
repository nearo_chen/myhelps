﻿using UnityEngine;
using System.Collections;

public static class MyHelpLayer
{
    /// <summary>
    /// 判斷傳入的camera culling layerMask有無包含layerName的layer
    /// </summary>
    public static bool IsMaskContainLayer(LayerMask layerMask, string layerName)
    {
        int layer = LayerMask.NameToLayer(layerName);
        if (layer < 0)
        {
            Debug.LogError("[MyHelpLayer] [IsContainLayer] project doesn't has layer : " + layerName);
            return false;
        }
        return (layerMask.value & 1 << layer) != 0;
    }

    /// <summary>
    /// 將傳入的camera culling layerMask移除layerName的layer
    /// </summary>
    public static LayerMask RemoveMaskLayer(LayerMask layerMask, string layerName)
    {
        int layer = LayerMask.NameToLayer(layerName);
        if (layer < 0)
        {
            Debug.LogError("[MyHelpLayer] [RemoveLayer] project doesn't has layer : " + layerName);
            return layerMask;
        }
        return RemoveMaskLayer(layerMask, layer);
    }

    /// <summary>
    /// 將傳入的camera culling layerMask移除layerName的layer
    /// </summary>
    public static LayerMask RemoveMaskLayer(LayerMask layerMask, int layer)
    {
        layerMask.value &= ~(1 << layer);
        return layerMask;
    }

    /// <summary>
    /// 將傳入的camera culling layerMask設定layerName的layer給他
    /// </summary>
    public static LayerMask InsertMaskLayer(LayerMask layerMask, string layerName)
    {
        int layer = LayerMask.NameToLayer(layerName);
        if (layer < 0)
        {
            Debug.LogError("[MyHelpLayer] [SetLayer] project doesn't has layer : " + layerName);
            return layerMask;
        }
        return InsertMaskLayer(layerMask, layer);
    }

    /// <summary>
    /// 將傳入的camera culling layerMask設定layerName的layer給他
    /// </summary>
    public static LayerMask InsertMaskLayer(LayerMask layerMask, int layer)
    {
        layerMask.value |= (1 << layer);
        return layerMask;
    }

    //Unity一個物件只能有一個layer選擇，不能複選
    //public static void InsertSceneLayer(Transform root, int layer)
    //{
    //    SetSceneLayer(root, layer);
    //}

    /// <summary>
    /// 設定TREE的LAYER
    /// </summary>
    public static void SetSceneLayer(Transform root, int layer)
    {
        root.gameObject.layer = layer;
        for (int a = 0; a < root.childCount; a++)
        {
            SetSceneLayer(root.GetChild(a), layer);
        }
    }

    /// <summary>
    /// 移除TREE的LAYER
    /// </summary>
    //Unity一個物件只能有一個layer選擇，不能複選
    //public static void RemoveSceneLayer(Transform root, int layer)
    //{
    //    root.gameObject.layer &= ~layer;

    //    for (int a = 0; a < root.childCount; a++)
    //    {
    //        RemoveSceneLayer(root.GetChild(a), layer);
    //    }
    //}
}
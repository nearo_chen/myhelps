﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Draw arrow direction to visualize velocity, by rigidbody
/// </summary>
[RequireComponent(typeof(ArrowDrawBase))]
public class VelocityArrowRigidbody : MonoBehaviour
{
    //[SerializeField] GameObject cameraRBPrefab;
    [Tooltip("Control the velocity's length")]
    [SerializeField] float velocitySensitive = 0.5f;
    [Tooltip("Control the arrow enable")]
    [SerializeField] float velocityThreshold = 20;
    float MaxLengthRate;

    Rigidbody _rb;
    [SerializeField] TextMesh debugText;
    [SerializeField] Transform debugHit;
    Vector3 debugTopMaxPos;
    ArrowDrawBase _arrowDrawBase;

    void Start()
    {
        _arrowDrawBase = GetComponent<ArrowDrawBase>();
        MaxLengthRate = _arrowDrawBase.transform.localScale.z;
        //GameObject followCamObj = Instantiate(cameraRBPrefab);
        GameObject followCamObj = new GameObject("VelocityArrow_Rigidbody");
        followCamObj.AddComponent<MyFollowAttachNode>();
        _rb = followCamObj.AddComponent<Rigidbody>();
        MyFollowAttachNode attach = followCamObj.GetComponent<MyFollowAttachNode>();
        attach.RigidbodyUpdate = true;
        attach.attachNode = GetComponentInParent<Camera>().transform;
        attach.transform.position = attach.attachNode.position;//Must update pose to align attach node
        attach.transform.rotation = attach.attachNode.rotation;//Must update pose to align attach node        

        _rb.isKinematic = true;
        _rb.interpolation = RigidbodyInterpolation.Interpolate;
    }

    float velLength;
    private void FixedUpdate()
    {
        Vector3 angularV = _rb.angularVelocity;
        //Transform velocity into local space
        angularV = GetComponentInParent<Camera>().transform.InverseTransformVector(angularV);
        //I gnore roll
        angularV.z = 0;
        float rec = angularV.x;
        angularV.x = angularV.y;
        angularV.y = -rec;

        velLength = angularV.magnitude;

        if (velLength > velocityThreshold)
        {
            //Only get X&Y velocity
            Vector3 dir = angularV / velLength;
            float scale = velLength / velocitySensitive;
            _arrowDrawBase.SetArrow(dir, scale);

            if (debugHit != null)
            {
                debugHit.position = _arrowDrawBase.transform.position + _arrowDrawBase.GetArrowDir() * _arrowDrawBase.GetArrowLength();
            }
        }
    }

    void Update()
    {
        string t = "angularVelocity : " + _rb.angularVelocity.ToString();
        t += "\nvelocity : " + _rb.velocity;
        t += "\nvelLength : " + velLength;
        debugText.text = t;
    }
}

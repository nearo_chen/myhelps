﻿#define USE_EXCELNPOIREADxx

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Excel;
using System.IO;
using System.Data;
using System.Xml;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Format;
using ICSharpCode.SharpZipLib.Zip;

namespace MyHelps
{
    public static class MyHelpExcel
    {
        //public static string ExcelName = "西遊遊戲配樂-170117";

        /// <summary>  
        /// 读取 Excel 需要添加 Excel; System.Data;  
        /// </summary>  
        /// <param name="sheet"></param>  
        /// <returns></returns>  
        public static void ReadExcel(string fileName)
        {
            //需要解決CodePage 437 not supported http://community.sharpdevelop.net/forums/t/21795.aspx
            ZipConstants.DefaultCodePage = 0;
            List<XYMonsterExcelInfo> ExcelInfo = new List<XYMonsterExcelInfo>();
#if USE_EXCELNPOIREAD
            DataTable data = ExcelToTableForXLSX(fileName+".xlsx");
            for(int i=11;i<data.Rows.Count;i++)//表头自动扣掉了，再减去9行范例和2行分割线
            {
                if (data.Rows[i][2].ToString() == XYMonsterExcelInfo.Type.single.ToString())//自己新加上的来判断怪物类别
                {
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.id = data.Rows[i][0].ToString();
                    _data.time = data.Rows[i][1].ToString();
                    _data.resourceName = data.Rows[i][3].ToString();
                    _data.locdef = data.Rows[i][4].ToString();
                    _data.locationX = data.Rows[i][5].ToString();
                    _data.locationY = data.Rows[i][6].ToString();
                    _data.monsterType = data.Rows[i][2].ToString();
                    ExcelInfo.Add(_data);
                }
                if (data.Rows[i][2].ToString() == XYMonsterExcelInfo.Type.boss.ToString())
                {
                    //XYMonsterExcelInfo.bossInfo _data = new XYMonsterExcelInfo.bossInfo();
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.id = data.Rows[i][0].ToString();
                    _data.time = data.Rows[i][1].ToString();
                    _data.resourceName = data.Rows[i][3].ToString();
                    _data.locdef = data.Rows[i][4].ToString();
                    _data.locationX = data.Rows[i][5].ToString();
                    _data.locationY = data.Rows[i][6].ToString();
                    _data.tag = data.Rows[i][7].ToString();
                    _data.monsterType = data.Rows[i][2].ToString();
                    ExcelInfo.Add(_data);
                }
                if (data.Rows[i][2].ToString() == XYMonsterExcelInfo.Type.manyHit.ToString())
                {
                    // XYMonsterExcelInfo.manyhitInfo _data = new XYMonsterExcelInfo.manyhitInfo();
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.spawnTimeOffset = "0";
                    _data.locdef = data.Rows[i][4].ToString();
                    _data.time = data.Rows[i][1].ToString();
                    _data.resourceName = data.Rows[i][3].ToString();
                    _data.id = data.Rows[i][0].ToString();
                    _data.timeLength = data.Rows[i][8].ToString();
                    _data.HitAmount = data.Rows[i][9].ToString();
                    _data.Moveable = data.Rows[i][10].ToString();
                    _data.monsterType = data.Rows[i][2].ToString();
                    ExcelInfo.Add(_data);
                }
            }
#else
            string path = MyHelps.MyHelpXML.GetFullPath(fileName + ".xlsx");
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = excelReader.AsDataSet();
            for (int i = 12; i < result.Tables[0].Rows.Count; i++)//把前12行扣掉，0行用来定义属性名称,前9行是范例,2行分割线
            {
                if (result.Tables[0].Rows[i][2].ToString() == XYMonsterExcelInfo.Type.single.ToString())//自己新加上的来判断怪物类别
                {
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.id = result.Tables[0].Rows[i][0].ToString();
                    _data.time = result.Tables[0].Rows[i][1].ToString();
                    _data.resourceName = result.Tables[0].Rows[i][3].ToString();
                    _data.locdef = result.Tables[0].Rows[i][4].ToString();
                    _data.locationX = result.Tables[0].Rows[i][5].ToString();
                    _data.locationY = result.Tables[0].Rows[i][6].ToString();
                    _data.locationZ = result.Tables[0].Rows[i][7].ToString();
                    _data.Score = result.Tables[0].Rows[i][8].ToString();
                    _data.monsterType = result.Tables[0].Rows[i][2].ToString();
                    _data.hitSound = result.Tables[0].Rows[i][13].ToString();
                    if (CheckUpEmpty(_data))
                        continue;
                    ExcelInfo.Add(_data);
                }
                if (result.Tables[0].Rows[i][2].ToString() == XYMonsterExcelInfo.Type.boss.ToString())
                {
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.id = result.Tables[0].Rows[i][0].ToString();
                    _data.time = result.Tables[0].Rows[i][1].ToString();
                    _data.resourceName = result.Tables[0].Rows[i][3].ToString();
                    _data.locdef = result.Tables[0].Rows[i][4].ToString();
                    _data.locationX = result.Tables[0].Rows[i][5].ToString();
                    _data.locationY = result.Tables[0].Rows[i][6].ToString();
                    _data.locationZ = result.Tables[0].Rows[i][7].ToString();
                    _data.Score = result.Tables[0].Rows[i][8].ToString();
                    _data.tag = result.Tables[0].Rows[i][9].ToString();
                    _data.monsterType = result.Tables[0].Rows[i][2].ToString();
                    _data.hitSound = result.Tables[0].Rows[i][13].ToString();
                    if (CheckUpEmpty(_data))
                        continue;
                    ExcelInfo.Add(_data);
                }
                if (result.Tables[0].Rows[i][2].ToString() == XYMonsterExcelInfo.Type.manyHit.ToString())
                {
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.spawnTimeOffset = "0";
                    _data.locdef = result.Tables[0].Rows[i][4].ToString();
                    _data.time = result.Tables[0].Rows[i][1].ToString();
                    _data.resourceName = result.Tables[0].Rows[i][3].ToString();
                    _data.id = result.Tables[0].Rows[i][0].ToString();
                    _data.Score = result.Tables[0].Rows[i][8].ToString();
                    _data.timeLength = result.Tables[0].Rows[i][10].ToString();
                    _data.HitAmount = result.Tables[0].Rows[i][11].ToString();
                    _data.Moveable = result.Tables[0].Rows[i][12].ToString();
                    _data.monsterType = result.Tables[0].Rows[i][2].ToString();
                    _data.hitSound = result.Tables[0].Rows[i][13].ToString();
                    if (CheckUpEmpty(_data))
                        continue;
                    ExcelInfo.Add(_data);
                }
                if (result.Tables[0].Rows[i][2].ToString() == XYMonsterExcelInfo.Type.continueHit.ToString())
                {
                    XYMonsterExcelInfo _data = new XYMonsterExcelInfo();
                    _data.id = result.Tables[0].Rows[i][0].ToString();
                    _data.time = result.Tables[0].Rows[i][1].ToString();
                    _data.resourceName = result.Tables[0].Rows[i][3].ToString();
                    _data.locdef = result.Tables[0].Rows[i][4].ToString();
                    _data.locationX = result.Tables[0].Rows[i][5].ToString();
                    _data.locationY = result.Tables[0].Rows[i][6].ToString();
                    _data.locationZ = result.Tables[0].Rows[i][7].ToString();
                    _data.Score = result.Tables[0].Rows[i][8].ToString();
                    _data.tag = result.Tables[0].Rows[i][9].ToString();
                    _data.monsterType = result.Tables[0].Rows[i][2].ToString();
                    _data.hitSound = result.Tables[0].Rows[i][13].ToString();
                    if (CheckUpEmpty(_data))
                        continue;
                    ExcelInfo.Add(_data);
                    //把从excel中读取到的ContinueHit怪放在List中便于筛选
                    if(!EliteMonsterManager.Instance.ContinueHitName.Contains(_data.resourceName))
                        EliteMonsterManager.Instance.ContinueHitName.Add(_data.resourceName);
                }
            }
#endif
            XmlDocument xml = MyHelpExcel.CreateXML();
            for (int i = 0; i < ExcelInfo.Count; i++)
            {
                if (ExcelInfo[i].monsterType == XYMonsterExcelInfo.Type.single.ToString())
                {
                    MyHelpExcel.AddSingleTempoToXML(xml, ExcelInfo[i].id, ExcelInfo[i].time, ExcelInfo[i].resourceName, ExcelInfo[i].locdef, ExcelInfo[i].locationX, ExcelInfo[i].locationY, ExcelInfo[i].locationZ,ExcelInfo[i].Score, ExcelInfo[i].hitSound);
                }
                if (ExcelInfo[i].monsterType == XYMonsterExcelInfo.Type.manyHit.ToString())
                {
                    MyHelpExcel.AddManyHitTempoToXML(xml, ExcelInfo[i].spawnTimeOffset, ExcelInfo[i].locdef, ExcelInfo[i].time, ExcelInfo[i].resourceName, ExcelInfo[i].id, ExcelInfo[i].Score, ExcelInfo[i].timeLength, ExcelInfo[i].HitAmount, ExcelInfo[i].Moveable, ExcelInfo[i].hitSound);
                }
                if (ExcelInfo[i].monsterType == XYMonsterExcelInfo.Type.boss.ToString())
                {
                    MyHelpExcel.AddBossToXML(xml, ExcelInfo[i].id, ExcelInfo[i].time, ExcelInfo[i].resourceName, ExcelInfo[i].locdef, ExcelInfo[i].locationX, ExcelInfo[i].locationY, ExcelInfo[i].locationZ, ExcelInfo[i].Score, ExcelInfo[i].tag, ExcelInfo[i].hitSound);
                }
                if (ExcelInfo[i].monsterType == XYMonsterExcelInfo.Type.continueHit.ToString())
                {
                    MyHelpExcel.AddContinueHitToXML(xml, ExcelInfo[i].id, ExcelInfo[i].time, ExcelInfo[i].resourceName, ExcelInfo[i].locdef, ExcelInfo[i].locationX, ExcelInfo[i].locationY, ExcelInfo[i].locationZ, ExcelInfo[i].Score, ExcelInfo[i].tag, ExcelInfo[i].hitSound);
                }
            }
            MyHelpExcel.SaveXML(xml);
        }

        //XmlDocument CreateXML()
        //{
        //    //新建xml对象  
        //    XmlDocument xml = new XmlDocument();
        //    //加入声明  
        //    xml.AppendChild(xml.CreateXmlDeclaration("1.0", "UTF-8", null));
        //    //加入根元素  
        //    xml.AppendChild(xml.CreateElement("tempoList"));
        //    return xml;
        //}

        /// <summary>
        /// 增加single怪
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="id"></param>
        /// <param name="time"></param>
        /// <param name="resourceName"></param>
        /// <param name="locdef"></param>
        /// <param name="localX"></param>
        /// <param name="localY"></param>
        public static void AddSingleTempoToXML(XmlDocument xml, string id, string time, string resourceName, string locdef, string localX, string localY, string localZ,string Score, string hitSound)
        {
            //获取根节点
            XmlNode root = xml.SelectSingleNode("IOTempoData/tempoList");
            //添加一级元素  
            XmlElement element1 = xml.CreateElement("Tempo");
            //添加二级元素  
            XmlElement element2 = xml.CreateElement("singleTempo");

            //在singleTempo节点下添加id子节点 
            XmlElement elelment_id = xml.CreateElement("id");
            elelment_id.InnerText = id;

            //在singleTempo节点下添加time子节点 
            XmlElement elelment_time = xml.CreateElement("time");
            elelment_time.InnerText = time;

            //在singleTempo节点下添加resourceName子节点 
            XmlElement elelment_resourceName = xml.CreateElement("resourceName");
            elelment_resourceName.InnerText = resourceName;

            //在singleTempo节点下添加locdef子节点 
            XmlElement elelment_locdef = xml.CreateElement("locdef");
            elelment_locdef.InnerText = locdef;

            //在singleTempo节点下添加locationX子节点 
            XmlElement elelment_localX = xml.CreateElement("locationX");
            elelment_localX.InnerText = localX;

            //在singleTempo节点下添加locationY子节点 
            XmlElement elelment_localY = xml.CreateElement("locationY");
            elelment_localY.InnerText = localY;

            //在singleTempo节点下添加locationZ子节点 
            XmlElement elelment_localZ = xml.CreateElement("locationZ");
            elelment_localZ.InnerText = localZ;

            //在singleTempo节点下添加Score子节点 
            XmlElement elelment_Score = xml.CreateElement("Score");
            elelment_Score.InnerText = Score;

            //在singleTempo节点下添加hitSound子节点 
            XmlElement elelment_hitSound = xml.CreateElement("hitSound");
            elelment_hitSound.InnerText = hitSound;

            root.AppendChild(element1);
            element1.AppendChild(element2);
            element2.AppendChild(elelment_id);
            element2.AppendChild(elelment_time);
            element2.AppendChild(elelment_resourceName);
            element2.AppendChild(elelment_locdef);
            element2.AppendChild(elelment_localX);
            element2.AppendChild(elelment_localY);
            element2.AppendChild(elelment_localZ);
            element2.AppendChild(elelment_Score);
            element2.AppendChild(elelment_hitSound);
        }

        /// <summary>
        /// 增加manyHit怪
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="spawnTimeOffset"></param>
        /// <param name="locdef"></param>
        /// <param name="time"></param>
        /// <param name="resourceName"></param>
        /// <param name="id"></param>
        /// <param name="timeLength"></param>
        /// <param name="HitAmount"></param>
        /// <param name="Moveable"></param>
        public static void AddManyHitTempoToXML(XmlDocument xml, string spawnTimeOffset, string locdef, string time, string resourceName, string id,string Score, string timeLength, string HitAmount, string Moveable, string hitSound)
        {
            //获取根节点
            XmlNode root = xml.SelectSingleNode("IOTempoData/tempoList");
            //添加一级元素  
            XmlElement element1 = xml.CreateElement("Tempo");
            //添加二级元素  
            XmlElement element2 = xml.CreateElement("manyHitTempo");

            //在manyHitTempo节点下添加spawnTimeOffset子节点 
            XmlElement elelment_spawnTimeOffset = xml.CreateElement("spawnTimeOffset");
            elelment_spawnTimeOffset.InnerText = spawnTimeOffset;

            //在manyHitTempo节点下添加locdef子节点 
            XmlElement elelment_locdef = xml.CreateElement("locdef");
            elelment_locdef.InnerText = locdef;

            //在manyHitTempo节点下添加time子节点 
            XmlElement elelment_time = xml.CreateElement("time");
            elelment_time.InnerText = time;

            //在manyHitTempo节点下添加resourceName子节点 
            XmlElement elelment_resourceName = xml.CreateElement("resourceName");
            elelment_resourceName.InnerText = resourceName;

            //在manyHitTempo节点下添加id子节点 
            XmlElement elelment_id = xml.CreateElement("id");
            elelment_id.InnerText = id;

            //在manyHitTempo节点下添加Score子节点 
            XmlElement elelment_Score = xml.CreateElement("Score");
            elelment_Score.InnerText = Score;

            //在manyHitTempo节点下添加timeLength子节点 
            XmlElement elelment_timeLength = xml.CreateElement("timeLength");
            elelment_timeLength.InnerText = timeLength;

            //在manyHitTempo节点下添加HitAmount子节点 
            XmlElement elelment_HitAmount = xml.CreateElement("HitAmount");
            elelment_HitAmount.InnerText = HitAmount;

            //在manyHitTempo节点下添加Moveable子节点 
            XmlElement elelment_Moveable = xml.CreateElement("Moveable");
            elelment_Moveable.InnerText = Moveable;

            //在manyHitTempo节点下添加hitSound子节点 
            XmlElement elelment_hitSound = xml.CreateElement("hitSound");
            elelment_hitSound.InnerText = hitSound;

            root.AppendChild(element1);
            element1.AppendChild(element2);
            element2.AppendChild(elelment_spawnTimeOffset);
            element2.AppendChild(elelment_locdef);
            element2.AppendChild(elelment_time);
            element2.AppendChild(elelment_resourceName);
            element2.AppendChild(elelment_id);
            element2.AppendChild(elelment_Score);
            element2.AppendChild(elelment_timeLength);
            element2.AppendChild(elelment_HitAmount);
            element2.AppendChild(elelment_Moveable);
            element2.AppendChild(elelment_hitSound);
        }

        /// <summary>
        /// 增加boss怪
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="id"></param>
        /// <param name="time"></param>
        /// <param name="resourceName"></param>
        /// <param name="locdef"></param>
        /// <param name="localX"></param>
        /// <param name="localY"></param>
        /// <param name="tag"></param>
        public static void AddBossToXML(XmlDocument xml, string id, string time, string resourceName, string locdef, string localX, string localY, string localZ,string Score, string tag, string hitSound)
        {
            //获取一级根节点
            XmlNode root = xml.SelectSingleNode("IOTempoData/tempoList");
            //添加一级元素  
            XmlElement element1 = xml.CreateElement("Tempo");
            //添加二级元素  
            XmlElement element2 = xml.CreateElement("singleTempo");
            //element1.SetAttribute("Type", "string");  

            //在singleTempo节点下添加id子节点 
            XmlElement elelment_id = xml.CreateElement("id");
            elelment_id.InnerText = id;

            //在singleTempo节点下添加time子节点 
            XmlElement elelment_time = xml.CreateElement("time");
            elelment_time.InnerText = time;

            //在singleTempo节点下添加resourceName子节点 
            XmlElement elelment_resourceName = xml.CreateElement("resourceName");
            elelment_resourceName.InnerText = resourceName;

            //在singleTempo节点下添加locdef子节点 
            XmlElement elelment_locdef = xml.CreateElement("locdef");
            elelment_locdef.InnerText = locdef;

            //在singleTempo节点下添加locationX子节点 
            XmlElement elelment_localX = xml.CreateElement("locationX");
            elelment_localX.InnerText = localX;

            //在singleTempo节点下添加locationY子节点 
            XmlElement elelment_localY = xml.CreateElement("locationY");
            elelment_localY.InnerText = localY;

            //在singleTempo节点下添加locationZ子节点 
            XmlElement elelment_localZ = xml.CreateElement("locationZ");
            elelment_localZ.InnerText = localZ;

            //在singleTempo节点下添加Score子节点 
            XmlElement elelment_Score = xml.CreateElement("Score");
            elelment_Score.InnerText = Score;

            //在singleTempo节点下添加tag子节点 
            XmlElement elelment_tag = xml.CreateElement("tag");
            elelment_tag.InnerText = tag;

            //在singleTempo节点下添加hitSound子节点 
            XmlElement elelment_hitSound = xml.CreateElement("hitSound");
            elelment_hitSound.InnerText = hitSound;

            root.AppendChild(element1);
            element1.AppendChild(element2);
            element2.AppendChild(elelment_id);
            element2.AppendChild(elelment_time);
            element2.AppendChild(elelment_resourceName);
            element2.AppendChild(elelment_locdef);
            element2.AppendChild(elelment_localX);
            element2.AppendChild(elelment_localY);
            element2.AppendChild(elelment_localZ);
            element2.AppendChild(elelment_Score);
            element2.AppendChild(elelment_tag);
            element2.AppendChild(elelment_hitSound);
        }

        /// <summary>
        /// 增加ManyHitbyTag怪
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="id"></param>
        /// <param name="time"></param>
        /// <param name="resourceName"></param>
        /// <param name="locdef"></param>
        /// <param name="localX"></param>
        /// <param name="localY"></param>
        /// <param name="tag"></param>
        public static void AddContinueHitToXML(XmlDocument xml, string id, string time, string resourceName, string locdef, string localX, string localY, string localZ,string Score, string tag, string hitSound)
        {
            //获取一级根节点
            XmlNode root = xml.SelectSingleNode("IOTempoData/tempoList");
            //添加一级元素  
            XmlElement element1 = xml.CreateElement("Tempo");
            //添加二级元素  
            XmlElement element2 = xml.CreateElement("continueHitTempo");
            //element1.SetAttribute("Type", "string");  

            //在continueHit节点下添加id子节点 
            XmlElement elelment_id = xml.CreateElement("id");
            elelment_id.InnerText = id;

            //在continueHit节点下添加time子节点 
            XmlElement elelment_time = xml.CreateElement("time");
            elelment_time.InnerText = time;

            //在continueHit节点下添加resourceName子节点 
            XmlElement elelment_resourceName = xml.CreateElement("resourceName");
            elelment_resourceName.InnerText = resourceName;

            //在continueHit节点下添加locdef子节点 
            XmlElement elelment_locdef = xml.CreateElement("locdef");
            elelment_locdef.InnerText = locdef;

            //在continueHit节点下添加locationX子节点 
            XmlElement elelment_localX = xml.CreateElement("locationX");
            elelment_localX.InnerText = localX;

            //在continueHit节点下添加locationY子节点 
            XmlElement elelment_localY = xml.CreateElement("locationY");
            elelment_localY.InnerText = localY;

            //在continueHit节点下添加locationZ子节点 
            XmlElement elelment_localZ = xml.CreateElement("locationZ");
            elelment_localZ.InnerText = localZ;

            //在continueHit节点下添加Score子节点 
            XmlElement elelment_Score = xml.CreateElement("Score");
            elelment_Score.InnerText = Score;

            //在continueHit节点下添加tag子节点 
            XmlElement elelment_tag = xml.CreateElement("tag");
            elelment_tag.InnerText = tag;

            //在continueHit节点下添加hitSound子节点 
            XmlElement elelment_hitSound = xml.CreateElement("hitSound");
            elelment_hitSound.InnerText = hitSound;

            root.AppendChild(element1);
            element1.AppendChild(element2);
            element2.AppendChild(elelment_id);
            element2.AppendChild(elelment_time);
            element2.AppendChild(elelment_resourceName);
            element2.AppendChild(elelment_locdef);
            element2.AppendChild(elelment_localX);
            element2.AppendChild(elelment_localY);
            element2.AppendChild(elelment_localZ);
            element2.AppendChild(elelment_Score);
            element2.AppendChild(elelment_tag);
            element2.AppendChild(elelment_hitSound);
        }

        public static void UpdateNodeToXML()
        {
            string filepath = Application.dataPath + @"/INFO.XML";
            if (File.Exists(filepath))
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(filepath);  //根据指定路径加载xml  
                XmlNodeList nodeList = xmldoc.SelectSingleNode("Root").ChildNodes; //Node节点  
                //遍历所有子节点  
                foreach (XmlElement xe in nodeList)
                {
                    //拿到节点中属性Type=“string”的节点  
                    if (xe.GetAttribute("Type") == "string")
                    {
                        //更新节点属性  
                        xe.SetAttribute("type", "text");
                        //继续遍历  
                        foreach (XmlElement xelement in xe.ChildNodes)
                        {
                            if (xelement.Name == "TitleNode")
                            {
                                //修改节点名称对应的数值，而上面的拿到节点连带的属性  
                                //xelement.SetAttribute("Title", "企业简介");  
                                xelement.InnerText = "企业简介";
                            }
                        }
                        break;
                    }
                }
                xmldoc.Save(filepath);
            }
        }
        public static XmlDocument CreateXML()
        {
            //新建xml对象  
            XmlDocument xml = new XmlDocument();
            //加入声明  
            xml.AppendChild(xml.CreateXmlDeclaration("1.0", "UTF-8", "yes"));
            XmlElement root = xml.CreateElement("IOTempoData");
            root.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            //加入根元素  
            xml.AppendChild(root);
            XmlElement node = xml.CreateElement("tempoList");
            root.AppendChild(node);
            return xml;
        }

        public static void SaveXML(XmlDocument xml)
        {
            //存储xml文件  
#if UNITY_EDITOR || UNITY_STANDALONE
            xml.Save(Directory.GetCurrentDirectory() + "/" + GamePlayingManager.GameRoadPath + ".xml");
            Debug.Log(GamePlayingManager.GameRoadPath + ".xml存档成功!");
#elif UNITY_ANDROID  
        xml.Save(Application.persistentDataPath + "/INFO.xml");  
#endif
        }

        public static DataTable ExcelToTableForXLSX(string file)
        {
            DataTable dt = new DataTable();
            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(fs);
                ISheet sheet = xssfworkbook.GetSheetAt(0);
                //表头  
                IRow header = sheet.GetRow(sheet.FirstRowNum);
                List<int> columns = new List<int>();
                for (int i = 0; i < header.LastCellNum; i++)
                {
                    object obj = GetValueTypeForXLSX(header.GetCell(i) as XSSFCell);
                    if (obj == null || obj.ToString() == string.Empty)
                    {
                        dt.Columns.Add(new DataColumn("Columns" + i.ToString()));
                        //continue;  
                    }
                    else
                        dt.Columns.Add(new DataColumn(obj.ToString()));
                    columns.Add(i);
                }
                //数据  
                for (int i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
                {
                    DataRow dr = dt.NewRow();
                    bool hasValue = false;
                    foreach (int j in columns)
                    {
                        dr[j] = GetValueTypeForXLSX(sheet.GetRow(i).GetCell(j) as XSSFCell);
                        if (dr[j] != null && dr[j].ToString() != string.Empty)
                        {
                            hasValue = true;
                        }
                    }
                    if (hasValue)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }
        private static object GetValueTypeForXLSX(XSSFCell cell)
        {
            if (cell == null)
                return null;
            switch (cell.CellType)
            {
                case CellType.Blank: //BLANK:  
                    return null;
                case CellType.Boolean: //BOOLEAN:  
                    return cell.BooleanCellValue;
                case CellType.Numeric: //NUMERIC:  
                    return cell.NumericCellValue;
                case CellType.String: //STRING:  
                    return cell.StringCellValue;
                case CellType.Error: //ERROR:  
                    return cell.ErrorCellValue;
                case CellType.Formula: //FORMULA:  
                default:
                    return "=" + cell.CellFormula;
            }
        }

        private static bool CheckUpEmpty(XYMonsterExcelInfo xy)
        {
            if (string.IsNullOrEmpty(xy.id))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:id");
                return true;
            }
            if (string.IsNullOrEmpty(xy.time))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:time");
                return true;
            }
            if (string.IsNullOrEmpty(xy.resourceName))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:resourceName");
                return true;
            }
            if (string.IsNullOrEmpty(xy.locdef))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:locdef");
                return true;
            }
            if (string.IsNullOrEmpty(xy.hitSound))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:hitSound");
                return true;
            }
            if (string.IsNullOrEmpty(xy.Score))
            {
                MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:Score");
                return true;
            }
            if (xy.monsterType == XYMonsterExcelInfo.Type.boss.ToString() || xy.monsterType == XYMonsterExcelInfo.Type.continueHit.ToString())
            {
                if (string.IsNullOrEmpty(xy.tag))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:tag");
                    return true;
                }
            }
            if (xy.monsterType == XYMonsterExcelInfo.Type.manyHit.ToString())
            {
                if (string.IsNullOrEmpty(xy.timeLength))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过出错ID：" + xy.id, "出错格式为:timeLength");
                    return true;
                }
                if (string.IsNullOrEmpty(xy.HitAmount))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:HitAmount");
                    return true;
                }
                if (string.IsNullOrEmpty(xy.Moveable))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:Moveable");
                    return true;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(xy.locationX))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:locationX");
                    return true;
                }
                if (string.IsNullOrEmpty(xy.locationY))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:locationY");
                    return true;
                }
                if (string.IsNullOrEmpty(xy.locationZ))
                {
                    MyMessageBox.Show("Excel格式出现问题该怪以跳过，出错ID：" + xy.id, "出错格式为:locationZ");
                    return true;
                }
            }
            return false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkFadeOut : MonoBehaviour
{
    public float Duaration = 10;
    public float StartInterval = 1f;
    //public float Damping = 0.1f;
    float _interval;
    float _timeCount;
    float _lastTime;
    Renderer[] meshRenders;
    //SkinnedMeshRenderer[] skinMeshRenders;

    // Use this for initialization
    void Start()
    {
        meshRenders = GetComponentsInChildren<Renderer>();
        //foreach (Renderer r in meshRenders)
            //Debug.LogError(r.gameObject.name);
        //skinMeshRenders = GetComponentsInChildren<SkinnedMeshRenderer>();
        _interval = StartInterval;
        _timeCount = _interval;
        _lastTime = Duaration;
    }

    // Update is called once per frame
    void Update()
    {
        _lastTime -= Time.deltaTime;

        if (_timeCount > 0)
        {
            _timeCount -= Time.deltaTime;
        }
        else
        {
            StartCoroutine(_blink(_interval / 2));
            if (_interval > 0)
            {
                _interval = _lastTime / Duaration * StartInterval;
            }
            else
            {
                _setMeshRenders(false);
                //_setSkinMeshRenders(false);
                Destroy(this);//把自己component destroy，不再閃爍
            }
            _timeCount = _interval;
        }
    }

    IEnumerator _blink(float time)
    {
        _setMeshRenders(false);
        //_setSkinMeshRenders(false);
        yield return new WaitForSeconds(time);
        _setMeshRenders(true);
        //_setSkinMeshRenders(true);
    }

    void _setMeshRenders(bool active)
    {
        if (meshRenders == null)
            return;
        foreach (Renderer mr in meshRenders)
        {
            //有些特效怪在怪物身上後會自動死亡或是一些狗屁蛋讓它消失了，造成mr == null，就報錯了，擋一下NULL
            if (mr != null)
                mr.enabled = active;
        }
    }

    //void _setSkinMeshRenders(bool active)
    //{
    //    if (skinMeshRenders == null)
    //        return;
    //    foreach (SkinnedMeshRenderer smr in skinMeshRenders)
    //    {
    //        smr.enabled = active;
    //    }
    //}
}

﻿using UnityEngine;
using System.Collections;

public class BackgroundQuadRenderer : MonoBehaviour
{
    //The camera to render background scene in RT
    Camera _backgroundSceneCamera;
    public Transform BackgroundSceneRoot;

    //The cameras render scene in front of the quad , which clear flag="depth only".
    public Camera[] MainCameraRemoveCullingmask;

    RenderTexture _RT;

    Camera _quadCamera;
    MeshRenderer _quadMesh;

    public Camera VRBackgroundQuadCamera;

    // Use this for initialization
    void Start()
    {
        int BackgroundSpaceSceneLayer = LayerMask.NameToLayer("BackgroundSpaceScene");
        int BackgroundQuadLayer = LayerMask.NameToLayer("BackgroundQuad");

        if (BackgroundSpaceSceneLayer < 0)
        {
            Debug.LogError("You need to add a 'BackgroundSpaceScene' layer");
            return;
        }

        if (BackgroundQuadLayer < 0)
        {
            Debug.LogError("You need to add a 'BackgroundQuad' layer");
            return;
        }

        Debug.Log("[BackgroundQuadRenderer] BackgroundQuadLayer: " + BackgroundQuadLayer);

        //_sceneCamera = GetComponent<Camera>();

        int screenW = Screen.width;
        int screenH = Screen.height;
        if (VRBackgroundQuadCamera != null && SteamVR.instance != null)
        {
            screenW = (int)(SteamVR.instance.sceneWidth);
            screenH = (int)(SteamVR.instance.sceneHeight);
        }
        _RT = new RenderTexture(screenW, screenH, 24);
        _RT.antiAliasing = 1;
        _RT.filterMode = FilterMode.Point;
        _RT.useMipMap = false;
        _RT.anisoLevel = 0;
                
        MyHelpNode.SetBackgroundSceneLayer(BackgroundSceneRoot, BackgroundSpaceSceneLayer, ref _backgroundSceneCamera);
        _backgroundSceneCamera.targetTexture = _RT;
        _backgroundSceneCamera.cullingMask = 1 << BackgroundSpaceSceneLayer;

        foreach (Camera c in MainCameraRemoveCullingmask)
        {
            if (c.enabled)
            {
                if (c.cullingMask == 0xff)
                    Debug.LogError("[BackgroundQuadRenderer] main camera should not draw everything!! -> " + c.name);
                else
                    c.cullingMask &= ~(1 << BackgroundQuadLayer | 1 << BackgroundSpaceSceneLayer);//remove draw layer "BackgroundQuad" & "BackgroundSpaceScene"
            }
        }

        GameObject quadCameraOBJ;
        if (VRBackgroundQuadCamera == null)
        {
            quadCameraOBJ = new GameObject("BackgroundQuadCamera");
            quadCameraOBJ.transform.parent = this.transform;
            quadCameraOBJ.transform.localPosition = Vector3.zero;
            quadCameraOBJ.transform.localRotation = Quaternion.identity;

            _quadCamera = quadCameraOBJ.AddComponent<Camera>();
        }
        else
        {
            quadCameraOBJ = VRBackgroundQuadCamera.gameObject;
            _quadCamera = VRBackgroundQuadCamera;
        }

        _quadCamera.clearFlags = CameraClearFlags.Color;
        _quadCamera.cullingMask = 1 << BackgroundQuadLayer;
        _quadCamera.backgroundColor = Color.red;
        _quadCamera.orthographic = true;
        _quadCamera.nearClipPlane = 0.3f;
        _quadCamera.farClipPlane = 0.5f;
        _quadCamera.depth = -2;
        _quadCamera.renderingPath = RenderingPath.VertexLit;
        _quadCamera.useOcclusionCulling = false;
        _quadCamera.hdr = false;

        GameObject quadOBJ = GameObject.CreatePrimitive(PrimitiveType.Quad);
        quadOBJ.transform.parent = quadCameraOBJ.transform;
        quadOBJ.transform.localPosition = Vector3.zero;
        quadOBJ.transform.localRotation = Quaternion.identity;
        quadOBJ.transform.localPosition = new Vector3(0, 0, 0.4f);//nearClipPlane = 0.3f, farClipPlane = 0.5f;
        quadOBJ.layer = BackgroundQuadLayer;

        Destroy(quadOBJ.GetComponent<MeshCollider>());//remove physX obj

        _quadMesh = quadOBJ.GetComponent<MeshRenderer>();
        _quadMesh.material = new Material(Shader.Find("Unlit/Texture"));
        _quadMesh.material.SetTexture("_MainTex", _RT);
        _quadMesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        _quadMesh.receiveShadows = false;
        _quadMesh.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

#if UNITY_5_4
        _quadMesh.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
#else
        _quadMesh.useLightProbes = false;
#endif
    }

    // Update is called once per frame
    void Update()
    {
        int screenW = Screen.width;
        int screenH = Screen.height;
        if (VRBackgroundQuadCamera != null && SteamVR.instance != null)
        {
            screenW = (int)(SteamVR.instance.sceneWidth);
            screenH = (int)(SteamVR.instance.sceneHeight);
        }

        if (_RT.width != screenW || _RT.height != screenH)
        {
            Debug.Log("[BackgroundQuadRenderer] Release RT");
            _RT.Release();
            _RT.width = screenW;
            _RT.height = screenH;
        }

        if (!_RT.IsCreated())
        {
            _RT.antiAliasing = 1;
            _RT.filterMode = FilterMode.Point;
            _RT.Create();

            _backgroundSceneCamera.pixelRect = new Rect(0, 0, screenW, screenH);
            _quadMesh.transform.localScale = new Vector3(1, (float)screenH / (float)screenW, 1);
            _quadCamera.orthographicSize = (float)screenH / (float)screenW * 0.5f;

            Debug.Log("Create rendertarget width: " + screenW + " , height: " + screenH);
        }



        //Debug.Log("_sceneCamera.pixelRect : " + _sceneCamera.pixelRect);
        //Debug.Log("screenW : " + Screen.width + " , screenH : " + Screen.height + " , aspect : " + _sceneCamera.aspect);
    }


}

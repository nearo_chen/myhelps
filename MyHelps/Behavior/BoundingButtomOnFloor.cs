﻿using UnityEngine;
using System.Collections;

namespace MyHelps
{

    public class BoundingButtomOnFloor : MonoBehaviour
    {
        Bounds? drawBounds = null;
        MyHelp.ShowMeshBounds showBound = new MyHelp.ShowMeshBounds();

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.A))
            {
                _fixObjectOnFloor(this.gameObject);
            }

            if (drawBounds.HasValue)
            {
                showBound.DrawBox(drawBounds.Value, Matrix4x4.identity);
            }
        }

        /// <summary>
        /// fix go's bounding box bottom align floor.
        /// </summary>
        bool _fixObjectOnFloor(GameObject go)
        {
            //Fix game object's bottom is on floor.        
            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
            if (renderers.Length > 0)
            {
                Bounds bounds = renderers[0].bounds;
                for (int a = 1; a < renderers.Length; a++)
                {
                    Renderer r = renderers[a];
                    Debug.Log(r.name + " , r.bounds : " + r.bounds);
                    bounds.Encapsulate(r.bounds);
                }

                Vector3 newLocalPos = go.transform.localPosition;
                Debug.Log("fix before : " + newLocalPos + " , bound : " + bounds);

                newLocalPos.y += bounds.extents.y;
                newLocalPos.y -= bounds.center.y;
                go.transform.localPosition = newLocalPos;

                drawBounds = bounds;
                Debug.Log("fix after : " + newLocalPos);
                return true;
            }
            return false;
        }

    }
}
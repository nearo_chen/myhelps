﻿#define USE_FOLLOWINGxx
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 給animation上，添加的event呼叫撥放特效
/// </summary>
public class EventPS : MonoBehaviour
{
#if USE_FOLLOWING
    static GameObject _eventPSNode = null;
    static GameObject GetEventPSNode()
    {
        if (_eventPSNode == null)
            _eventPSNode = new GameObject();
        _eventPSNode.name = "Event PS Node";
        return _eventPSNode;
    }
#endif

    public delegate void PlayCALLBACK(string aniName, string parentname);
    public PlayCALLBACK playcallback;

    List<GameObject> recordList = new List<GameObject>();
    public GameObject[] PSObject;
    public string[] ParentNames;

#if UNITY_EDITOR
    //DebugAnimationStates _debugAnimationStates = new DebugAnimationStates();
#endif

    public void play(string aniName)
    {
        Debug.LogWarning("[EventPS] aniName : " + aniName);
        int index = 0;
        GameObject prefab = null;
        foreach (GameObject p in PSObject)
        {
            if (p.name.ToLower() == aniName.ToLower())
            {
                prefab = p;
                break;
            }
            index++;
        }

        if (prefab == null)
        {
            string allname = " , all name : ";
            foreach (GameObject p in PSObject)
                allname += p.name + " , ";
            Animator ani = GetComponent<Animator>();

#if UNITY_EDITOR
            AnimatorClipInfo[] infos = ani.GetCurrentAnimatorClipInfo(0);
            foreach (AnimatorClipInfo info in infos)
            {
                Debug.LogWarning("[EventPS]" + info.clip.name + " , objName:" + ani.name);
                //_debugAnimationStates.Add(info.clip.name);
            }
#endif

            Debug.LogWarning("[EventPS] prefab not found : " + aniName + allname);
            return;
        }

        GameObject go;
        Transform parent;
        Transform find = null;
        if (ParentNames != null && ParentNames.Length > 0 && !string.IsNullOrEmpty(ParentNames[index]))
            MyHelpNode.FindTransform(transform, ParentNames[index], out find, false);

        if (find == null)
            parent = transform;
        else
            parent = find;

        if (playcallback != null)
            playcallback(aniName, parent.name);

#if USE_FOLLOWING
        go = Instantiate(prefab);//, parent);
        //設定跟隨物件
        Following following = go.AddComponent<Following>();
        following.SetTarget(parent.gameObject);
        go.transform.parent = GetEventPSNode().transform;
#else
        go = Instantiate(prefab, parent);

        //如果身上特效的renderer是關的就把他幹掉了，避免parent的開關影響到底下的特效
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
        for (int a = 0; a < renderers.Length; a++)
        {
            if (!renderers[a].enabled)
                Destroy(renderers[a]);
        }
#endif    

        //關閉所有renderer影子及接受影子
        RagDollSetting._setRendererCastShadow(go.transform, false);
        RagDollSetting._setRendererRecieveShadow(go.transform, false);
        RagDollSetting._setRendererMotionVectors(go.transform, false);
        RagDollSetting._setLightShadow(go.transform, false);
        RagDollSetting._setRendererLightProb(go.transform, false);
        RagDollSetting._setRendererReflectionProb(go.transform, false);

        /*Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderers)
        {
            r.receiveShadows = false;
            r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }*/

        //go.AddComponent<ParticleSystemAutoDestroy>();

        //Debug.LogWarning("animation event : " + transform.name + " : " + parent.name);
        
        recordList.Add(go);
    }

    /// <summary>
    /// 將已經生出的particle刪除
    /// </summary>
    public void DestroyCurrentEffect()
    {
        while (recordList.Count > 0)
        {
            if (recordList[0] == null)
            {
                recordList.RemoveAt(0);
            }
            else
            {
                DestroyImmediate(recordList[0]);
                recordList.RemoveAt(0);
            }
        }
    }

//#if UNITY_EDITOR
//    public class DebugAnimationStates
//    {
//        private Dictionary<int, string> m_states = new Dictionary<int, string>();

//        //public void Init(string[] aniNames)
//        //{
//        //    foreach (string name in aniNames)
//        //    {
//        //        Add(name);
//        //    }
//        //}

//        public string Get(int _fullPathHash)
//        {
//            string name;
//            if (m_states.TryGetValue(_fullPathHash, out name))
//                return name;

//            return "Unknown#";
//        }

//        public void Add(string _stateName)
//        {
//            int hash = Animator.StringToHash(_stateName);
//            m_states.Add(hash, _stateName);
//        }
//    }
//#endif
}

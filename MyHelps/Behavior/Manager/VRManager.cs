﻿#define USE_POSTPROCESSING
#define USE_DEVELOPING_LOGOxx
#define CAMERA_SHAKE_ENABLE
//#define DRAW_DEBUG
//#define USE_STEAMVR_PLAYERxx

using UnityEngine;
using System.Collections;

#if CAMERA_SHAKE_ENABLE
using DG.Tweening;
#endif

using Valve.VR.InteractionSystem;

#if USE_POSTPROCESSING
//https://github.com/Unity-Technologies/PostProcessing
using UnityEngine.PostProcessing;
#endif

public class VRManager : MySingleton<VRManager>
{
    public float near = 0.05f;
    public float far = 2000.0f;

    bool _isSwitchHand = false;
    public void SwitchHand()
    {
        _isSwitchHand = !_isSwitchHand;
    }

    //  public bool DRAW_DEBUG;
    public GameObject DevelopingLogoPrefab;

    public GameObject CameraRigPrefab;
    public GameObject SteamVRPlayerPrefab;

#if USE_POSTPROCESSING
    public PostProcessingProfile postProcessingProfile;
#endif

    public bool UseSteamVrPlayer;
    bool _UseSteamVrPlayerOld;

    GameObject _cameraRig, _cameraRigRoot;
    Transform _leftHand, _rightHand, _head, _ears, _cameraUI;
    /*
    /// <summary>
    /// 負責處理在editor階段對scene的處理
    /// </summary>
    [ContextMenu("PreSceneProcess")]
    public void PreSceneProcess()
    {
        GameObject cameraRigRoot = GameObject.Find("CameraRigRoot");
        if (cameraRigRoot != null)
            DestroyImmediate(cameraRigRoot);
        GameObject createCameraRig = CameraRig;//觸發創建camera rig
    }

    /// <summary>
    /// 負責處理在editor階段對scene的處理
    /// </summary>
    [ContextMenu("PreSceneProcess_ControllNotDraw")]
    public void PreSceneProcess_ControllNotDraw()
    {
        PreSceneProcess();
        VRManager.Instance.RightControllNotDraw();
        VRManager.Instance.LefttControllNotDraw();
    }*/

    void Start()
    {
        _UseSteamVrPlayerOld = UseSteamVrPlayer;
        GameObject createCameraRig = CameraRig;//觸發創建camera rig

        Debug.Log("[VRManager] enabled : " + SteamVR.enabled);
        Debug.Log("[VRManager] active : " + SteamVR.active);
        Debug.Log("[VRManager] calibrating : " + SteamVR.calibrating);
        Debug.Log("[VRManager] initializing : " + SteamVR.initializing);
        Debug.Log("[VRManager] usingNativeSupport : " + SteamVR.usingNativeSupport);
    }

    public void Reset()
    {
        _leftHand = _rightHand = _head = _ears = _cameraUI = null;
        _cameraRig = null;
        GameObject createCameraRig = CameraRig;//觸發創建camera rig
    }

    void Update()
    {
        if (UseSteamVrPlayer != _UseSteamVrPlayerOld)
        {
            Reset();
            _UseSteamVrPlayerOld = UseSteamVrPlayer;
            GameObject createCameraRig = CameraRig;//觸發創建camera rig
        }

        if (!SteamVR.active)
        {
            if (UseSteamVrPlayer)
            {
                CameraUI.GetComponent<Camera>().tag = "MainCamera";
            }
            _cameraRig.transform.localPosition = Vector3.up * 1.7f;//VIVE沒開就自動升高170CM
        }

        if (!UseSteamVrPlayer)
        {
            //有時使用cameraRig 左右在開開關關的情況下會失蹤，使用update找回來
            if (
                (LeftHand != null && !LeftHand.gameObject.activeSelf) &&
                (RightHand != null && !RightHand.gameObject.activeSelf))
            {
                SteamVR_ControllerManager controllManager = _cameraRig.GetComponent<SteamVR_ControllerManager>();
                if (controllManager != null)
                {
                    controllManager.Refresh();
                    Debug.LogWarning("[VRManager][有時使用cameraRig 左右在開開關關的情況下會失蹤，使用controllManager.UpdateTargets找回來]");
                }
            }
        }

        if (_rightControllNotDraw != _rightControllNotDrawOld && IsRightHandTracking())
        {
            //關閉controller render
            Transform BlankController_Hand2 = RightHand.transform.Find("BlankController_Hand2");
            if (BlankController_Hand2 != null)
            {
                SpawnRenderModel srm = BlankController_Hand2.GetComponent<SpawnRenderModel>();
                srm.enabled = !_rightControllNotDraw;
            }

            _rightControllNotDrawOld = _rightControllNotDraw;
            SteamVR_RenderModel[] rendermodels = RightHand.GetComponentsInChildren<SteamVR_RenderModel>();
            foreach (SteamVR_RenderModel rendermodel in rendermodels)
                rendermodel.GetComponent<Renderer>().enabled = !_rightControllNotDraw;
        }
        if (_leftControllNotDraw != _leftControllNotDrawOld && IsLeftHandTracking())
        {
            //關閉controller render
            Transform BlankController_Hand1 = LeftHand.transform.Find("BlankController_Hand1");
            if (BlankController_Hand1 != null)
            {
                SpawnRenderModel srm = BlankController_Hand1.GetComponent<SpawnRenderModel>();
                srm.enabled = !_leftControllNotDraw;
            }

            _leftControllNotDrawOld = _leftControllNotDraw;
            SteamVR_RenderModel[] rendermodels = LeftHand.GetComponentsInChildren<SteamVR_RenderModel>();
            foreach (SteamVR_RenderModel rendermodel in rendermodels)
                rendermodel.GetComponent<Renderer>().enabled = !_leftControllNotDraw;
        }

#if !DRAW_DEBUG
        return;
#endif

        if (Input.GetKeyDown(KeyCode.S) || ViveHelp.IsPadUpPress(VRManager.Instance.RightHand.GetComponent<SteamVR_TrackedObject>(), ViveHelp.PressState.Down))
        {
            if (_showText != null)
                _showText.gameObject.SetActive(!_showText.gameObject.activeSelf);
            if (_showText2 != null)
                _showText2.gameObject.SetActive(!_showText2.gameObject.activeSelf);
            if (_showText3 != null)
                _showText3.gameObject.SetActive(!_showText3.gameObject.activeSelf);
        }
    }

    /// <summary>
    /// 檢測VR頭盔是否啟用
    /// </summary>
    public bool IsVRAvailable
    {
        get
        {
            return //Head.gameObject.activeSelf &&
                   //Head.GetComponentInChildren<SteamVR_TrackedObject>().isValid &&
                SteamVR.active &&
                Valve.VR.OpenVR.IsRuntimeInstalled() &&
                Valve.VR.OpenVR.IsHmdPresent();
        }
    }

    /// <summary>
    /// 負責處理在editor階段對scene的處理
    /// </summary>
    [ContextMenu("Create camera rig root")]
    public void CreateCameraRigRoot()
    {
        GameObject cameraRigRoot = GameObject.Find("CameraRigRoot");
        if (cameraRigRoot != null)
            DestroyImmediate(cameraRigRoot);

        bool orig = UseSteamVrPlayer;

        UseSteamVrPlayer = false;
        Reset();//重設並創建

        UseSteamVrPlayer = true;
        Reset();//重設並創建

        UseSteamVrPlayer = orig;
    }

    public GameObject CameraRig
    {
        get
        {
            if (_cameraRig == null)
            {
                _cameraRigRoot = GameObject.Find("CameraRigRoot");
                if (_cameraRigRoot == null)
                    _cameraRigRoot = new GameObject("CameraRigRoot");

                //GameObject.Find(SteamVRPlayerPrefab.name);
                Transform cameraPlayer = null;
                Transform cameraRig = null;
                MyHelpNode.FindTransform(_cameraRigRoot.transform, SteamVRPlayerPrefab.name, out cameraPlayer);
                MyHelpNode.FindTransform(_cameraRigRoot.transform, CameraRigPrefab.name, out cameraRig);

                if (UseSteamVrPlayer)
                {
                    if (cameraPlayer != null) _cameraRig = cameraPlayer.gameObject;
                    if (cameraPlayer != null) cameraPlayer.gameObject.SetActive(true);
                    //因為cameraRig deactive之後，在開回來手柄會抓不到，所以直接幹掉他，他重生時會再重抓
                    //if (cameraRig != null) Destroy(cameraRig.gameObject);
                    if (cameraRig != null) cameraRig.gameObject.SetActive(false);
                }
                else
                {
                    if (cameraRig != null) _cameraRig = cameraRig.gameObject;
                    if (cameraPlayer != null) cameraPlayer.gameObject.SetActive(false);
                    if (cameraRig != null) cameraRig.gameObject.SetActive(true);
                }

                if (_cameraRig == null)
                {
                    if (UseSteamVrPlayer)
                        _cameraRig = Instantiate(SteamVRPlayerPrefab);
                    else
                        _cameraRig = Instantiate(CameraRigPrefab);

                    _cameraRig.name = _cameraRig.name.Replace("(Clone)", "");
                    _cameraRig.transform.parent = _cameraRigRoot.transform;

                    if (UseSteamVrPlayer)
                    {
                        //如果有[Status] 把它關掉
                        Transform find = null;
                        MyHelpNode.FindTransform(_cameraRig.transform, "[Status]", out find);
                        if (find != null)
                            find.gameObject.SetActive(false);

                        //把hand1設定成Left，把hand2設定成Right
                        MyHelpNode.FindTransform(_cameraRig.transform, "Hand1", out find);
                        Hand hand = find.GetComponent<Hand>();
                        hand.startingHandType = Hand.HandType.Left;

                        MyHelpNode.FindTransform(_cameraRig.transform, "Hand2", out find);
                        hand = find.GetComponent<Hand>();
                        hand.startingHandType = Hand.HandType.Right;
                    }
                    else
                    {
                        //把head的maincamera拿掉
                        Transform head = null;
                        MyHelpNode.FindTransform(_cameraRig.transform, "head", out head, true);
                        head.gameObject.tag = "Untagged";

                        //unity 5.6之後要在eye上面加一個steamvr update pose
                        SteamVR_UpdatePoses updatePose = CameraEye.gameObject.GetComponent<SteamVR_UpdatePoses>();
                        if (updatePose == null)
                            CameraEye.gameObject.AddComponent<SteamVR_UpdatePoses>();
                    }

#if USE_POSTPROCESSING
                    PostProcessingBehaviour ppb = CameraEye.gameObject.AddComponent<PostProcessingBehaviour>();
                    Debug.Log("CameraEye" + CameraEye.name);
                    ppb.profile = postProcessingProfile;
#endif

#if USE_DEVELOPING_LOGO
                    if (DevelopingLogoPrefab != null)
                    {
                        GameObject devLogo = Instantiate(DevelopingLogoPrefab);
                        Vector3 pos = devLogo.transform.localPosition;
                        Quaternion rot = devLogo.transform.localRotation;
                        devLogo.transform.parent = CameraEye;
                        devLogo.transform.localPosition = pos;
                        devLogo.transform.localRotation = rot;
                    }
#endif
                    Debug.LogWarning("[VRManager] Instantiate _cameraRig");
                }
                else
                {
                    _cameraRigRoot = _cameraRig.transform.parent.gameObject;
                    Debug.LogWarning("[VRManager] cameraRig use from scene");
                }

                SteamVR_ControllerManager controllManager = _cameraRig.GetComponent<SteamVR_ControllerManager>();
                if (controllManager != null)
                {
                    controllManager.objects = null;
                    controllManager.UpdateTargets();
                }

                CameraEye.GetComponentInChildren<Camera>().nearClipPlane = near;
                CameraEye.GetComponentInChildren<Camera>().farClipPlane = far;
#if !UNITY_EDITOR
                Transform DebugUI = _cameraRig.transform.Find("DebugUI");
                if (DebugUI != null)
                    DebugUI.gameObject.SetActive(false);
#endif
            }
            return _cameraRigRoot;
        }
    }

    public void CameraShake(float strength)
    {
#if CAMERA_SHAKE_ENABLE
        //按下B键测试，开关在GamePlayingManager的Update中
        _cameraRig.transform.DOShakePosition(0.5f, strength, 30).SetEase(Ease.OutQuart).OnComplete(_shakeComplete);
        //改变rotation，调整后也感觉蛮自然的
        //_cameraRig.transform.DOShakeRotation(0.5f, 1.0f, 10, 10.0f).SetEase(Ease.OutQuart);
#endif
    }

    void _shakeComplete()
    {
        _cameraRig.transform.localPosition = Vector3.zero;
    }

    public Transform RightHand
    {
        get
        {
            if (_isSwitchHand)
                return _checkLeftHand();
            else
                return _checkRightHand();
        }
    }

    Transform _checkRightHand()
    {
        if (UseSteamVrPlayer)
        {
            if (_rightHand == null)
                MyHelpNode.FindTransform(CameraRig.transform, "Hand2", out _rightHand, true);
        }
        else
        {
            if (_rightHand == null)
                MyHelpNode.FindTransform(CameraRig.transform, "right", out _rightHand, true);
        }
        return _rightHand;
    }

    bool _isHandTracking(Transform vrmanagerHand)
    {
        if (UseSteamVrPlayer)
        {
            Hand hand = vrmanagerHand.GetComponent<Hand>();
            if (hand == null || hand.controller == null || !hand.controller.valid)
                return false;
        }
        else
        {
            SteamVR_TrackedObject trackObj = vrmanagerHand.GetComponent<SteamVR_TrackedObject>();
            if (trackObj == null || !trackObj.isValid)
                return false;
        }
        return true;
    }

    public bool IsLeftHandTracking()
    {
        return _isHandTracking(LeftHand);
    }

    public bool IsRightHandTracking()
    {
        return _isHandTracking(RightHand);
    }

    public Transform LeftHand
    {
        get
        {
            if (_isSwitchHand)
                return _checkRightHand();
            else
                return _checkLeftHand();
        }
    }

    Transform _checkLeftHand()
    {
        if (UseSteamVrPlayer)
        {
            if (_leftHand == null)
                MyHelpNode.FindTransform(CameraRig.transform, "Hand1", out _leftHand, true);
        }
        else
        {
            if (_leftHand == null)
                MyHelpNode.FindTransform(CameraRig.transform, "left", out _leftHand, true);
        }
        return _leftHand;
    }

    /// <summary>
    /// 取得VR上有Camera類別繪出畫面的node
    /// </summary>
    public Transform CameraEye
    {
        get { return Head; }
    }

    public bool IsHeadTracking()
    {
#if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying)
            return true;
#endif

        if (UseSteamVrPlayer)
        {
            if (SteamVR.instance == null || !Player.instance.rigSteamVR.activeSelf)
                return false;
        }
        else
        {
            SteamVR_TrackedObject headTrack = Head.GetComponentInChildren<SteamVR_TrackedObject>();
            if (headTrack == null || !headTrack.isValid)
                return false;
        }
        return true;
    }

    public Transform Head
    {
        get
        {
            if (UseSteamVrPlayer)
            {
                if (_head == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "VRCamera", out _head, true);
            }
            else
            {
                if (_head == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "eye", out _head, true);//執行時head是在eye下面的，所以我改抓eye當作head                
            }
            return _head;
        }
    }

    public Transform CameraEars
    {
        get
        {
            if (UseSteamVrPlayer)
            {
                if (_ears == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "FollowHead", out _ears, true);//用来让Monster LookAt面对自己，但是eyes/head跑丢了，抓ears坐标
            }
            else
            {
                if (_ears == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "ears", out _ears, true);//用来让Monster LookAt面对自己，但是eyes/head跑丢了，抓ears坐标
            }
            return _ears;
        }
    }

    public Transform CameraUI
    {
        get
        {
            if (UseSteamVrPlayer)
            {
                if (_cameraUI == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "FallbackObjects", out _cameraUI, false);//必須完全match去搜尋
            }
            else
            {
                if (_cameraUI == null)
                    MyHelpNode.FindTransform(CameraRig.transform, "UI", out _cameraUI, true);
            }
            return _cameraUI;
        }
    }

    TextMesh _showText, _showText2, _showText3;//顯示在HMD前方debug用的字串

    /// <summary>
    /// VR畫面裡面顯示debug文字
    /// </summary>
    public void DebugText(string text, Color c)
    {
#if !UNITY_EDITOR && !DRAW_DEBUG
        return;
#endif
        // if (!DRAW_DEBUG)
        //     return;

        //GameObject backPlane;
        if (_showText == null)
        {
            GameObject obj = new GameObject("TextMesh", new System.Type[] { typeof(MeshRenderer), typeof(TextMesh) });
            _showText = obj.GetComponent<TextMesh>();
            _showText.transform.parent = this.Head;
            _showText.transform.localPosition = Vector3.right * -5f + Vector3.up * 1.5f + Vector3.forward * 10;
            _showText.transform.localRotation = Quaternion.identity;
            _showText.characterSize = 0.4f;
            //backPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            //backPlane.transform.parent = _showText.transform;
            //Destroy(backPlane.GetComponent<Collider>());
        }
        _showText.text = text;
        _showText.color = c;

        //backPlane = _showText.transform.GetChild(0).gameObject;
        //backPlane.transform.localRotation = Quaternion.Euler(270, 0, 0);
        //MeshRenderer renderer = _showText.gameObject.GetComponent<MeshRenderer>();
        //backPlane.transform.localPosition = new Vector3(renderer.bounds.extents.x, -renderer.bounds.extents.y, 0.1f);
        //backPlane.transform.localScale = new Vector3(renderer.bounds.extents.x * 0.3f, renderer.bounds.extents.z, renderer.bounds.extents.z * 0.02f);
    }

    /// <summary>
    /// VR畫面裡面顯示debug文字
    /// </summary>
    public void DebugText2(string text)
    {
#if !UNITY_EDITOR && !DRAW_DEBUG
        return;
#endif
        // if (!DRAW_DEBUG)
        //     return;

        //GameObject backPlane;
        if (_showText2 == null)
        {
            GameObject obj = new GameObject("TextMesh2", new System.Type[] { typeof(MeshRenderer), typeof(TextMesh) });
            _showText2 = obj.GetComponent<TextMesh>();
            _showText2.transform.parent = this.Head;
            _showText2.transform.localPosition = Vector3.right * -5f + Vector3.up * 0.5f + Vector3.forward * 10;
            _showText2.transform.localRotation = Quaternion.identity;
            _showText2.characterSize = 0.4f;
            //backPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            //backPlane.transform.parent = _showText2.transform;
            //Destroy(backPlane.GetComponent<Collider>());
        }
        _showText2.text = text;

        //backPlane = _showText2.transform.GetChild(0).gameObject;
        //backPlane.transform.localRotation = Quaternion.Euler(270, 0, 0);
        //MeshRenderer renderer = _showText2.gameObject.GetComponent<MeshRenderer>();
        //backPlane.transform.localPosition = new Vector3(renderer.bounds.extents.x, -renderer.bounds.extents.y, 0.1f);
        //backPlane.transform.localScale = new Vector3(renderer.bounds.extents.x * 0.3f, renderer.bounds.extents.z, renderer.bounds.extents.z * 0.02f);
    }

    /// <summary>
    /// VR畫面裡面顯示debug文字
    /// </summary>
    public void DebugText3(string text)
    {
#if !UNITY_EDITOR && !DRAW_DEBUG
        return;
#endif
        //  if (!DRAW_DEBUG)
        //     return;

        //GameObject backPlane;
        if (_showText3 == null)
        {
            GameObject obj = new GameObject("TextMesh3", new System.Type[] { typeof(MeshRenderer), typeof(TextMesh) });
            _showText3 = obj.GetComponent<TextMesh>();
            _showText3.transform.parent = this.Head;
            _showText3.transform.localPosition = Vector3.right * -5f + Vector3.up * -1f + Vector3.forward * 10;
            _showText3.transform.localRotation = Quaternion.identity;
            _showText3.characterSize = 0.4f;
            //backPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            //backPlane.transform.parent = _showText3.transform;
            //Destroy(backPlane.GetComponent<Collider>());
        }
        _showText3.text = text;

        //backPlane = _showText3.transform.GetChild(0).gameObject;
        //backPlane.transform.localRotation = Quaternion.Euler(270, 0, 0);
        //MeshRenderer renderer = _showText3.gameObject.GetComponent<MeshRenderer>();
        //backPlane.transform.localPosition = new Vector3(renderer.bounds.extents.x, -renderer.bounds.extents.y, 0.1f);
        //backPlane.transform.localScale = new Vector3(renderer.bounds.extents.x * 0.3f, renderer.bounds.extents.z, renderer.bounds.extents.z * 0.02f);
    }

    //public void RightHandHapticPulse(ushort durationTime)
    //{
    //    if (!RightHand.gameObject.activeSelf)
    //        return;
    //    IEnumerator coroutine = HapticPulse(RightHand, durationTime);
    //    StartCoroutine(coroutine);
    //}

    //解釋詳細的手柄震動的功能
    //http://www.itdadao.com/articles/c15a259328p0.html    
    public void HandHapticPulse(bool isRightHand, float durationTime)
    {
        //if (!hand.gameObject.activeSelf)
        //    return;
        IEnumerator coroutine = null;
        if (!isRightHand)
            coroutine = _hapticPulseLeft(durationTime);
        else
            coroutine = _hapticPulseRight(durationTime);

        if (coroutine != null)
            StartCoroutine(coroutine);
    }

    bool _leftShock, _rightShock;

    //定义了一个协程
    IEnumerator _hapticPulseLeft(float durationTime)
    {
        //Invoke函数，表示durationTime秒后，执行StopShock函数；
        _leftShock = true;
        Invoke("_LeftHandStopShock", durationTime);
        //Debug.LogWarning("_LeftHandStopShock  : Invoke : " + durationTime);

        //协程一直使得手柄产生震动，直到布尔型变量IsShock为false;
        while (_leftShock)
        {
            //Debug.LogWarning("_hapticPulseLeft");
            if (UseSteamVrPlayer)
            {
                ViveHelp.HapticPulse(LeftHand.GetComponent<Hand>().controller);
            }
            else
            {
                ViveHelp.HapticPulse(LeftHand.GetComponent<SteamVR_TrackedObject>());
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator _hapticPulseRight(float durationTime)
    {
        //Invoke函数，表示durationTime秒后，执行StopShock函数；
        _rightShock = true;
        Invoke("_RightHandStopShock", durationTime);
        //Debug.LogWarning("_RightHandStopShock  : Invoke : " + durationTime);

        //协程一直使得手柄产生震动，直到布尔型变量IsShock为false;
        while (_rightShock)
        {
            //Debug.LogWarning("_hapticPulseRight");
            if (UseSteamVrPlayer)
            {
                ViveHelp.HapticPulse(RightHand.GetComponent<Hand>().controller);
            }
            else
            {
                ViveHelp.HapticPulse(RightHand.GetComponent<SteamVR_TrackedObject>());
            }
            yield return new WaitForEndOfFrame();
        }
    }

    void _LeftHandStopShock()
    {
        _leftShock = false; //关闭手柄的震动
        //Debug.LogWarning("_LeftHandStopShock : xxxxxxxxxxxxxxxxxxxxxxx");
    }

    void _RightHandStopShock()
    {
        _rightShock = false; //关闭手柄的震动
        //Debug.LogWarning("_RightHandStopShock : xxxxxxxxxxxxxxxxxxxxxxx");
    }

    /// <summary>
    /// 取得手位於camera rig中的local位置
    /// </summary>
    void _getHandLocalPose(Matrix4x4 handWorldMatrix, out Vector3 localPos, out Quaternion localRot)
    {
        Matrix4x4 handLocalMatrix = CameraRig.transform.worldToLocalMatrix * handWorldMatrix;
        localPos = handLocalMatrix.GetPosition();
        localRot = handLocalMatrix.GetRotation();
    }

    /// <summary>
    /// 取得手位於camera rig中的local位置
    /// </summary>
    public void RightHandLocalPose(out Vector3 localPos, out Quaternion localRot)
    {
        _getHandLocalPose(RightHand.transform.localToWorldMatrix, out localPos, out localRot);
    }

    /// <summary>
    /// 取得手位於camera rig中的local位置
    /// </summary>
    public void LeftHandLocalPose(out Vector3 localPos, out Quaternion localRot)
    {
        _getHandLocalPose(LeftHand.transform.localToWorldMatrix, out localPos, out localRot);
    }

    public void Fade(Color color, float time)
    {
        if (!CameraEye.GetComponent<SteamVR_Fade>())
        {
            CameraEye.gameObject.AddComponent<SteamVR_Fade>();
        }
        SteamVR_Fade.Start(color, time, true);
    }

    AudioSource _audioSource;
    public AudioSource GetAudioSource()
    {
        if (_audioSource == null)
            _audioSource = CameraEye.gameObject.AddComponent<AudioSource>();
        return _audioSource;
    }

    public bool _rightControllNotDraw;
    public bool _leftControllNotDraw;
    bool _rightControllNotDrawOld = false;
    bool _leftControllNotDrawOld = false;
    public void RightControllNotDraw()
    {
        _rightControllNotDraw = true;
        //if (UseSteamVrPlayer)
        //{   
        //    //RightHand.GetComponent<Hand>().controllerPrefab = null;

        //    /*SteamVR_RenderModel[] rendermodels = RightHand.GetComponentsInChildren<SteamVR_RenderModel>();
        //    foreach (SteamVR_RenderModel rendermodel in rendermodels)
        //        rendermodel.GetComponent<Renderer>().enabled = false;*/
        //}
        //else
        //{
        //    RightHand.GetComponentInChildren<SteamVR_RenderModel>().enabled = false;
        //}
    }

    public void LefttControllNotDraw()
    {
        _leftControllNotDraw = true;

        //if (UseSteamVrPlayer)
        //{
        //    //LeftHand.GetComponent<Hand>().controllerPrefab = null;

        //    /*SteamVR_RenderModel[] rendermodels = RightHand.GetComponentsInChildren<SteamVR_RenderModel>();
        //    foreach (SteamVR_RenderModel rendermodel in rendermodels)
        //        rendermodel.GetComponent<Renderer>().enabled = false;*/
        //}
        //else
        //{
        //    LeftHand.GetComponentInChildren<SteamVR_RenderModel>().enabled = false;
        //}
    }

    /// <summary>
    /// 直接經由手把device取得velocity
    /// </summary>
    public Vector3 RightHandVelocity()
    {
        return _getVelocity(RightHand);
    }

    /// <summary>
    /// 直接經由手把device取得velocity
    /// </summary>
    public Vector3 LeftHandVelocity()
    {
        return _getVelocity(LeftHand);
    }

    Vector3 _getVelocity(Transform handDevice)
    {
        if (handDevice == null)
            return Vector3.zero;

        if (UseSteamVrPlayer)
        {
            Hand hand = handDevice.GetComponent<Hand>();
            if (hand != null && hand.controller != null)
                return hand.controller.velocity;
        }
        else
        {
            return ViveHelp.Velocity(handDevice.GetComponent<SteamVR_TrackedObject>());
        }
        return Vector3.zero;
    }

    public bool IsRightHandSwing()
    {
        Vector3 handVelocity = RightHandVelocity();
        return _checkHandSwing(ref handVelocity, ref _swingOldVelocityR, ref _isSwingStartR,
            SwingVelocitythreshold);
    }

    public bool IsLeftHandSwing()
    {
        Vector3 handVelocity = LeftHandVelocity();
        return _checkHandSwing(ref handVelocity, ref _swingOldVelocityL, ref _isSwingStartL,
            SwingVelocitythreshold);
    }

    float _swingOldVelocityR, _swingOldVelocityL;
    bool _isSwingStartR, _isSwingStartL;
    public float SwingVelocitythreshold = 0.01f;
    static bool _checkHandSwing(ref Vector3 handVelocity, ref float oldVelocity, ref bool _isSwingStart, float swingThreshold)
    {
        ///拿取RB速度
        float velocity = handVelocity.sqrMagnitude;

        ///計算與上個farme的速度差
        float velocitygap = (velocity - oldVelocity) * Time.deltaTime;

        //if (velocitygap > 0.001)
        //Debug.Log(velocitygap);

        ///如果大於的閥值
        if (velocitygap >= swingThreshold && !_isSwingStart)
        {
            //Debug.LogWarning("速度大於速度閥值");
            _isSwingStart = true;

            //if (PrefabManager.Instance != null &&
            //    PrefabManager.Instance.MonkeykingBarSwingAudio.Length > 0
            //    )
            //{
            //    AudioSource AS = VRManager.Instance.GetAudioSource();
            //    int rand = UnityEngine.Random.Range(0, PrefabManager.Instance.MonkeykingBarSwingAudio.Length);
            //    AudioClip audioC = PrefabManager.Instance.MonkeykingBarSwingAudio[rand];
            //    AS.PlayOneShot(audioC, 0.4f);
            //}
        }
        else if (velocitygap <= 0)
        {
            _isSwingStart = false;
        }

        oldVelocity = velocity;
        return _isSwingStart;
    }

    /// <summary>
    /// 取得eye位於空間內的相對座標
    /// </summary>
    /// <returns></returns>
    public Vector3 GetEyeHeight()
    {
        return CameraEye.position - CameraRig.transform.position;
    }
}

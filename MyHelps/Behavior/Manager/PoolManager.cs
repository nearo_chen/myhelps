﻿using EZObjectPools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MySingleton<PoolManager>
{
    public bool ShowDebug;

    //key GameObject 為prefab的指標
    Dictionary<GameObject, MyPoolData> PoolList = new Dictionary<GameObject, MyPoolData>();
    GameObject Marker;

    public class MyPoolData
    {
        //public GameObject prefab;//key 就是 prefab
        public EZObjectPool pool;
        //public Behaviour[] needComponents;//紀錄pool物件應該要存在那些component(包括管理pool的script)
        public GameObject instantiatedPrefab;//把对象池里的第一个实例放进Dada里，重生时用来比较。不能用Prefab，因为Prefab没有生出Skin

        public Dictionary<GameObject, List<int>> originalObjList = new Dictionary<GameObject, List<int>>();//紀錄pool中每個物件原始的gameObject
    }

    /// <summary>
    /// (作初始化並)取得Pool
    /// </summary>
    public MyPoolData GetObjectPool(GameObject prefab, int poolSize = 10)
    {
        MyPoolData data;
        if (!PoolList.TryGetValue(prefab, out data))
        {
            EZObjectPool objectPool = EZObjectPool.CreateObjectPool(prefab, prefab.name, poolSize, false, true, true);//不做autoresize

            data = new MyPoolData();

            foreach (GameObject poolObj in objectPool.ObjectList)
            {
                data.originalObjList.Add(poolObj, new List<int>(MyHelpNode.GetFlattenListHash(poolObj.transform)));
            }

            //data.prefab = prefab;
            data.pool = objectPool;
            //data.needComponents = objectPool.ObjectList[0].GetComponents<Behaviour>();//紀錄物件最原始身上需要的component，建立好pool之後才去做紀錄(包括管理pool的script)
            if (Marker == null)
            {
                Marker = new GameObject("Object Pool Templates");
            }
            GameObject g = Instantiate(objectPool.ObjectList[0]);
            g.SetActive(false);
            g.transform.parent = Marker.transform;
            data.instantiatedPrefab = g;
            //foreach (Behaviour b in data.needComponents)
            //    Debug.LogWarning("[" + data.prefab.name + "] : " + b.GetType() + " , " + ((b.enabled) ? "On" : "Off"));

            PoolList.Add(prefab, data);
        }
        return data;
    }

    public GameObject Spawn(GameObject prefab)
    {
        return Spawn(prefab, Vector3.zero, Quaternion.identity);
    }

    public GameObject Spawn(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        MyPoolData data = GetObjectPool(prefab);
        EZObjectPool objectPool = data.pool;
        GameObject obj;
        if (!objectPool.TryGetNextObject(pos, rot, out obj))
        {
            Debug.LogError("[PoolManager] EZObjectPool : " + prefab.name + " , spawn failed : " + prefab.name);
        }

        //根據樣板怪物移除多餘的gameobject
        //MyHelpNode.CompareTreeObject(data.instantiatedPrefab.transform, obj.transform);

        //複製prefab的pose，給生出來的怪，還原default值
        //MyHelpNode.CopyTreePose(data.instantiatedPrefab.transform, obj.transform);//, Matrix4x4.TRS(pos, rot, Vector3.one));

        //RagDollSetting._setAllRigidBodySleep(obj.transform, true);//設定sleep避免之前的力影響到重生物件的座標
        //RagDollSetting.CopyTreeRigidbody(data.instantiatedPrefab.transform, obj.transform);

        //_compareComponent(data.instantiatedPrefab.transform, obj.transform);

        //如果有使用ragdoll要注意updateMode要改回來，不然重生的怪物會被RB拖走
        //Animator animator = obj.GetComponentInChildren<Animator>();
        //if (animator != null)
        //{
        //    animator.updateMode = AnimatorUpdateMode.Normal;
        //    animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
        //    animator.enabled = true;
        //}
        obj.name = prefab.name;
        obj.transform.position = pos;
        obj.transform.rotation = rot;
        return obj;
    }

    /// <summary>
    /// 檢查prefab的怪，與回收重生的怪身上的component開關,檢查prefab的怪，與回收重生的怪身上的component是否對印，要是有多餘的就把該component destroy
    /// </summary>
    void _compareComponent(Transform src, Transform dest, bool destroyImmediate = true)
    {
        Behaviour[] componentsSrc = src.GetComponents<Behaviour>();
        Behaviour[] componentsDest = dest.GetComponents<Behaviour>();
        List<Behaviour> removeList = new List<Behaviour>(componentsDest);
        for (int a = 0; a < componentsSrc.Length; a++)
        {
            if (componentsSrc[a].GetType() != componentsDest[a].GetType())
            {
                Debug.LogError("[PoolManager] respwn onject's component not correspond...");
                continue;
            }

            componentsDest[a].enabled = componentsSrc[a].enabled;
            removeList.Remove(componentsDest[a]);
        }

        //把之前程序另外加上去的component移除
        while (removeList.Count > 0)
        {
            if (destroyImmediate)
                DestroyImmediate(removeList[0]);//生出的當下絕對不能用Destroy
            else
                Destroy(removeList[0]);
            removeList.RemoveAt(0);
        }
    }

    List<GameObject> LaterDeSpawnList = new List<GameObject>();
    public void DeSpawn(GameObject obj, float sec)
    {
        if (sec > 0)
        {
            LaterDeSpawnList.Add(obj);
            StartCoroutine(LateCall(obj, sec));
        }
        else
        {
            if (LaterDeSpawnList.IndexOf(obj) >= 0)
                Debug.LogError("[PoolManager][DeSpawn] 要直接刪除的物件，已經在稍後刪除清單中，如果現在直接刪除，會被別人拿去用，然後再被別人刪除");

            _deactiveObj(obj);
        }
    }

    IEnumerator LateCall(GameObject obj, float sec)
    {
        //RagDollSetting._resetAllRigidBody(obj.transform);

        yield return new WaitForSeconds(sec);

        LaterDeSpawnList.Remove(obj);

        _deactiveObj(obj);
    }

    /// <summary>
    /// 從Parent pool中找出MyPoolData
    /// </summary>
    MyPoolData _findPoolData(EZObjectPool parentPool)
    {
        MyPoolData find = null;
        Dictionary<GameObject, MyPoolData>.Enumerator itr = PoolList.GetEnumerator();
        while (itr.MoveNext())
        {
            if (itr.Current.Value.pool == parentPool)
            {
                find = itr.Current.Value;
                break;
            }
        }
        return find;
    }

    void _removeOtherObjects(List<int> checkHashList, GameObject obj)
    {
        List<Transform> outFlattenList = new List<Transform>();
        MyHelpNode.GetFlattenList(obj.transform, ref outFlattenList);

        List<GameObject> removeList = new List<GameObject>();
        foreach (Transform t in outFlattenList)
        {
            if (checkHashList.IndexOf(t.gameObject.GetHashCode()) < 0)
            {
                removeList.Add(t.gameObject);
            }
        }
        while (removeList.Count > 0)
        {
            Debug.Log("[PoolManager][Despawn] Remove object by pool : " + removeList[0].name + ", hash : " + removeList[0].GetHashCode());
            Destroy(removeList[0]);
            removeList.RemoveAt(0);
        }
    }

    void _deactiveObj(GameObject obj)
    {
        PooledObject poolObj = obj.GetComponent<PooledObject>();
        MyPoolData poolData = _findPoolData(poolObj.ParentPool);

        //根據樣板怪物移除多餘的gameobject
        _removeOtherObjects(poolData.originalObjList[obj], obj);
        //MyHelpNode.CompareTreeObject(poolData.instantiatedPrefab.transform, obj.transform);

        //複製prefab的pose，給生出來的怪，還原default值
        MyHelpNode.CopyTreePose(poolData.instantiatedPrefab.transform, obj.transform);//, Matrix4x4.TRS(pos, rot, Vector3.one));

        //RagDollSetting._setAllRigidBodySleep(obj.transform, true);//設定sleep避免之前的力影響到重生物件的座標
        RagDollSetting.CopyTreeRigidbody(poolData.instantiatedPrefab.transform, obj.transform);

        _compareComponent(poolData.instantiatedPrefab.transform, obj.transform, false);

        Animator animator = obj.GetComponentInChildren<Animator>();
        if (animator != null)
        {
            animator.updateMode = AnimatorUpdateMode.Normal;
            animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            animator.enabled = true;
        }

        //如果有被亂parent，把他還回去
        obj.transform.parent = poolObj.ParentPool.transform;

        obj.SetActive(false);
    }

    void OnGUI()
    {
#if !UNITY_EDITOR
        return;
#endif
        if (!ShowDebug)
            return;
        GUI.Box(new Rect(10, 10, 250, 50 + PoolList.Count * 35), "Object Pool List");
        GUILayout.BeginArea(new Rect(29f, 40, 215, Screen.height - 40));

        Dictionary<GameObject, MyPoolData>.Enumerator itr = PoolList.GetEnumerator();
        while (itr.MoveNext())
        {
            EZObjectPool e = itr.Current.Value.pool;
            GUILayout.Label(e.PoolName + ": Size - " + e.PoolSize + ", " + e.AvailableObjectCount() + " available objects. ");
        }

        GUILayout.EndArea();
    }

    /// <summary>
    /// 檢查此物件是否有作Pooling
    /// </summary>
    public bool IsInPool(GameObject obj)
    {
        Dictionary<GameObject, MyPoolData>.Enumerator itr = PoolList.GetEnumerator();
        while (itr.MoveNext())
        {
            EZObjectPool e = itr.Current.Value.pool;
            if (e.ObjectList.Contains(obj))
                return true;
        }
        return false;
    }
}

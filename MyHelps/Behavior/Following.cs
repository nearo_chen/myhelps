﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Following : MonoBehaviour
{
    public GameObject followingTarget;
    Vector3 localPos;
    Quaternion localRot;

    void Awake()
    {
        localPos = transform.localPosition;
        localRot = transform.localRotation;
    }

    void Start()
    {
        SetTarget(followingTarget);
    }

    public void SetTarget(GameObject target)
    {
        followingTarget = target;
        name += "_following:" + target.GetInstanceID() + "";
        Update();
    }

#if UNITY_EDITOR
    public string DebugText;
#endif
    void Update()
    {
#if UNITY_EDITOR
        if (followingTarget != null)
            DebugText = "followingTarget : " + followingTarget + " , activeInHierarchy : " + followingTarget.activeInHierarchy;
#endif
        if (followingTarget == null ||
            //followingTarget.gameObject == null ||
            !followingTarget.activeInHierarchy)
        {
            Destroy(gameObject);
        }
        else
        {
            Matrix4x4 mat = followingTarget.transform.localToWorldMatrix * Matrix4x4.TRS(localPos, localRot, Vector3.one);
            transform.position = mat.GetPosition();
            transform.rotation = mat.GetRotation();
            transform.localScale = mat.GetScale();//unity5.3以后粒子系统把Scaling Mode改成hierarchy就可以一起缩放了
        }
    }

    //public static Matrix4x4 CombineMatrix(Transform local, Transform parent)
    //{
    //    Matrix4x4 mat = Matrix4x4.TRS(local.localPosition, local.localRotation, Vector3.one) * parent.localToWorldMatrix;
    //    return mat;
    //}
}

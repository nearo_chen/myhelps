﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseEmitterAfterTime : MonoBehaviour {

    public float Closetime;
	// Use this for initialization
	void Start () {
        StartCoroutine(DelayClose(Closetime));
	}
	IEnumerator DelayClose(float Closetime)
    {
        yield return new WaitForSeconds(Closetime);
        //所有PS都關掉
        ParticleSystem[] pss = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem p in pss)
        {
            var em = p.emission;
            em.enabled = false;
            //em.rateOverTime = 0.0f;
            ParticleSystem.MainModule ppp = p.main;
            ppp.loop = false;       
        }
        //關閉emission後掛上ParticleSystemAutoDestroy
        gameObject.AddComponent<ParticleSystemAutoDestroy>();

    }

}

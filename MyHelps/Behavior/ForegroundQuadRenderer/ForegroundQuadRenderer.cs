﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 此class 拿來處理將某camera的render target，永遠蓋在camera最上面
/// </summary>
public class ForegroundQuadRenderer : MonoBehaviour
{
    public Shader ForegroundQuadShader;

    [Tooltip("以proflare為例，畫lensflare mesh的camera")]
    public Camera LensFlareCamera;

    [Tooltip("以proflare為例，為ProFlareBatch")]
    public Transform LensflareSceneRoot;

    //[Tooltip("把Quad畫在最上層的Camera")]
    //public Camera VRForegroundQuadCamera;
    public GameObject VRCameraRig;
    Camera _vrForegroundQuadCamera;

    //The cameras render scene in front of the quad , which clear flag="depth only".
    //[Tooltip("傳入其他畫場景的camera，做cullingmask的設定")]
    //public Camera[] MainCameraRemoveCullingmask;

    RenderTexture _LensflareRT;
    MeshRenderer _quadMesh;

    void Start()
    {
        //Duplicate a VR foreground camera
        Transform findHead = null;
        MyHelpNode.FindTransform(VRCameraRig.transform, "head", ref findHead, true);
        GameObject foregroundCameraHead = Instantiate(findHead.gameObject);
        List<Transform> allchild = new List<Transform>();
        MyHelpNode.GetAllComponents(foregroundCameraHead.transform, ref allchild);
        foreach (Transform tran in allchild)
        {
            if (!tran.name.Contains("head") && !tran.name.Contains("eye"))
            {
                Destroy(tran.gameObject);
                continue;
            }

            tran.name += "_lensflare";
            if (tran.name.Contains("eye"))
            {
                _vrForegroundQuadCamera = tran.GetComponent<Camera>();
            }
        }

        //set render mask
        int LensflareSceneLayer = LayerMask.NameToLayer("LensflareScene");
        int ForegroundQuadLayer = LayerMask.NameToLayer("ForegroundQuad");

        if (LensflareSceneLayer < 0)
        {
            Debug.LogError("You need to add a 'LensflareScene' layer for your forground object");
            return;
        }

        if (ForegroundQuadLayer < 0)
        {
            Debug.LogError("You need to add a 'ForegroundQuad' layer");
            return;
        }

        Debug.Log("[ForegroundQuadRenderer] ForegroundQuadLayer: " + ForegroundQuadLayer);

        int screenW = Screen.width;
        int screenH = Screen.height;
        if (VRCameraRig != null && SteamVR.instance != null)
        {
            screenW = (int)(SteamVR.instance.sceneWidth);
            screenH = (int)(SteamVR.instance.sceneHeight);
        }

        _LensflareRT = new RenderTexture(screenW, screenH, 24);
        _LensflareRT.antiAliasing = 1;
        _LensflareRT.filterMode = FilterMode.Point;
        _LensflareRT.useMipMap = false;
        _LensflareRT.anisoLevel = 0;

        //設定畫lensflare的camera，還有lensflare本身的culling mask，
        //The camera to render foreground scene in RT
        MyHelpNode.SetSceneLayer(LensflareSceneRoot, LensflareSceneLayer);
        LensFlareCamera.clearFlags = CameraClearFlags.SolidColor;
        LensFlareCamera.backgroundColor = new Color(0, 0, 0, 0);
        LensFlareCamera.targetTexture = _LensflareRT;
        LensFlareCamera.cullingMask = 1 << LensflareSceneLayer;
        LensFlareCamera.depth = -10;

        //設定main camera的culling mask
        Transform mainCameraTran = null;
        MyHelpNode.FindTransform(VRCameraRig.transform, "eye", ref mainCameraTran, true);
        Camera mainCamera = mainCameraTran.GetComponent<Camera>();
        if (mainCamera.cullingMask == 0xff)
            Debug.LogError("[BackgroundQuadRenderer] main camera should not draw everything!! -> " + mainCamera.name);
        else
        {
            mainCamera.cullingMask &= ~(1 << ForegroundQuadLayer | 1 << LensflareSceneLayer);//remove draw layer "BackgroundQuad" & "BackgroundSpaceScene"
            mainCamera.name += "_cull[" + LayerMask.LayerToName(ForegroundQuadLayer) + "_" + LayerMask.LayerToName(LensflareSceneLayer) + "]";
        }

        //foreach (Camera c in MainCameraRemoveCullingmask)
        //{
        //    if (c.enabled)
        //    {
        //        if (c.cullingMask == 0xff)
        //            Debug.LogError("[BackgroundQuadRenderer] main camera should not draw everything!! -> " + c.name);
        //        else
        //            c.cullingMask &= ~(1 << ForegroundQuadLayer | 1 << LensflareSceneLayer);//remove draw layer "BackgroundQuad" & "BackgroundSpaceScene"
        //    }
        //}

        GameObject quadCameraOBJ = _vrForegroundQuadCamera.gameObject;

        _vrForegroundQuadCamera.clearFlags = CameraClearFlags.Depth; //CameraClearFlags.Color;
        _vrForegroundQuadCamera.cullingMask = 1 << ForegroundQuadLayer;
        _vrForegroundQuadCamera.backgroundColor = new Color(1, 0, 0, 0);//Color.red;
        _vrForegroundQuadCamera.orthographic = true;
        _vrForegroundQuadCamera.nearClipPlane = 0.3f;
        _vrForegroundQuadCamera.farClipPlane = 0.5f;
        _vrForegroundQuadCamera.depth = 10;
        _vrForegroundQuadCamera.renderingPath = RenderingPath.VertexLit;
        _vrForegroundQuadCamera.useOcclusionCulling = false;
        _vrForegroundQuadCamera.hdr = false;

        GameObject quadOBJ = GameObject.CreatePrimitive(PrimitiveType.Quad);
        quadOBJ.transform.parent = quadCameraOBJ.transform;
        quadOBJ.transform.localPosition = Vector3.zero;
        quadOBJ.transform.localRotation = Quaternion.identity;
        quadOBJ.transform.localPosition = new Vector3(0, 0, 0.4f);//nearClipPlane = 0.3f, farClipPlane = 0.5f;
        quadOBJ.layer = ForegroundQuadLayer;

        Destroy(quadOBJ.GetComponent<MeshCollider>());//remove physX obj

        _quadMesh = quadOBJ.GetComponent<MeshRenderer>();
        _quadMesh.material = new Material(ForegroundQuadShader);// Shader.Find("Hidden/ForegroundQuadShader"));
        _quadMesh.material.SetTexture("_MainTex", _LensflareRT);
        _quadMesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        _quadMesh.receiveShadows = false;
        _quadMesh.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

#if UNITY_5_4
        _quadMesh.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
#else
        _quadMesh.useLightProbes = false;
#endif
    }

    //public static void SaveToFile(RenderTexture renderTexture, string name)
    //{
    //    RenderTexture currentActiveRT = RenderTexture.active;
    //    RenderTexture.active = renderTexture;
    //    Texture2D tex = new Texture2D(renderTexture.width, renderTexture.height);
    //    tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
    //    var bytes = tex.EncodeToPNG();
    //    System.IO.File.WriteAllBytes(name, bytes);
    //    UnityEngine.Object.Destroy(tex);
    //    RenderTexture.active = currentActiveRT;
    //}

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    SaveToFile(_LensflareRT, "d:/123.png");
        //}

        int screenW = Screen.width;
        int screenH = Screen.height;
        if (VRCameraRig != null && SteamVR.instance != null)
        {
            screenW = (int)(SteamVR.instance.sceneWidth);
            screenH = (int)(SteamVR.instance.sceneHeight);
        }

        if (_LensflareRT.width != screenW || _LensflareRT.height != screenH)
        {
            Debug.Log("[ForegroundQuadRenderer] Release RT");
            _LensflareRT.Release();
            _LensflareRT.width = screenW;
            _LensflareRT.height = screenH;
        }

        if (!_LensflareRT.IsCreated())
        {
            _LensflareRT.antiAliasing = 1;
            _LensflareRT.filterMode = FilterMode.Point;
            _LensflareRT.Create();

            //LensFlareCamera.clearFlags = CameraClearFlags.Color;
            //LensFlareCamera.backgroundColor = new Color(0, 0, 0, 0);

            LensFlareCamera.pixelRect = new Rect(0, 0, screenW, screenH);
            _quadMesh.transform.localScale = new Vector3(1, (float)screenH / (float)screenW, 1);
            _vrForegroundQuadCamera.orthographicSize = (float)screenH / (float)screenW * 0.5f;

            Debug.Log("Create rendertarget width: " + screenW + " , height: " + screenH);
        }
        //Debug.Log("_sceneCamera.pixelRect : " + _sceneCamera.pixelRect);
        //Debug.Log("screenW : " + Screen.width + " , screenH : " + Screen.height + " , aspect : " + _sceneCamera.aspect);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCombineRT : MonoBehaviour
{
    public RenderTexture RTL;
    public RenderTexture RTR;
    public CopyCameraDepthColor copyRT;
    public Material mat;
    public MeshRenderer debugRender;
    public Camera _cam;
    Material _mat;

    // Start is called before the first frame update
    void Start()
    {
        _cam = GetComponent<Camera>();
        _mat = new Material(mat);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (copyRT != null)
        {
            _mat.SetTexture("_CombineTex", copyRT.GetRT(_cam.stereoActiveEye));
            Graphics.Blit(null, destination, _mat);
        }
        else
        {
            _mat.SetTexture("_CameraTex", source);
            if (_cam.stereoActiveEye == Camera.MonoOrStereoscopicEye.Left)
                _mat.SetTexture("_CombineTex", RTL);
            else if (_cam.stereoActiveEye == Camera.MonoOrStereoscopicEye.Right)
                _mat.SetTexture("_CombineTex", RTR);

            Graphics.Blit(null, destination, _mat);
        }

        if (debugRender != null)
        {
            debugRender.material.mainTexture = destination;
        }
    }
}

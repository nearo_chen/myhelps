﻿Shader "MyHelp/CameraCombineRT"
{
    Properties
    {
        _CameraTex ("Texture", 2D) = "white" {}
        _CombineTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _CameraTex, _CombineTex;

            fixed4 frag (v2f_img i) : SV_Target
            {
                // sample the texture
                float4 colCamera = tex2D(_CameraTex, i.uv);
                float4 colCombine = tex2D(_CombineTex, i.uv);
                float3 final = lerp(colCombine.rgb, colCamera.rgb, colCamera.a);
                return float4(final, 1);
            }
            ENDCG
        }
    }
}

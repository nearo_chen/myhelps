﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyCameraDepthColor : MonoBehaviour
{
    public bool USE_FLOAT32 = true;
    public bool USE_2RT = false;
    public bool BlitSource;
    Camera currentCamera;
    public RenderTexture[] ColorDepthLRT = new RenderTexture[1];
    public RenderTexture[] ColorDepthRRT = new RenderTexture[1];
    public RenderTexture DepthRT;

    public Material CopyCameraDepthColorMaterial;
    public Material /*CopyCameraColorMaterial, */CopyCameraDepthMaterial;

    public MeshRenderer debugRender;
    const int delayFrame = 0;



    public RenderTexture GetRT(Camera.MonoOrStereoscopicEye? eye = null)
    {
        if (eye.HasValue && USE_2RT)
            return (eye.Value == Camera.MonoOrStereoscopicEye.Left) ? ColorDepthLRT[delayFrame] : ColorDepthRRT[delayFrame];
        else
            return ColorDepthLRT[delayFrame];
    }

    private void Start()
    {
        Init(USE_2RT, BlitSource, USE_FLOAT32);
    }

    public void Init(bool use2RT = false, bool blitSource = false, bool use_float32 = true)
    {
        if (currentCamera != null)
            return;

        USE_FLOAT32 = use_float32;
        USE_2RT = use2RT;
        BlitSource = blitSource;

        currentCamera = GetComponent<Camera>();
        currentCamera.depthTextureMode = DepthTextureMode.Depth;

        RenderTextureFormat rtformat = (USE_FLOAT32) ? RenderTextureFormat.ARGBFloat : RenderTextureFormat.ARGB32;//RGB565;

        for (int a = 0; a < ColorDepthLRT.Length; a++)
        {
            ColorDepthLRT[a] = new RenderTexture(currentCamera.pixelWidth, currentCamera.pixelHeight, 0, rtformat);
            ColorDepthLRT[a].antiAliasing = 1;
            ColorDepthLRT[a].filterMode = FilterMode.Point;
            ColorDepthLRT[a].useMipMap = false;
            ColorDepthLRT[a].anisoLevel = 0;
        }
        if (USE_2RT)
        {
            for (int a = 0; a < ColorDepthRRT.Length; a++)
            {
                ColorDepthRRT[a] = new RenderTexture(currentCamera.pixelWidth, currentCamera.pixelHeight, 0, rtformat);
                ColorDepthRRT[a].antiAliasing = 1;
                ColorDepthRRT[a].filterMode = FilterMode.Point;
                ColorDepthRRT[a].useMipMap = false;
                ColorDepthRRT[a].anisoLevel = 0;
            }
        }

        if (!USE_FLOAT32 && CopyCameraDepthMaterial != null)
        {
            DepthRT = new RenderTexture(currentCamera.pixelWidth, currentCamera.pixelHeight, 0, RenderTextureFormat.RFloat);
            DepthRT.antiAliasing = 1;
            DepthRT.filterMode = FilterMode.Point;
            DepthRT.useMipMap = false;
            DepthRT.anisoLevel = 0;
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RenderTexture colorDepthRT = null;

        if (USE_2RT)
        {
            if (currentCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Left)
            {
                if (delayFrame == 0)
                {
                    colorDepthRT = ColorDepthLRT[0];
                }
                else
                {
                    RenderTexture recLast = ColorDepthLRT[ColorDepthLRT.Length - 1];
                    Array.Copy(ColorDepthLRT, 0, ColorDepthLRT, 1, ColorDepthLRT.Length - 1);
                    ColorDepthLRT[0] = recLast;
                    colorDepthRT = ColorDepthLRT[0];
                }
                //if (current != 0)
                //    Debug.LogError("0");
                //current = 1;
                //Debug.LogWarning("[CopyCameraColorDepth] copy L");
            }
            else if (currentCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Right)
            {
                if (delayFrame == 0)
                {
                    colorDepthRT = ColorDepthRRT[0];
                }
                else
                {
                    RenderTexture recLast = ColorDepthRRT[ColorDepthRRT.Length - 1];
                    Array.Copy(ColorDepthRRT, 0, ColorDepthRRT, 1, ColorDepthRRT.Length - 1);
                    ColorDepthRRT[0] = recLast;
                    colorDepthRT = ColorDepthRRT[0];
                }

                //if (current != 1)
                //    Debug.LogError("1");
                //current = 0;
                //Debug.LogWarning("[CopyCameraColorDepth] copy R");
            }
            else
            {
                Debug.LogError("[CopyCameraDepthColor] copy Failed");
                return;
            }
        }
        else
        {
            colorDepthRT = ColorDepthLRT[delayFrame];
        }

        if (USE_FLOAT32)
        {
            Graphics.Blit(source, colorDepthRT, CopyCameraDepthColorMaterial);
        }
        else
        {
            Graphics.Blit(source, colorDepthRT);//only blit color
            if (CopyCameraDepthMaterial != null)
                Graphics.Blit(source, DepthRT, CopyCameraDepthMaterial);
        }

        if (debugRender != null)
            debugRender.material.mainTexture = colorDepthRT;

        if (BlitSource)
            Graphics.Blit(source, destination);
    }
}

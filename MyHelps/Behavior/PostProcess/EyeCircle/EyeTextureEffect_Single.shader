﻿Shader "Custom/EyeTextureEffect_Single"
{
    Properties
    {
        //_Color("Color", Color) = (1,1,1,1)        
        _Alpha("alpha", Float) = 1
        _MainTex("Texture", 2D) = "white" {}
        _EyeTex("Texture", 2D) = "white" {}
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            //https://docs.unity3d.com/Manual/SinglePassInstancing.html
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex, _EyeTex;

            //LOOK! The texture name + ST is needed to get the tiling/offset
            uniform float4 _EyeTex_ST;
            float4 StartPosScreen01[2];
            float TexW, TexH;
            //float4 _Color;
            float _Alpha;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 uv = UnityStereoTransformScreenSpaceTex(i.uv);
                float4 sourceColor = tex2D(_MainTex, uv);

                float2 eyePos = StartPosScreen01[unity_StereoEyeIndex];
                float offsetX = uv.x - eyePos.x;
                float offsetY = uv.y - eyePos.y;

                //if (abs(offsetX) > TexW * 0.5 || abs(offsetY) > TexH*0.5)
                //discard;

                float2 pivot = eyePos - float2(TexW * 0.5, TexH * 0.5);

                float2 eyeTexUV = float2(offsetX + TexW * 0.5, offsetY + TexH * 0.5);
                eyeTexUV = eyeTexUV / float2(TexW, TexH);
                //eyeTexUV.x = clamp(eyeTexUV.x, pivot.x, pivot.x + TexW);
                //eyeTexUV.y = clamp(eyeTexUV.y, pivot.y, pivot.y + TexH);
                eyeTexUV = eyeTexUV * _EyeTex_ST.xy + _EyeTex_ST.zw;

                float4 col = tex2D(_EyeTex, eyeTexUV);

                col.a *= 
                    (abs(offsetX) <= TexW * 0.5) * (abs(offsetY) <= TexH * 0.5);

                float a = col.a * _Alpha;
                float3 final = col.rgb * a + sourceColor.rgb * (1 - a);
                return float4(final, 1);
            }
            ENDCG
        }
    }
}

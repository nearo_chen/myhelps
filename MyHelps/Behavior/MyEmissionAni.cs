﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEmissionAni : MonoBehaviour
{
    public Material material;
    //public string ColorName;
    public Color ColorStart, ColorEnd;
    public float RatioStart, RatioEnd;
    public float speed;
    public float pow = 1;
    public bool InvertRatio;
    
    void Update()
    {
        float ratio = (Mathf.Sin(Time.time * speed) + 1.0f) * 0.5f;
        ratio = Mathf.Pow(ratio, pow);
        ratio = (InvertRatio) ? (1 - ratio) : ratio;
        
        Color start = ColorStart * RatioStart;
        start.a = 0;
        Color end = ColorEnd * RatioEnd;
        end.a = 0;
        material.SetColor("_EmissionColor", Color.Lerp(start, end, ratio));
    }
}

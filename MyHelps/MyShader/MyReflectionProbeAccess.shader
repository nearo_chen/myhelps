﻿/*Please do support www.bitshiftprogrammer.com by joining the facebook page : fb.com/BitshiftProgrammer
Legal Stuff:
This code is free to use no restrictions but attribution would be appreciated.
Any damage caused either partly or completly due to usage this stuff is not my responsibility*/
Shader "Custom/MyReflectionProbeAccess"
{
	Properties
	{
		_Roughness("Roughness", Range(0.0, 10.0)) = 0.0
		_ReflectRatio("_ReflectRatio", Range(0.0, 1)) = 0.1
	}

		SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float _Roughness, _ReflectRatio;
	
			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float3 worldPos : TEXCOORD0;
				float3 worldNormal : TEXCOORD1;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}

			//https://zhuanlan.zhihu.com/p/35495074			
			inline half3 BoxProjectedCubemapDirection(half3 worldRefl, float3 worldPos, float4 cubemapCenter, float4 boxMin, float4 boxMax)
			{
				// Do we have a valid reflection probe?
				UNITY_BRANCH
					if (cubemapCenter.w > 0.0)
					{
						half3 nrdir = normalize(worldRefl);

						half3 rbmax = (boxMax.xyz - worldPos) / nrdir;
						half3 rbmin = (boxMin.xyz - worldPos) / nrdir;

						half3 rbminmax = (nrdir > 0.0f) ? rbmax : rbmin;

						half fa = min(min(rbminmax.x, rbminmax.y), rbminmax.z);

						worldPos -= cubemapCenter.xyz;
						worldRefl = worldPos + nrdir * fa;
					}
				return worldRefl;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos)); //Direction of ray from the camera towards the object surface
				half3 reflection = reflect(-worldViewDir, i.worldNormal); // Direction of ray after hitting the surface of object
				reflection = BoxProjectedCubemapDirection(
					reflection, i.worldPos,
					unity_SpecCube0_ProbePosition,
					unity_SpecCube0_BoxMin,
					unity_SpecCube0_BoxMax);

				/*If Roughness feature is not needed : UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, reflection) can be used instead.
				It chooses the correct LOD value based on camera distance*/				
				half4 skyData = 
					//UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, reflection);
					UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflection, _Roughness);
				half3 skyColor = DecodeHDR(skyData, unity_SpecCube0_HDR); // This is done becasue the cubemap is stored HDR

				skyColor = lerp(float3(1, 1, 1), skyColor, _ReflectRatio);
				return half4(skyColor, 1);
			}
			ENDCG
		}
	}
}
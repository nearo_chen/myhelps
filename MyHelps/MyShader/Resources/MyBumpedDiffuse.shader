//https://gamedev.stackexchange.com/questions/165760/how-to-add-emission-to-texture-in-shader
Shader "Mobile/My Bumped Diffuse" {
Properties{
    _MainTex("Base (RGB)", 2D) = "white" {}
    [NoScaleOffset] _BumpMap("Normalmap", 2D) = "bump" {}
    _Color("Color", Color) = (1, 1, 1, 1)
        //_Emission("Emission", float) = 0
		_EmissionMap ("Emission Map", 2D) = "black" {}
[HDR] _EmissionColor ("Emission Color", Color) = (0,0,0)
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Culling", int) = 2						// def: back
}

    SubShader{
        Tags{ "Queue" = "Transparent" "RenderType" = "Transparency" }
        LOD 250
		Cull [_CullMode]
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

    CGPROGRAM
    #pragma surface surf Lambert noforwardadd alpha:fade

    sampler2D _MainTex;
    sampler2D _BumpMap, _EmissionMap;
    fixed4 _Color, _EmissionColor;
    float _Emission;

    struct Input {
        float2 uv_MainTex;
    };

    void surf(Input IN, inout SurfaceOutput o) {
        fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
        o.Albedo = c.rgb;
        o.Alpha = c.a;
        o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
        o.Emission = tex2D(_EmissionMap, IN.uv_MainTex) * _EmissionColor;
    }
    ENDCG
    }

        FallBack "Mobile/Diffuse"
		}
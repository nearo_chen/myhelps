﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitToSwitchState<T>
{
    MonoBehaviour _behaviour;
    StatePatternBase _statePatternBase;
    public WaitToSwitchState(MonoBehaviour behaviour, StatePatternBase statePatternBase)
    {
        _behaviour = behaviour;
        _statePatternBase = statePatternBase;
    }

    string recWait2State;
    Coroutine _wait2State;
    public void SwitchState(T stateName, float delaySec = -1, bool unscaleTime = false)
    {
        if (delaySec > 0)
        {
            if (_wait2State != null)
            {
                _behaviour.StopCoroutine(_wait2State);
                Debug.LogWarning("[WaitToSwitchState] cancel state : " + recWait2State);
                _wait2State = null;
            }
            recWait2State = stateName.ToString();
            _wait2State = _behaviour.StartCoroutine(_waitToSwitchState(stateName, delaySec, unscaleTime));
        }
        else
            _statePatternBase.SwitchState(stateName);
    }

    IEnumerator _waitToSwitchState(T stateName, float delaySec, bool unscaleTime)
    {
        if (delaySec < 0)
        {
            yield return new WaitForEndOfFrame();
        }
        else
        {
            if (unscaleTime)
                yield return new WaitForSecondsRealtime(delaySec);
            else
                yield return new WaitForSeconds(delaySec);
        }

        _statePatternBase.SwitchState(stateName);
    }
}

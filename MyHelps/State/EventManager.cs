﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventManager
{
    void AddEvent(string name, Action<object> action);
    void RemoveEvent(string name, Action<object> action);
    void InvokeEvent(string name, object obj = null);
}

public class EventManager : MySingleton<EventManager>, IEventManager
{
    void Start()
    {        
    }

    void Update()
    {        
    }

    void IEventManager.AddEvent(string name, Action<object> action)
    {
        if (!_events.ContainsKey(name))
        {
            _events.Add(name, action);
        }
        else
        {
            _events[name] += action;
        }
    }

    void IEventManager.RemoveEvent(string name, Action<object> action)
    {
        if (_events.ContainsKey(name))
            _events[name] -= action;
    }

    void IEventManager.InvokeEvent(string name, object obj)
    {
        _events[name].Invoke(obj);
    }

    public enum EventName
    {
        BulletShoot
        /*BulletHitEnemy*/,
    }
    public void AddEvent(EventName name, Action<object> action)
    {
        string sname = name.ToString();
        if (!_events.ContainsKey(sname))
        {
            _events.Add(sname, action);
        }
        else
        {
            _events[sname] += action;
        }
    }
    public void RemoveEvent(EventName name, Action<object> action)
    {
        string sname = name.ToString();
        if (_events.ContainsKey(sname))
            _events[sname] -= action;
    }
    public void InvokeEvent(EventName name, object obj = null)
    {
        string sname = name.ToString();
        _events[sname].Invoke(obj);
    }
    Dictionary<string, Action<object>> _events = new Dictionary<string, Action<object>>();

}

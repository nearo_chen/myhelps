﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitToAnimation
{
    MonoBehaviour _behaviour;
    StatePatternBase _statePatternBase;
    Animator _ani;
    string waitAni;

    public WaitToAnimation(MonoBehaviour behaviour, StatePatternBase statePatternBase)
    {
        _behaviour = behaviour;
        _statePatternBase = statePatternBase;
        _ani = _behaviour.GetComponent<Animator>();        
    }

    Coroutine _oldCoroutine;
    bool firstEnter;
    int firstTimeCounter;

    public void WaitToTriggerAnimation(string aniName, float delaySec = 0)
    {
        if (_oldCoroutine != null)
        {
            _behaviour.StopCoroutine(_oldCoroutine);
            _oldCoroutine = null;
        }
        firstEnter = true;
        firstTimeCounter = 0;

        waitAni = aniName;
        if (delaySec > 0)
            _oldCoroutine = _behaviour.StartCoroutine(_waitToTriggerAnimation(aniName, delaySec));
        else
            _ani.SetTrigger(aniName);
    }

    public bool WaitAniDone()
    {
        AnimatorStateInfo info = _ani.GetCurrentAnimatorStateInfo(0);
        if (firstEnter)
        {
            if (info.normalizedTime > 0.99f)
            {
                firstTimeCounter++;
                if (firstTimeCounter < 30)//To prevent normalize time never < 1
                    return false;
            }
            if (info.IsName(waitAni))
                firstEnter = false;
            return false;
        }

        if (waitAni != null && info.IsName(waitAni))
        {
            //wait until prev animation is done            
            //if (firstEnter && info.normalizedTime > 0.5f)
            //    return;
            //firstEnter = false;

            //wait transform animation is done       
            if (info.normalizedTime > 0.99f)
                waitAni = null;
        }

        return (waitAni == null);
    }

    IEnumerator _waitToTriggerAnimation(string aniName, float delaySec)
    {
        yield return new WaitForSeconds(delaySec);
        _ani.SetTrigger(aniName);
    }

    public void WaitToSetBoolAnimation(string aniName, bool b, float delaySec = 0)
    {
        if (_oldCoroutine != null)
        {
            _behaviour.StopCoroutine(_oldCoroutine);
            _oldCoroutine = null;
        }
        firstEnter = true;
        firstTimeCounter = 0;

        if (delaySec > 0)
            _oldCoroutine = _behaviour.StartCoroutine(_waitToSetBoolAnimation(aniName, b, delaySec));
        else
            _ani.SetTrigger(aniName);
    }

    IEnumerator _waitToSetBoolAnimation(string aniName, bool b, float delaySec)
    {
        yield return new WaitForSeconds(delaySec);
        _ani.SetBool(aniName, b);
    }
}

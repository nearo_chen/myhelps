﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

abstract public class StatePatternBase
{
#if UNITY_EDITOR
    public List<string> debugStateList = new List<string>();
#endif
    private IState _currentState;
    private IState[] _stateList;
    protected abstract void OnCreateState(out IState[] pleaseCreateThisList);
    //protected abstract void OnUpdateBase();
    public abstract void DebugLog(string text, int level = 0);

    public IState GetCurrentState()
    {
        return _currentState;
    }

    public void Restart()
    {
        try
        {
            OnCreateState(out _stateList);
        }
        catch (Exception e)
        {
            DebugLog("error error error : OnCreateState()[" + _currentState.Name() + "] : " + e.StackTrace + " , Message : " + e.Message, 2);
        }
    }

    public virtual void Update()
    {
       // IState updateRec = null;
        //while (_switchStateList.Count > 0)
        //{
        //    doSwitchState(_switchStateList[0]);
        //    _switchStateList.RemoveAt(0);

        //   // _currentState.UpdateState();
        //  //  updateRec = _currentState;
        //}

       // if(updateRec != _currentState)
            _currentState.UpdateState();

        //DebugLog("[StatePatternBase] update : " + _currentState.Name());

        if (_currentState == null)
            Debug.LogError("[StatePatternBase] _currentState == null , maybe you are not calling Restart()");
    }

    List<object> _switchStateList = new List<object>();
    public void SwitchState(object newState)
    {
        _switchStateList.Add(newState);

        while (_switchStateList.Count > 0)
        {
            object stateSwitch = _switchStateList[0];
            _switchStateList.RemoveAt(0);
            doSwitchState(stateSwitch);
            

            // _currentState.UpdateState();
            //  updateRec = _currentState;
        }
    }

    void doSwitchState(object newState)
    {
        int currentID = GetStateEnum(_currentState);
        int newID = (int)newState;


        DebugLog((newState == null) ? "newState==null" : ("newState not null : " + newID));
        DebugLog("[StatePatternBase][SwitchState] :" + ((_currentState == null) ? "none" : _currentState.Name()) + " -> : " + _stateList[newID].Name());
        if (newID >= _stateList.Length)
            Debug.LogError("[SwitchState] : stateID >= _stateList.Length");

        //prevent switch same state
        if (currentID == newID)
        {
            Debug.LogError("[SwitchState] state : currentID == newID : " + newID);
            return;
        }

        if (_currentState != null)
            _currentState.LeaveState();
        IState oldState = _currentState;
        _currentState = _stateList[newID];
        _currentState.EnterState(oldState, this);

#if UNITY_EDITOR
        debugStateList.Add(_currentState.GetType().ToString());
#endif
    }

    public T GetState<T>(object newState)
    {
        int newID = (int)newState;
        return (T)_stateList[newID];
    }

    public int GetStateEnum(IState istate)
    {
        int id = Array.IndexOf(_stateList, istate);
        return id;
    }

    public int GetCurrentStateEnum()
    {
        int id = Array.IndexOf(_stateList, _currentState);
        return id;
    }
}


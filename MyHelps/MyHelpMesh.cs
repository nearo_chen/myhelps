using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MyHelpMesh
{
    /// <summary>
    /// 將位於傳入transform底下的骨架模型建立一個固態的模型
    /// </summary>
    public static GameObject Skinned2Mesh(Transform zombie)
    {
        SkinnedMeshRenderer[] skinnedMeshes = zombie.GetComponentsInChildren<SkinnedMeshRenderer>();
        List<CombineInstance> combineList = new List<CombineInstance>();

        GameObject parent = new GameObject();
        parent.name = zombie.name + "_Splitter" + Random.Range(0, 1000);

        MeshRenderer meshRenderer = parent.AddComponent<MeshRenderer>();
        MeshFilter mshFilter = parent.AddComponent<MeshFilter>();

        //if (skinnedMeshes.Length == 1)
        //{
        //    Debug.LogWarning("single mesh");
        //    Mesh mesh = new Mesh();
        //    skinnedMeshes[0].BakeMesh(mesh);
        //    mshFilter.mesh = mesh;
        //    meshRenderer.material = GameObject.Instantiate(skinnedMeshes[0].material);
        //}
        //else
        {
            bool isInitMaterial = false;
            for (int a = 0; a < skinnedMeshes.Length; a++)
            {
                SkinnedMeshRenderer skinRenderer = skinnedMeshes[a];

                if (skinRenderer.enabled == false)
                    continue;
                if (!isInitMaterial)
                {
                    isInitMaterial = true;
                    meshRenderer.material = GameObject.Instantiate(skinRenderer.material);
                }

                CombineInstance combine = new CombineInstance();
                Mesh mesh = new Mesh();
                skinRenderer.BakeMesh(mesh);

                combine.mesh = mesh;
                combine.transform = Matrix4x4.TRS(zombie.position, zombie.rotation, Vector3.one);// zombie.localToWorldMatrix;
                combineList.Add(combine);
            }
            mshFilter.mesh.CombineMeshes(combineList.ToArray(), true, true);
            //parent.SetActive(true);

            while (combineList.Count > 0)
            {
                GameObject.Destroy(combineList[0].mesh);
                combineList.RemoveAt(0);
            }
        }

        //if (skinnedMeshes.Length == 1)
        //{
        //    parent.transform.position = zombie.position;
        //    parent.transform.rotation = zombie.rotation;
        //}
        return parent;
    }

    /// <summary>
    /// 從root開始蒐，只要有mesh renderer的物件，都將他產生mesh collider
    /// </summary>
    public static void _setMeshCollider(Transform root, bool isDestroy)
    {
        MeshRenderer meshrRender = root.GetComponent<MeshRenderer>();
        if (meshrRender != null)
        {
            MeshCollider collider = root.GetComponent<MeshCollider>();
            if (collider != null)
                GameObject.DestroyImmediate(collider);

            if (!isDestroy)
                collider = root.gameObject.AddComponent<MeshCollider>();
        }

        for (int a = 0; a < root.childCount; a++)
        {
            Transform t = root.GetChild(a);
            _setMeshCollider(t, isDestroy);
        }
    }


    /// <summary>
    /// 用??置所有boss子物体的MeshRenderer
    /// </summary>
    public static void SetRenderer(Transform obj, bool b)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer != null)
        {
            //renderer.updateWhenOffscreen = true;
            renderer.enabled = b;            
        }

        for (int a = 0; a < obj.transform.childCount; a++)
            SetRenderer(obj.transform.GetChild(a), b);
    }

    /// <summary>
    /// 用??置所有boss子物体的SkinMeshRenderer
    /// </summary>
    public static void SkinUpdateOffscreen(Transform obj, bool b)
    {
        SkinnedMeshRenderer renderer = obj.GetComponent<SkinnedMeshRenderer>();
        if (renderer != null)
        {
            if (b)
                renderer.enabled = true;
            renderer.updateWhenOffscreen = b;
        }
        for (int a = 0; a < obj.transform.childCount; a++)
            SkinUpdateOffscreen(obj.transform.GetChild(a), b);
    }

}
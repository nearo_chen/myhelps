﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//Smooth lerp the tiling animation frame.
Shader "Custom/TileAnimationMask"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Mask("Mask", 2D) = "black" {}
        _Color("Color", Color) = (1, 1, 1, 1)
        _Column("Cols Count", Int) = 5
        _Row("Rows Count", Int) = 3
        _FrameTime("Per Frame Length", Float) = 0.5
        _GameTime("Speed", Float) = 1
    }

        SubShader
        {
            Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask RGB
            Cull Off Lighting Off ZWrite Off
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                sampler2D _MainTex, _Mask;
                float4 _MainTex_ST;

                fixed4 _Color;

                float _Column;
                float _Row;

                float _FrameTime, _GameTime;

                fixed4 shot(sampler2D tex, float2 uv, float dx, float dy, int frame) {
                    float2 newuv = float2(
                        uv.x = fmod(uv.x , 1),
                        uv.y = fmod(uv.y, 1)
                        );//限制uv都是在01之間

                    newuv = float2(
                        (newuv.x * dx) + fmod(frame, _Column) * dx,
                        1.0 - ((newuv.y * dy) + floor(frame / _Column) * dy)
                        );
                        
                    //newuv = float2(clamp(uv.x, 0, 1), clamp(uv.y, 0,1 ));
                    return tex2D(tex, newuv);
                }

                v2f vert(appdata v) {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    float2 uv = TRANSFORM_TEX(v.uv, _MainTex);                    
                    //uv = clamp(uv, float2(0,0), float2(1, 1));
                    uv.y *= -1;
                    o.uv = uv;
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target {
                    int frames = _Row * _Column;
                    float frame = fmod(_GameTime / _FrameTime, frames);
                    int current = floor(frame);
                    float dx = 1.0 / _Column;
                    float dy = 1.0 / _Row;

                    // not lerping to next frame
                    // return shot(_MainTex, i.uv, dx, dy, current) * _Color;

                    int next = floor(fmod(frame + 1, frames));
                    
                    return lerp(shot(_MainTex, i.uv, dx, dy, current), shot(_MainTex, i.uv, dx, dy, next), frame - current) * _Color * 
                        tex2D(_Mask, i.uv).r;
                }

                ENDCG
            }
        }
}
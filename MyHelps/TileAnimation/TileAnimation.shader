﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//Smooth lerp the tiling animation frame.
Shader "Custom/TileAnimation"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1, 1, 1, 1)
        _Column("Cols Count", Int) = 5
        _Row("Rows Count", Int) = 3
        _FrameTime("Per Frame Length", Float) = 0.5
        _GameTime("Speed", Float) = 1
    }

        SubShader
        {
            Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask RGB
            Cull Off Lighting Off ZWrite Off
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;

                fixed4 _Color;

                float _Column;
                float _Row;

                float _FrameTime, _GameTime;
                float clampUMin=0, clampUMax=1;
                float clampVMin=0, clampVMax=1;

                fixed4 shot(sampler2D tex, float2 uv, float dx, float dy, int frame) {
                    float2 newuv = float2(
                        uv.x = fmod(uv.x , 1),
                        uv.y = fmod(uv.y, 1)
                        );

                    newuv = float2(
                        (newuv.x * dx) + fmod(frame, _Column) * dx,
                        1.0 - ((newuv.y * dy) + floor(frame / _Column) * dy)
                        );
                        
                    //newuv = float2(clamp(uv.x, 0, 1), clamp(uv.y, 0,1 ));
                    return tex2D(tex, newuv);
                }

                v2f vert(appdata v) {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    float2 uv = TRANSFORM_TEX(v.uv, _MainTex);                    
                    uv = clamp(uv, float2(clampUMin, clampVMin), float2(clampUMax, clampVMax));
                    uv.y *= -1;
                    o.uv = uv;
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target {
                    int frames = _Row * _Column;
                    float frame = fmod(_GameTime / _FrameTime, frames);
                    int current = floor(frame);
                    float dx = 1.0 / _Column;
                    float dy = 1.0 / _Row;

                    // not lerping to next frame
                    // return shot(_MainTex, i.uv, dx, dy, current) * _Color;

                    int next = floor(fmod(frame + 1, frames));
                    
                    float4 final = lerp(shot(_MainTex, i.uv, dx, dy, current), shot(_MainTex, i.uv, dx, dy, next), frame - current) * _Color;
                /*    if (i.uv.x > 1) {
                        final.x = 1; final.y = final.z = 0;
                    }
                    else if (i.uv.x < 0) {
                        final.x = 0; final.y = 1;  final.z = 0;
                    }
                    else {
                        final.x = 1; final.y = 1;  final.z = 1;
                    }*/
                    return final;
                }

                ENDCG
            }
        }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileAnimationShaderControllerSimple : MonoBehaviour
{
    public Renderer model;
    public Texture2D TileTexture;
    public float Column, Row;
    public Color BlendColor = Color.white;
    public float speed = 1;
    //Texture2D _oldTexture;
    //int _oldColumn, _oldRow;
    //Color _oldColor;
    Renderer _render;

    public float AddTileingU = 1;
    public float AddTileingV = 1;

    public float offsetU = 0;
    public float offsetV = 0;

    public bool testPlay;
    public bool EnablePlay = false;

    private void OnEnable()
    {
        if (EnablePlay)
            testPlay = true;
    }

    private void OnDisable()
    {
        if (autoStopRoutine != null)
            StopCoroutine(autoStopRoutine);
        autoStopRoutine = null;
        Stop();
    }

    public virtual void Play()
    {
        _render = model;
        _render.gameObject.SetActive(true);
        _render.material.SetTexture("_MainTex", TileTexture);
        _render.material.SetFloat("_Column", Column);
        _render.material.SetFloat("_Row", Row);
        _render.material.SetColor("_Color", BlendColor);
        _render.material.SetTextureScale("_MainTex", new Vector2(AddTileingU, AddTileingV));
        _render.material.SetTextureOffset("_MainTex", new Vector2(offsetU, offsetV));

        _timeCount = 0;
        _render.material.SetFloat("_GameTime", _timeCount * speed);

        _isStop = false;

        if (autoStopRoutine != null)
            StopCoroutine(autoStopRoutine);
        autoStopRoutine = null;

        SetTexRepeat();

        if(_render.material.shader.name != "Custom/TileAnimation")
            Debug.LogError("[TileAnimationShaderControllerSimple] must use shader -> Custom/TileAnimation");
    }

    public void SetTexClamp()
    {
        _render.material.SetFloat("clampUMin", 0);
        _render.material.SetFloat("clampUMax", 1);

        _render.material.SetFloat("clampVMin", 0);
        _render.material.SetFloat("clampVMax", 1);
    }

    public void SetTexRepeat()
    {
        _render.material.SetFloat("clampUMin", -9999f);
        _render.material.SetFloat("clampUMax", 9999f);

        _render.material.SetFloat("clampVMin", -9999f);
        _render.material.SetFloat("clampVMax", 9999f);
    }

    Coroutine autoStopRoutine;
    public void PlayDuration(float duration = -1)
    {
        Play();

        if (autoStopRoutine != null)
            StopCoroutine(autoStopRoutine);
        autoStopRoutine = null;
        if (duration > 0)
            autoStopRoutine = StartCoroutine(autoStop(duration));
    }

    IEnumerator autoStop(float duration)
    {
        yield return new WaitForSeconds(duration);
        Stop();
    }

    [Header("debug..............")]
    public bool _isStop;
    public float _timeCount;

    public void Stop()
    {
        _isStop = true;
        _timeCount = 0;
    }

    void Update()
    {
        if (testPlay)
        {
            testPlay = false;
            Play();
        }

        if (_render == null)
            return;

        //Renderer render = model;
        //if(_oldTexture != TileTexture)
        //{
        //    _oldTexture = TileTexture;
        //    _timeCount = 0;
        //    render.material.SetTexture("_MainTex", TileTexture);
        //    render.material.SetInt("_Column", Column);
        //    render.material.SetInt("_Row", Row);
        //}

        //if (_oldColor != BlendColor)
        //{
        //    _oldColor = BlendColor;
        //    render.material.SetColor("_Color", BlendColor);
        //}

        _render.material.SetColor("_Color", BlendColor);
        _render.material.SetTextureScale("_MainTex", new Vector2(AddTileingU, AddTileingV));
        _render.material.SetTextureOffset("_MainTex", new Vector2(offsetU, offsetV));

        _render.material.SetFloat("_GameTime", _timeCount * speed);
        _timeCount += (_isStop) ? 0 : Time.deltaTime;
    }
}

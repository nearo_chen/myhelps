﻿Shader "Custom/EyeCircleEffect_Single"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            //https://docs.unity3d.com/Manual/SinglePassInstancing.html
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float CircleAlpha;
            float Pow;
            sampler2D _MainTex;
            float CircleLength, EdgeWidth;
            float4 StartPosScreen01[2];
            float3 CircleColor;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 uv = UnityStereoTransformScreenSpaceTex(i.uv);
                float4 col = tex2D(_MainTex, uv);

                float len = length(i.uv - StartPosScreen01[unity_StereoEyeIndex]);
                float dis = abs(len - CircleLength);
                float ratio = dis / EdgeWidth;
                ratio = clamp(ratio, 0, 1);
                ratio = pow(ratio, Pow);
                //col.rgb = col.rgb * ratio + float3(0, 0, 0)*(1 - ratio);
                col.rgb = col.rgb + CircleColor * (1 - ratio) *CircleAlpha;
                
                return col;
            }
            ENDCG
        }
    }
}

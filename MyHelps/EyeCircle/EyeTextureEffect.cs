﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeTextureEffect : MonoBehaviour
{
    public Texture2D tex;
    public Material EyeTextureEffectMaterial;
    public MyTileAni myTileAni;
    public float texScale = 0.005f;
    public float _Alpha = 1;
    Vector3 _goal;

    Material _EyeTextureEffectMaterial;

    private void Start()
    {
        _EyeTextureEffectMaterial = new Material(EyeTextureEffectMaterial);

        if (myTileAni != null)
            myTileAni.aniTexMaterial = _EyeTextureEffectMaterial;
    }

    public void SetLookAt(Vector3 goal)
    {
        _goal = goal;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        List<Vector4> StartPosScreen01 = new List<Vector4>();
        Camera cam = GetComponent<Camera>();
        Vector3 pos = _goal;
        //debugBall.transform.position = pos;
        pos = cam.WorldToScreenPoint(pos, Camera.MonoOrStereoscopicEye.Left);
        pos = cam.ScreenToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        StartPosScreen01.Add(pos);

        pos = _goal;
        pos = cam.WorldToScreenPoint(pos, Camera.MonoOrStereoscopicEye.Right);
        pos = cam.ScreenToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        StartPosScreen01.Add(pos);

        _EyeTextureEffectMaterial.SetVectorArray("StartPosScreen01", StartPosScreen01);

        _EyeTextureEffectMaterial.SetTexture("_EyeTex", tex);
        _EyeTextureEffectMaterial.SetTexture("_MainTex", source);
        _EyeTextureEffectMaterial.SetFloat("TexW", (float)tex.width * texScale);
        _EyeTextureEffectMaterial.SetFloat("TexH", (float)tex.height * texScale);
        _EyeTextureEffectMaterial.SetFloat("_Alpha", _Alpha);

        Graphics.Blit(null, destination, _EyeTextureEffectMaterial);
    }


}

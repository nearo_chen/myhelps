﻿
#define CIRCLE_LEN_ANIxx
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeCircleEffect : MonoBehaviour
{
    //public Renderer debugBall;

    [Range(0, 0.5f)]
    public float CircleLength = 0.0f;

    [Range(0, 0.5f)]
    public float EdgeWidth = 0.1f;

    [Range(0, 2)]
    public float Pow = 0.1f;
    public Material EyeCircleEffectMaterial;

    Vector3 _goal;
    Vector3? _oldGoal;
    Color _color;

    [Range(0.01f, 1)]
    public float MoveSpeedRed = 0.02f;

    [Range(0.01f, 1)]
    public float MoveSpeedWhite = 0.02f;

    float MoveSpeed = 0.01f;

    Tweener _SwitchTweener;

    private void Start()
    {
        enabled = false;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (_oldGoal == null)
            _oldGoal = _goal;

        List<Vector4> StartPosScreen01 = new List<Vector4>();
        Camera cam = GetComponent<Camera>();
        _oldGoal = _goal * MoveSpeed + _oldGoal * (1 - MoveSpeed);
        Vector3 pos = _oldGoal.Value;
        //debugBall.transform.position = pos;
        pos = cam.WorldToScreenPoint(pos, Camera.MonoOrStereoscopicEye.Left);
        pos = cam.ScreenToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        StartPosScreen01.Add(pos);

        pos = _oldGoal.Value;
        pos = cam.WorldToScreenPoint(pos, Camera.MonoOrStereoscopicEye.Right);
        pos = cam.ScreenToViewportPoint(pos);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        StartPosScreen01.Add(pos);

        EyeCircleEffectMaterial.SetVectorArray("StartPosScreen01", StartPosScreen01);

#if CIRCLE_LEN_ANI
        material.SetFloat("CircleLength", _CircleLength);
#else
        EyeCircleEffectMaterial.SetFloat("CircleLength", CircleLength);
#endif
        EyeCircleEffectMaterial.SetFloat("EdgeWidth", EdgeWidth);
        EyeCircleEffectMaterial.SetFloat("Pow", Pow);
        EyeCircleEffectMaterial.SetFloat("CircleAlpha", CircleAlpha);
        EyeCircleEffectMaterial.SetColor("CircleColor", _color);
        Graphics.Blit(source, destination, EyeCircleEffectMaterial);
    }

    float CircleAlpha;
    float _intensity_get()
    {
        return CircleAlpha;
    }

    void _intensity_set(float intensity)
    {
        //Debug.Log("_intensity_set : " + intensity);
        CircleAlpha = intensity;
    }

    void _turnOffDone()
    {
        enabled = false;
    }

    public void SetLookAt(Vector3 goal)
    {
        _goal = goal;       
    }

    public void SetLookAtRed(Vector3 goal)
    {
        SetLookAt(goal);
        SetColor(Color.red);
        MoveSpeed = MoveSpeedRed;

#if CIRCLE_LEN_ANI
        if (_CircleLengthTweener != null) _CircleLengthTweener.Kill();
        _CircleLengthTweener = DOTween.To(() => _CircleLength, (x) => _CircleLength = x, CircleLength, 0.8f);//.SetEase(Ease.InBounce);
        isCircleWhite = false;
#endif
    }

#if CIRCLE_LEN_ANI
    float _CircleLength;
    bool isCircleWhite;
    Tweener _CircleLengthTweener;
#endif

    public void SetLookAtWhite(Vector3 goal)
    {
        SetLookAt(goal);
        SetColor(Color.white);
        MoveSpeed = MoveSpeedWhite;

#if CIRCLE_LEN_ANI
        if (!isCircleWhite)
        {
            isCircleWhite = true;
            //  _CircleLength = CircleLength * 2;
            if (_CircleLengthTweener != null) _CircleLengthTweener.Kill();
            _CircleLengthTweener = DOTween.To(() => _CircleLength, (x) => _CircleLength = x, CircleLength, 0.4f);//.SetEase(Ease.InBounce);
        }
#endif
    }

    public void SetColor(Color color)
    {
        _color = color;
    }

    public void On()
    {
        if (enabled)
            return;

        enabled = true;
        CircleAlpha = 0;
        if (_SwitchTweener != null) _SwitchTweener.Kill();
        _SwitchTweener = DOTween.To(_intensity_get, _intensity_set, 1, 1).SetEase(Ease.InBounce);

        _oldGoal = null;
    }

    public void Off()
    {
        if (!enabled)
            return;

        CircleAlpha = 1;
        if (_SwitchTweener != null) _SwitchTweener.Kill();
        _SwitchTweener = DOTween.To(_intensity_get, _intensity_set, 0, 1).OnComplete(_turnOffDone);
    }

    private void OnEnable()
    {
        if (_SwitchTweener != null) _SwitchTweener.Kill();
        _SwitchTweener = null;

#if CIRCLE_LEN_ANI
        if (_CircleLengthTweener != null) _CircleLengthTweener.Kill();
        _CircleLengthTweener = null;
#endif
    }
}

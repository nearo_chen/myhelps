﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MyHelps
{
    /// <summary>
    /// 處理Tag格式為 title1:data1,title2:data2,title3:data3...根據','切割為Dictionary
    /// <titile1, data1> , <titile2, data2> , <titile3, data3>
    /// </summary>
    public class TagParser// : MonoBehaviour
    {
        //    void Start()
        //    {
        //        Init("title1:data1,title2:data2,title3:data3");
        //    }

        Dictionary<string, string> _parseDictionary = new Dictionary<string, string>();

        public void Init(string tagString)
        {
            tagString.Replace(" ", "");//先把空格去掉

            string[] splits = tagString.Split(',');
            if (splits.Length > 0)
            {
                foreach (string split in splits)
                {
                    string[] title_data = split.Split(':');
                    if (title_data.Length > 1)
                    {
                        _parseDictionary.Add(title_data[0], title_data[1]);
                    }
                }
            }
        }

        public string TryGetValue(string title)
        {
            string outData;
            _parseDictionary.TryGetValue(title, out outData);
            return outData;
        }
    }
}
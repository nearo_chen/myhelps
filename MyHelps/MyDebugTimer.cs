﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public static class MyDebugTimer
{
    class SWData
    {
        public Stopwatch sw;
        public string text;
    }

    static Dictionary<string, SWData> _swList = new Dictionary<string, SWData>();
    static SWData _currentSW;
    public static void Start()
    {
        string watchName = _getTrace();
        SWData swData;
        if (!_swList.TryGetValue(watchName, out swData))
        {
            swData = new SWData();
            swData.sw = new Stopwatch();
            _swList.Add(watchName, swData);
        }
        swData.sw.Reset();
        swData.sw.Start();
        swData.text = string.Format("[{0}] : ", watchName);
        _currentSW = swData;
    }

    static string _getTrace()
    {
        string output = string.Empty;
        var stackTrace = new StackTrace(true);

        StackFrame[] frames = stackTrace.GetFrames();
        //foreach (var r in stackTrace.GetFrames())
        var r = frames[frames.Length - 1];//取得最後一個frame
        {
            string filename = r.GetFileName();
            output += string.Format("{0} : {1} : {2}",// Column: {3}  ",
                filename.Remove(0, filename.LastIndexOf('\\') + 1),
                r.GetMethod(),
                r.GetFileLineNumber());
            //,r.GetFileColumnNumber());
        }
        return output;
    }

    public static void Stop()
    {
        //http://peterkellner.net/2009/12/21/how-to-get-a-stack-trace-from-c-without-throwing-exception/
        _currentSW.text += string.Format(" , MS : {0}", _currentSW.sw.ElapsedMilliseconds.ToString());
        _currentSW.sw.Stop();
    }

    public static string Output
    {
        get
        {
            string output = string.Empty;
            Dictionary<string, SWData>.Enumerator itr = _swList.GetEnumerator();
            while (itr.MoveNext())
            {
                output += itr.Current.Value.text + "\r\n";
            }
            return output;
        }
    }
}

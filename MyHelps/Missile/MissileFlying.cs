﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileFlying : MonoBehaviour//, ColliderBulletHitted
{
    Vector3 _startPos;
    public Transform target;
    public float speedSec_Meter;

    public float disturbSpeedMove = 20;
    public float disturbSpeedRot = 20;

    [Header("disturb X")]
    public float disturbYDegree = 30;
    public float disturbXPos = 1;

    [Header("disturb Y")]
    public float disturbXDegree = 30;
    public float disturbYPos = 1;

    Vector3 _dir;
    float _magnitude;
    void OnEnable()
    {
        //Destroy(gameObject, 10);
    }

    Vector3 _destLoc;
    public void Play(Vector3 startPos, Vector3 destLoc)
    {
        _startPos = startPos;
        transform.position = _startPos;
        //target = t;
        _destLoc = destLoc;// target.transform.position;

        Transform rotChild = transform.GetChild(0);
        rotChild.localPosition = Vector3.zero;
        rotChild.localRotation = Quaternion.identity;

        _dir = _destLoc - transform.position;
        _magnitude = _dir.magnitude;
        _dir /= _magnitude;

        _timeCount = 0;
    }

    [Range(1, 20)]
    public float disturbFactor = 2;
    public bool reset;

    [Range(-1, 1)]
    public float rotOffsetX;

    [Range(-1, 1)]
    public float rotOffsetY;

    float _timeCount;

    void Update()
    {
        if (reset)
        {
            Play(Vector3.zero, target.position);
            reset = false;
        }
        if (_magnitude == 0)
            return;

        EnemyHP enemyHP = GetComponent<EnemyHP>();
        float moveLength = 0;
        if (enemyHP != null && enemyHP.HPRatio() > 0)
        {
            moveLength = speedSec_Meter * Time.deltaTime;
            _timeCount += Time.deltaTime;
        }
        else
        {
            moveLength = speedSec_Meter * Time.unscaledDeltaTime;
            _timeCount += Time.unscaledDeltaTime;
        }
        float ratio = (_destLoc - transform.position).sqrMagnitude;
        ratio = ratio / (_magnitude * _magnitude);
        //ratio = 1 - ratio;
        ratio = Mathf.Abs(ratio);
        ratio = Mathf.Clamp01(ratio);
        ratio = ratio * (1 - ratio) * (1 - ratio) * disturbFactor;
        //Debug.Log(ratio);

        Transform rotChild = transform.GetChild(0);
        float CosX = Mathf.Cos(_timeCount * disturbSpeedMove);
        float SinX = Mathf.Sin(_timeCount * disturbSpeedMove);
        float CosRotY = Mathf.Cos(_timeCount * disturbSpeedRot + Mathf.PI * rotOffsetX);
        float SinRotY = Mathf.Sin(_timeCount * disturbSpeedRot + Mathf.PI * rotOffsetY);
        rotChild.localRotation = Quaternion.Euler(disturbXDegree * SinRotY * ratio, disturbYDegree * CosRotY * ratio, 0);
        rotChild.localPosition = new Vector3(disturbXPos * SinX, disturbYPos * CosX, 0) * ratio;
        //Vector3 localForward = rotChild.worldToLocalMatrix.MultiplyVector(rotChild.forward);
        //localForward.z = 0;
        //localForward.Normalize();
        //rotChild.localPosition = rotChild.localPosition + localForward * moveLength;
        //Vector3 posFilter = rotChild.localPosition;
        //posFilter.z = 0;
        //rotChild.localPosition = posFilter;        

        transform.forward = -_dir;
        transform.position = transform.position + _dir * moveLength;
        //GetComponent<Rigidbody>().MoveRotation(Quaternion.LookRotation(-_dir));
        //GetComponent<Rigidbody>().MovePosition(transform.position + _dir * moveLength);
    }

    private void OnDestroy()
    {
        //EyeLockInfoNew eyeLock = GetComponentInChildren<EyeLockInfoNew>();
        //if (eyeLock != null)
        //    ETGun.Instance.RemoveEnemyLock(eyeLock.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.WallFloorColLayer/* || collision.gameObject.layer == GameManager.WallLayer*/)
        {
            Gamemanager.CreateExplosionLaser(collision.contacts[0].point, collision.contacts[0].normal);
            Destroy(gameObject);
            return;
        }

        //if (collision.gameObject.layer == GameManager.PlayerBodyLayer)
        //{
        //    GameManager.Instance.aircraftAI.SetIdleFlying();
        //    return;
        //}
    }

    //public void OnColliderBulletHitted(Transform bullet, RaycastHit hit)
    //{
    //    Vector3 hitDir = bullet.forward;
    //    _behit(hitDir, hit.point);

    //    EnemyHP enemyHP = GetComponent<EnemyHP>();
    //    enemyHP.DecreaseHP(bullet.GetComponent<BulletInfo>().decreaseHP);

    //    //GameManager.Instance.InvokeEvent(GameManager.EventName.BulletHitEnemy, bullet.gameObject);
    //    ScoreManager.Instance.BulletHit(bullet.GetComponent<ColliderBullet>());

    //    if(enemyHP.GetHP() <= 0)
    //        GameManager.Instance.aircraftAI.SetIdleFlying();
    //}

    //void _behit(Vector3 hitDir, Vector3 hitPoint)
    //{
    //    //Debug.LogWarning("Missile behitted");


    //    EyeLockInfoNew eyeLock = GetComponentInChildren<EyeLockInfoNew>();
    //    if (eyeLock != null)
    //        ETGun.Instance.RemoveEnemyLock(eyeLock.gameObject);

    //    _dir = hitDir * 0.4f + _dir * 0.6f;

    //    _magnitude = 10;
    //    _dir.Normalize();

    //    GameManager.Instance.CreateExplosionLaser(hitPoint, true);
    //}


}

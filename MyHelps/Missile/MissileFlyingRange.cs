﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MissileFlying))]
public class MissileFlyingRange : MonoBehaviour
{
    public Vector2 speedSec_Meter;

    public Vector2 disturbSpeedMove;
    public Vector2 disturbSpeedRot;

    [Header("disturb X")]
    public Vector2 disturbYDegree;
    public Vector2 disturbXPos;

    [Header("disturb Y")]
    public Vector2 disturbXDegree;
    public Vector2 disturbYPos;

    public bool DoubleSide = true;
    public bool reset;
    void Update()
    {
        if (reset)
        {
            reset = false;
            SetValue();

            MissileFlying missileFlying = GetComponent<MissileFlying>();
            missileFlying.reset = true;
        }
    }

    public void SetValue()
    {


        MissileFlying missileFlying = GetComponent<MissileFlying>();
        missileFlying.speedSec_Meter = Random.Range(speedSec_Meter.x, speedSec_Meter.y);


        missileFlying.disturbSpeedMove = Random.Range(disturbSpeedMove.x, disturbSpeedMove.y);

        missileFlying.disturbSpeedRot = Random.Range(disturbSpeedRot.x, disturbSpeedRot.y);

        float side = (DoubleSide && Random.Range(0, 2) == 0) ? -1f : 1f;
        missileFlying.disturbYDegree = Random.Range(disturbYDegree.x, disturbYDegree.y) * side;
        missileFlying.disturbXPos = Random.Range(disturbXPos.x, disturbXPos.y) * side;

        side = (DoubleSide && Random.Range(0, 2) == 0) ? -1f : 1f;
        missileFlying.disturbXDegree = Random.Range(disturbXDegree.x, disturbXDegree.y) * side;
        missileFlying.disturbYPos = Random.Range(disturbYPos.x, disturbYPos.y) * side;
    }
}

﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class ViveHelp
{
    public static bool IsTracking(SteamVR_TrackedObject obj)
    {
        if (obj.index == SteamVR_TrackedObject.EIndex.None)
            return false;

        return obj.gameObject.activeSelf;

        //if (obj == null)
        //    return false;

        //if (!obj.isValid)
        //{
        //    //Debug.Log("obj.isValid == false");
        //    return false;
        //}

        //var device = SteamVR_Controller.Input((int)obj.index);
        //if (device == null)
        //{
        //    //Debug.Log("SteamVR_Controller.Input((int)obj.index) == null");
        //    return false;
        //}

        ////Debug.Log("device.hasTracking : " + device.hasTracking);
        //return device.hasTracking;
    }

    public enum PressState
    {
        Down,
        Up,
    }
    public static bool IsTriggerPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!_checkTrackedObjectOK(obj))
            return false;

        if (state == PressState.Down)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger);
        else if (state == PressState.Up)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger);
        return false;
    }

    public static bool IsTriggerPress(uint index, PressState state)
    {
        if (state == PressState.Down)
            return SteamVR_Controller.Input((int)index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger);
        else if (state == PressState.Up)
            return SteamVR_Controller.Input((int)index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger);
        return false;
    }

    public static bool IsTouchpadPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!_checkTrackedObjectOK(obj))
            return false;

        if (state == PressState.Down)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Touchpad);
        else if (state == PressState.Up)
            return IsTracking(obj) && SteamVR_Controller.Input((int)obj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return false;
    }

    public static bool IsPadUpPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!IsTouchpadPress(obj, state))
            return false;

        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        Vector2 touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return (touchpad.y > 0.7f);
    }

    public static bool IsPadDownPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!IsTouchpadPress(obj, state))
            return false;

        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        Vector2 touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return (touchpad.y < -0.7f);
    }

    public static bool IsPadLeftPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!IsTouchpadPress(obj, state))
            return false;

        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        Vector2 touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return (touchpad.x < -0.7f);
    }

    public static bool IsPadRightPress(SteamVR_TrackedObject obj, PressState state)
    {
        if (!IsTouchpadPress(obj, state))
            return false;

        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        Vector2 touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
        return (touchpad.x > 0.7f);
    }

    public static void HapticPulse(SteamVR_TrackedObject obj)
    {
        if (!_checkTrackedObjectOK(obj))
            return;
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        device.TriggerHapticPulse(3000);//調500微震，1000小震，最高到3000，4000不會震
    }

    public static void HapticPulse(SteamVR_Controller.Device device)
    {
        if (device != null)
            device.TriggerHapticPulse(3000);//調500微震，1000小震，最高到3000，4000不會震
    }

    public static Vector3 Velocity(SteamVR_TrackedObject obj)
    {
        if (!_checkTrackedObjectOK(obj))
            return Vector3.zero;
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)obj.index);
        return device.velocity;
    }

    static bool _checkTrackedObjectOK(SteamVR_TrackedObject obj)
    {
        return obj != null && obj.isValid;
    }
}

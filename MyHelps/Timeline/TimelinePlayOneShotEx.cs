﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelinePlayOneShotEx : TimelineEnableTriggerAction
{
    public AudioSource audioSource;
    public AudioClip audioClip;

    protected override void _awakeAction()
    {
        audioSource.PlayOneShot(audioClip);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelinePlayer : MonoBehaviour
{
    public PlayableDirector[] TimelineAI;
    Dictionary<string, PlayableDirector> TimelineAIDic = new Dictionary<string, PlayableDirector>();
    Dictionary<string, System.Action> _doneEventRec = new Dictionary<string, System.Action>();

    virtual public void Awake()
    {
        //List<PlayableDirector> timelineailist = new List<PlayableDirector>(TimelineAI);
        for (int a = 0; a < TimelineAI.Length; a++)
        {
            PlayableDirector pd = TimelineAI[a];
            TimelineAIDic.Add(pd.name, pd);
        }
    }

    public void SetTimeLineAIEndTime(string timelineName)
    {
        TimelineAIDic[timelineName].time = TimelineAIDic[timelineName].duration;
    }

    public void SetTimeLineAIAtTime(string timelineName, float time)
    {
        TimelineAIDic[timelineName].Stop();
        TimelineAIDic[timelineName].time = time;
        TimelineAIDic[timelineName].Evaluate();
    }

    public double GetTimeLineAITime(string timelineName)
    {
        return TimelineAIDic[timelineName].time;
    }

    public void SetTimelineAIPlayOnly(string timelineName, System.Action stopEvent = null, bool unscaleTime = false, float speed = 1)
    {
        SetTimelineAIPlay(timelineName, stopEvent, unscaleTime, speed, false);
    }

    public void SetTimelineAIPlay(string timelineName, System.Action doneEvent = null, bool unscaleTime = false, float speed = 1, bool stopAll = true)
    {
        if (!TimelineAIDic.ContainsKey(timelineName))
        {
            Debug.LogError("[SetTimelineAIPlay] not ContainsKey : " + timelineName);
            return;
        }

        Debug.Log("[TimelinePlayer][SetTimelineAIPlay] : timelineName : " + timelineName);

        if (stopAll)
            SetTimelineAIStop();

        TimelineAIDic[timelineName].Play();
        TimelineAIDic[timelineName].time = 0;
        TimelineAIDic[timelineName].Evaluate();
        TimelineAIDic[timelineName].timeUpdateMode = (unscaleTime) ? DirectorUpdateMode.UnscaledGameTime : DirectorUpdateMode.GameTime;

        TimelineAIDic[timelineName].stopped += _timelineAIPlayStopped;

        _doneEventRec[timelineName] = doneEvent;

        SetSpeed(TimelineAIDic[timelineName], speed);
    }

    public void SetTimelineAIStop(string timelineName = null)
    {
        if (timelineName == null)
        {
            foreach (PlayableDirector pd in TimelineAIDic.Values)
            {
                //pd.time = 0;//千萬不能觸發Evaluate()，因為所有的timeline會觸發active time 0一次，會造成OnEnable()
                //pd.Evaluate();
                pd.stopped -= _timelineAIPlayStopped;
                if (_doneEventRec.ContainsKey(pd.name))
                    _doneEventRec.Remove(pd.name);
                pd.Stop();
            }
        }
        else
        {
            //TimelineAIDic[timelineName].time = 0;//千萬不能觸發Evaluate()，因為所有的timeline會觸發active time 0一次，會造成OnEnable()
            //TimelineAIDic[timelineName].Evaluate();
            TimelineAIDic[timelineName].stopped -= _timelineAIPlayStopped;
            if (_doneEventRec.ContainsKey(timelineName))
                _doneEventRec.Remove(timelineName);
            TimelineAIDic[timelineName].Stop();
        }
    }

    public bool IsTimelineAIPlay(string timelineName)
    {
        return TimelineAIDic[timelineName].state != PlayState.Paused;
    }

    void _timelineAIPlayStopped(PlayableDirector pd)
    {
        pd.stopped -= _timelineAIPlayStopped;

        System.Action doneEvent;
        if (_doneEventRec.TryGetValue(pd.name, out doneEvent))
        {
            if (doneEvent != null)
                doneEvent.Invoke();
            _doneEventRec.Remove(pd.name);
        }
    }

    //https://forum.unity.com/threads/problem-with-manual-update.670321/
    public static void SetSpeed(PlayableDirector pd, float speed)
    {
        if (!pd.playableGraph.IsValid())
            return;
        pd.playableGraph.GetRootPlayable(0).SetSpeed(speed);
    }
}

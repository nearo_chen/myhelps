﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineAnimatorTrigger : MonoBehaviour
{
    public Animator animator;
    public string TriggerName;
    public void Play()
    {
        //animator.SetTrigger(TriggerName);
        StartCoroutine(delayPlay());
    }

    IEnumerator delayPlay()
    {
        yield return new WaitForEndOfFrame();

        animator.SetTrigger(TriggerName);
    }
}

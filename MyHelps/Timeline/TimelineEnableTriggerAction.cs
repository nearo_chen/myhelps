﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

//https://forum.unity.com/threads/timeline-events.479400/
public class TimelineEnableTriggerAction : MonoBehaviour
{
    public bool ForceOnce = false;
    int _invokeCount = 0;
    public void ResetOnce()
    {
        _invokeCount = 0;
    }

    public UnityEvent eventAction;
    //public UnityEvent eventDisable;

    void Awake()
    {
        PlayableDirector pd = transform.parent.GetComponent<PlayableDirector>();
        if (pd == null)
            Debug.LogError("[TimelineEnableTriggerAction] transform.parent.GetComponent<PlayableDirector>() must not null : " + name);

        //Dictionary<string, PlayableBinding> bindingDict;
        //TimelineManager.initBindingDict(pd, out bindingDict);
        //Dictionary<string, PlayableBinding>.Enumerator itr = bindingDict.GetEnumerator();
        //while(itr.MoveNext())
        //{
        //    if(itr.Current.Value.sourceObject.GetHashCode() == gameObject.GetHashCode())
        //    { }
        //}

        pd.stopped += _stoppedTimeline;

        //if (name == "SmokeStart")
        //{
        //    int a = 0;
        //}

        gameObject.SetActive(false);

        eventAction.AddListener(_awakeAction);
    }

    virtual protected void _awakeAction()
    {

    }

    void _stoppedTimeline(PlayableDirector pd)
    {
        gameObject.SetActive(false);
        //MonoBehaviour[] componts = GetComponents<MonoBehaviour>();
        //foreach(MonoBehaviour c in componts)
        //{
        //    if(c.name.Contains("Timeline") && !(c is TimelineEnableTriggerAction))
        //    {
        //        c.enabled = false;
        //    }
        //}
    }

    void OnEnable()
    {
        PlayableDirector pd = transform.parent.GetComponent<PlayableDirector>();
        if (pd.state == PlayState.Paused)
            return;
        //if (name == "SmokeStart")
        //{
        //    int a = 0;
        //}

        if (ForceOnce && _invokeCount != 0)
            return;

        //if (eventAction.GetPersistentTarget(0) == null)
        //    Debug.LogError("eventAction == null");
        eventAction.Invoke();
        _invokeCount++;
    }
}

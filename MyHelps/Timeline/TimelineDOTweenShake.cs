﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineDOTweenShake : TimelineEnableTriggerAction
{
    public Transform shakeNode;
    public float shockNodeTime = 0.3f;
    public float shockNodePosStrength = 0.2f;
    public float shockNodeRotStrength = 0.2f;

    protected override void _awakeAction()
    {
        shakeNode.DOShakePosition(shockNodeTime, shockNodePosStrength);
        shakeNode.DOShakeRotation(shockNodeTime, shockNodeRotStrength);
    }

}

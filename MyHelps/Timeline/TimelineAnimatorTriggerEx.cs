﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineAnimatorTriggerEx : TimelineEnableTriggerAction
{
    public Animator animator;
    public string TriggerName;

    protected override void _awakeAction()
    {
        StartCoroutine(delayPlay());

        //animator.SetTrigger(TriggerName);
    }

    IEnumerator delayPlay()
    {
        yield return new WaitForEndOfFrame();

        animator.SetTrigger(TriggerName);
    }
}

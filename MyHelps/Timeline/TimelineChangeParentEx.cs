﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelineChangeParentEx : TimelineEnableTriggerAction
{
    public Transform changeObject;
    public Transform parentDest;
    Transform origTransform;

    public bool resetLocal;

    public bool isDelayPlay = false;

    protected override void _awakeAction()
    {
        if (isDelayPlay)
            StartCoroutine(delayPlay());
        else
            _do();
        //animator.SetTrigger(TriggerName);
    }

    IEnumerator delayPlay()
    {
        yield return new WaitForEndOfFrame();
        _do();
    }

    void _do()
    {
        Transform dest = parentDest;
        //if (!parentDest.gameObject.activeInHierarchy)
        //{
        //    dest = TimelineManager.Instance.FindInActors(parentDest);
        //}
        //if (dest == null)
        //    dest = parentDest;

        origTransform = changeObject.transform.parent;
        changeObject.transform.SetParent(dest, !resetLocal);

        if (resetLocal)
        {
            changeObject.transform.localPosition = Vector3.zero;
            changeObject.transform.localRotation = Quaternion.identity;
        }
    }

    public void Recover()
    {
        changeObject.transform.SetParent(origTransform, true);
    }



}

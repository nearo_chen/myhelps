﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineLoopTo : MonoBehaviour
{
    public PlayableDirector pd;
    public float loopToTime;
    public Action OnLoopTo;
    public void LoopTo()
    {
        pd.time = loopToTime;
        if (OnLoopTo != null)
            OnLoopTo();
    }
}

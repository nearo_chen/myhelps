﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TimelineChangeVideoClip : TimelineEnableTriggerAction
{
    public VideoPlayer videoPlayer;
    public VideoClip videoClip;

    protected override void _awakeAction()
    {
        videoPlayer.Stop();
        videoPlayer.clip = videoClip;
        videoPlayer.Play();
    }

}

﻿//#define MY_DEBUG
using UnityEngine;
using System.Collections;

public class WaypointTracking : MonoBehaviour
{
	//[System.Serializable]
	//public struct TrackingObject
	//{
	//    public MeshRenderer trackingAABB;
	//    public GameObject arrowShow;
	//}

	//[SerializeField]
	//TrackingObject _trackingObjects;

	[SerializeField]
	Renderer
		_trackingAABB;

	[SerializeField]
	float
		_arrowDistance;

	[SerializeField]
	float
		_arrowOffset;

	[SerializeField]
	Camera
		_camera;

	[SerializeField]
	float
		_fixFOV;

	Renderer _arrowRenderer;

#if UNITY_EDITOR && MY_DEBUG
    public bool ShowDebug = true;
#endif

	// Use this for initialization
	void Start ()
	{
		//_camera = GetComponent<Camera>();
		_arrowRenderer = GetComponent<Renderer> ();
	}

	// Update is called once per frame
	void Update ()
	{
		GameObject go = new GameObject ("WaypointTracking_FixCamera");
		Camera fixCamera = go.AddComponent<Camera> ();
		fixCamera.fieldOfView = _fixFOV;
		fixCamera.nearClipPlane = _camera.nearClipPlane;
		fixCamera.farClipPlane = _camera.farClipPlane;
		fixCamera.transform.position = _camera.transform.position;
		fixCamera.transform.rotation = _camera.transform.rotation;
		Plane[] frustumPlanes = GeometryUtility.CalculateFrustumPlanes (fixCamera);
		GameObject.DestroyImmediate (go);

		Vector3 cameraForward = _camera.transform.forward;
		Vector3 cameraPos = _camera.transform.position;

#if UNITY_EDITOR && MY_DEBUG
        if (ShowDebug)
        {
            //[0] = Left
            //[1] = Right
            //[2] = Down
            //[3] = Up
            //[4] = Near
            //[5] = Far
            Vector3 pos = cameraPos + cameraForward * 10f;
            foreach (Plane p in frustumPlanes)
                MyHelps.MyHelp.DrawPlane(p, pos);
        }
#endif

		Plane nearPlane = frustumPlanes [4];
		_arrowRenderer.enabled = false;

		//step 1: check obj's aabb is in view frustum, if not{ get a vector A (plane center)->(project point).}
		if (!GeometryUtility.TestPlanesAABB (frustumPlanes, _trackingAABB.bounds))
        {
			Vector3 nearPlaneCenter = cameraPos + cameraForward * _camera.nearClipPlane;
			Vector3 projectionPoint = _trackingAABB.bounds.center;
			float dis = nearPlane.GetDistanceToPoint (projectionPoint);
			projectionPoint = projectionPoint + nearPlane.normal * -dis;

			Vector3 trackingDirection = (projectionPoint - nearPlaneCenter).normalized;

			//step 2: use vector A collide with view frustum and get a collision point, this point will place the arrow mark.
			Vector3 arrowPos = nearPlaneCenter + cameraForward * _arrowDistance;
			Ray ray = new Ray (arrowPos, trackingDirection);

#if UNITY_EDITOR && MY_DEBUG
            if (ShowDebug)
            {
                //Debug.Log("[WaypointTracking] not in frustum, dis: " + dis);
                Debug.DrawLine(nearPlaneCenter, projectionPoint);
                Debug.DrawRay(ray.origin, ray.direction, Color.blue);
            }
#endif

			float? minDis = null;
			for (int a = 0; a <= 3; a++)
            {
				if (frustumPlanes [a].Raycast (ray, out dis))
                {
					if (minDis == null || dis < minDis.Value)
						minDis = dis;
				}
			}
			if (minDis == null)
            {   
				Debug.LogWarning ("[WaypointTracking] tracking object fail...4 side view frustum collision fial...");
				return;
			}

			arrowPos = ray.origin + ray.direction * minDis.Value * _arrowOffset;
			this.transform.position = arrowPos;
			_arrowRenderer.enabled = true;

			this.transform.rotation = Quaternion.LookRotation (ray.direction, -cameraForward);
		}

	}

	public void SetNeedData (Renderer trackingAABB, Camera camera)
	{
		_trackingAABB = trackingAABB;
		_camera = camera;
	}
}

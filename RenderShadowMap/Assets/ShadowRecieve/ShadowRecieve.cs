﻿#define USE_BLURHV
#define USE_copyCameraDepth
using UnityEngine;
using System.Collections;

#if USE_copyCameraDepth
[RequireComponent(typeof(Camera), typeof(CopyCameraDepth))]
#endif
public class ShadowRecieve : MonoBehaviour
{
    //public bool UseBlur;
    public float ScreenMapScale = 0.5f;
    public Light shadowLight;
    ShadowCastCamera shadowLightCamera;
    GameObject _shaderReplacementCamera;
    RenderTexture ScreenShadowMap, ScreenShadowMapBlur;
    public Shader ShadowRecieveShader;
    public LayerMask recieveShadowLayer;
    public MeshRenderer debugRender;
    public Material screenShadowBlit, blurMaterial, blurHMaterial, blurVMaterial;
    [Range(0, 10), Tooltip("BlurFactor = 0 is turn off blur")]
    public float BlurFactor;
    public Color shadowColor;
    Camera renderCamera;

#if USE_copyCameraDepth
    CopyCameraDepth copyCameraDepth;
#else
    public CopyCameraDepthColor VRDepthColor;
#endif
    //public Texture VRColorDepth;


    [Range(0.0f, 0.01f)]
    public float bais = 0.01f;

    [Range(0.0f, 0.1f)]
    public float shadowBlitBais = 0.0001f;

    [Range(0f, 100f)]
    public float ShadowFactor = 1;

    void Start()
    {
        shadowLightCamera = shadowLight.GetComponentInChildren<ShadowCastCamera>();
        renderCamera = GetComponent<Camera>();
        ScreenShadowMap = new RenderTexture(Mathf.CeilToInt(renderCamera.pixelWidth * ScreenMapScale), Mathf.CeilToInt(renderCamera.pixelHeight * ScreenMapScale), 24, RenderTextureFormat.RGFloat);
        ScreenShadowMapBlur = new RenderTexture(Mathf.CeilToInt(renderCamera.pixelWidth * ScreenMapScale), Mathf.CeilToInt(renderCamera.pixelHeight * ScreenMapScale), 24, RenderTextureFormat.RGFloat);
        ScreenShadowMap.useMipMap = ScreenShadowMapBlur.useMipMap = false;
        ScreenShadowMap.filterMode = ScreenShadowMapBlur.filterMode = FilterMode.Point;
        ScreenShadowMap.antiAliasing = ScreenShadowMapBlur.antiAliasing = 1;
        ScreenShadowMap.anisoLevel = ScreenShadowMapBlur.anisoLevel = 0;

#if USE_copyCameraDepth
        copyCameraDepth = GetComponent<CopyCameraDepth>();
#endif
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnPostRender()
    {
        //https://github.com/Unity-Technologies/VolumetricLighting/blob/master/Assets/AreaLight/Scripts/AreaLight.cs

        if (_shaderReplacementCamera == null)
        {
            _shaderReplacementCamera = new GameObject("RecieveShadowCameraClone");
            _shaderReplacementCamera.hideFlags = HideFlags.HideAndDontSave;
            Camera c = _shaderReplacementCamera.AddComponent<Camera>();
            c.enabled = false;
        }

        Camera cam = _shaderReplacementCamera.GetComponent<Camera>();
        cam.CopyFrom(renderCamera);
        //cam.CopyFrom(shadowLightCamera.GetComponent<Camera>());

        //   cam.aspect = lightcamera.aspect;
        cam.backgroundColor = Color.white;//new Color(0, 0, 0, 0);
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.renderingPath = RenderingPath.Forward;
        cam.useOcclusionCulling = false;
        cam.targetTexture = ScreenShadowMap;
        cam.depthTextureMode = DepthTextureMode.None;

        cam.depth = 100;
        cam.cullingMask = recieveShadowLayer;//-1;//everythingCullingMask

        {
            Shader.SetGlobalMatrix("_ShadowVP", shadowLightCamera.GetWorld2LightMatrix());
            Shader.SetGlobalTexture("_DepthMap", shadowLightCamera.GetDepthMap());
            Shader.SetGlobalFloat("_Bais", bais);
            Shader.SetGlobalColor("_ShadowColor", shadowColor);
            Shader.SetGlobalVector("_lightDir", shadowLight.transform.forward);
            Shader.SetGlobalFloat("_ShadowFactor", ShadowFactor);
            cam.stereoTargetEye = StereoTargetEyeMask.None;
            cam.RenderWithShader(ShadowRecieveShader, "");
        }

        if (BlurFactor > 0)
        {
#if USE_BLURHV
            blurHMaterial.SetFloat("_Factor", BlurFactor);
            blurVMaterial.SetFloat("_Factor", BlurFactor);
            Vector4 texelSize = new Vector4(ScreenShadowMap.texelSize.x, ScreenShadowMap.texelSize.y, 0, 0);
            blurHMaterial.SetVector("_MainTex_TexelSize", texelSize);
            blurVMaterial.SetVector("_MainTex_TexelSize", texelSize);

            Graphics.Blit(ScreenShadowMap, ScreenShadowMapBlur, blurHMaterial);
            Graphics.Blit(ScreenShadowMapBlur, ScreenShadowMap, blurVMaterial);
#else
            blurMaterial.SetFloat("_Factor", BlurFactor);

            //http://www.shaderslab.com/demo-39---blur-effect-with-grab-pass.html
            //blur shadow map
            Graphics.Blit(ScreenShadowMap, ScreenShadowMapBlur, blurMaterial);
            RenderTexture tmp = ScreenShadowMap;
            ScreenShadowMap = ScreenShadowMapBlur;
            ScreenShadowMapBlur = tmp;
#endif
        }

        if (debugRender != null)
            debugRender.material.mainTexture = ScreenShadowMap;
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        screenShadowBlit.SetTexture("_ScreenShadowMap", ScreenShadowMap);
#if USE_copyCameraDepth
        screenShadowBlit.SetTexture("_VRDepthColor", copyCameraDepth.DepthRT);

#else
        if (VRDepthColor == null)
            VRDepthColor = GetComponent<CopyCameraDepthColor>();
        screenShadowBlit.SetTexture("_VRDepthColor", VRDepthColor.GetRT(renderCamera.stereoActiveEye));
#endif
        screenShadowBlit.SetFloat("_shadowBlitBais", shadowBlitBais);
        Graphics.Blit(src, dest, screenShadowBlit);
    }

    void OnDisable()
    {
        if (_shaderReplacementCamera != null)
        {
            DestroyImmediate(_shaderReplacementCamera);
        }
    }
}

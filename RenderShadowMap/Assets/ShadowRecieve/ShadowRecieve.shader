﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ShadowRecieve"
{
    Properties
    {
        //_DepthMap ("_DepthMap", 2D) = "white" {}
        _ShadowFactor ("ShadowFactor", Range(0,100)) = 0.01
    }
    SubShader
    {
        Tags{ "RenderType" = "Opaque" }
        LOD 100

        Pass
    {
        CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

        /*	struct appdata
        {
        float4 pos : POSITION;
        };*/

        struct v2f
    {
        float4 pos : SV_POSITION;
        float4 worldPos : TEXCOORD0;
        float2 depth : TEXCOORD1;
        float3 worldNormal : TEXCOORD2;
    };

    float4 _lightDir;
    uniform sampler2D _DepthMap;
    uniform float4x4 _ShadowVP;
    uniform float _Bais, _ShadowFactor;
    uniform float4 _ShadowColor;

    v2f vert(appdata_base v)
    {
        v2f o;
        o.worldPos = mul(unity_ObjectToWorld, v.vertex);
        o.pos = UnityObjectToClipPos(v.vertex);
        o.depth.x = o.pos.z;
        o.depth.y = o.pos.w;

        o.worldNormal = UnityObjectToWorldNormal(v.normal);
        return o;
    }

    float2 frag(v2f i) : SV_Target
    {
        float4 frustumPos = mul(_ShadowVP, float4(i.worldPos.xyz, 1));
        float cameraDepth = Linear01Depth(i.depth.x / i.depth.y);
        float3 shadowColor = float3(1,1,1);
        //Outside shadow frustum method 1:
        //float2 shadowUV = 0.5 * frustumPos.xy / frustumPos.w + float2(0.5,0.5);
        //float2 frustumUV = shadowUV;
        //if(!(shadowUV.x>0 &&shadowUV.x<1 && shadowUV.y>0 && shadowUV.y<1))

        //Outside shadow frustum method 2: https://github.com/Unity-Technologies/VolumetricLighting/blob/master/Assets/AreaLight/Scripts/AreaLight.cs
        float2 shadowUV = frustumPos.xy / frustumPos.w;
        float2 frustumUV = shadowUV*0.5 + 0.5;
        if (any(step(1.0, abs(shadowUV))))
        {
            //shadowColor.rgb = float3(1,1,1);//outside shadow frustum
            return float2(1, cameraDepth);
        }

        float shadowDepth = 1 - tex2D(_DepthMap, frustumUV).r;
        //return float4(shadowDepth,shadowDepth,shadowDepth,1);//Debug
        float pixelDepth = frustumPos.z / frustumPos.w;
        pixelDepth = 1 + pixelDepth;
        pixelDepth = pixelDepth / 2;

        //if(pixelDepth >0.9)//Debug
        //if(shadowDepth < 0.1)//Debug
        float shadowOffset = (pixelDepth - _Bais) - shadowDepth;
        if (shadowOffset > 0)//near plane is 0, far plane is 1
        {
            float face = dot(_lightDir, i.worldNormal);
            if (face < 0)
            {
                shadowColor.rgb = _ShadowColor.rgb;
                shadowColor.r *= shadowOffset * _ShadowFactor;//make the shadow not close, not so black
            }
        }
        //return float4(shadowColor.r, shadowColor.g, shadowColor.b, cameraDepth);//Debug
        //return EncodeDepthNormal(cameraDepth, float3(shadowColor.r, shadowColor.g, shadowColor.b));

        return float2(shadowColor.r, cameraDepth);
    }
                ENDCG
    }
    }
}

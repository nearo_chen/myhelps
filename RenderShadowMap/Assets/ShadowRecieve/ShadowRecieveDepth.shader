﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ShadowRecieveDepth"
{
	Properties
	{
		//_DepthMap ("_DepthMap", 2D) = "white" {}
		//_Bais ("Bais", Range(0,1)) = 0.01
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			// https://forum.unity.com/threads/recreating-world-position-from-depth-algorithm.263435/
            float4x4 _ViewToWorld;
            float4 _FrustrumPoints;
            float _CameraFar;
            // 1. Take the screen space position uv and find where it lies on the rectangle on the far clip plane. This is a ray from the camera to the far clip plane.
            // 2. Then we multiply the z position by the depth to find where that point lies on a ray. We have to multiple the x and y coordinates by depth if it's perspective.
            // The _FrustrumPoints are calculated in the code
            float3 CalcVS(float2 screenPos, float depth)
            {
                float2 lerpVal = screenPos;
                float3 ray = float3(
                    _FrustrumPoints.x * lerpVal.x + _FrustrumPoints.y * (1 - lerpVal.x),
                    _FrustrumPoints.z * lerpVal.y + _FrustrumPoints.w * (1 - lerpVal.y),
                    _CameraFar); // I think _ProjectionParams.z will also work

            #if defined (PERSPECTIVE)
                float3 posVS = float3(ray.xy * -depth, ray.z * -depth);
            #else
                float3 posVS = float3(ray.xy, ray.z * -depth);
            #endif
                return posVS;
            }

			uniform sampler2D _ShadowDepth, _DeferredDepth;
			uniform float4x4 _ShadowVP;
			uniform float _Bais;
			uniform float4 _ShadowColor;
			
			float2 frag (v2f_img i) : SV_Target
			{
				//↓↓↓↓↓↓↓↓↓↓
				float cameraDepth = tex2D(_DeferredDepth, float2(1 - i.uv.x, 1 - i.uv.y)).r;
				//float cameraDepth = Linear01Depth( i.depth.x / i.depth.y);
				float2 TexCoord = i.uv;
                float3 positionVS = CalcVS(TexCoord, cameraDepth);

                // Convert to worldSpace
                float3 worldPos = mul(_ViewToWorld, float4(positionVS, 1)).xyz;
				//↑↑↑↑↑↑↑↑↑↑

				float4 frustumPos = mul( _ShadowVP, float4(worldPos, 1));                
                float3 shadowColor = float3(1,1,1);
				//Outside shadow frustum method 1:
				//float2 shadowUV = 0.5 * frustumPos.xy / frustumPos.w + float2(0.5,0.5);
				//float2 frustumUV = shadowUV;
				//if(!(shadowUV.x>0 &&shadowUV.x<1 && shadowUV.y>0 && shadowUV.y<1))

				//Outside shadow frustum method 2: https://github.com/Unity-Technologies/VolumetricLighting/blob/master/Assets/AreaLight/Scripts/AreaLight.cs
				float2 shadowUV = frustumPos.xy/frustumPos.w;
				float2 frustumUV = shadowUV*0.5+0.5;
				if(any(step(1.0, abs(shadowUV))))
				{
                    //shadowColor.rgb = float3(1,1,1);//outside shadow frustum
					return float2(1, cameraDepth);
				}

				float shadowDepth = 1-tex2D(_ShadowDepth, frustumUV).r;
				//return float4(shadowDepth,shadowDepth,shadowDepth,1);//Debug
				float pixelDepth = frustumPos.z / frustumPos.w;
                pixelDepth = 1 + pixelDepth;
                pixelDepth = pixelDepth / 2;

                //if(pixelDepth >0.9)//Debug
				//if(shadowDepth < 0.1)//Debug
				if(shadowDepth < pixelDepth-_Bais)//near plane is 0, far plane is 1
				{
                    shadowColor.rgb = _ShadowColor.rgb;
				}
				//return float4(shadowColor.r, shadowColor.g, shadowColor.b, cameraDepth);//Debug
                //return EncodeDepthNormal(cameraDepth, float3(shadowColor.r, shadowColor.g, shadowColor.b));
                return float2(shadowColor.r, cameraDepth);
			}
			ENDCG
		}
	}
}

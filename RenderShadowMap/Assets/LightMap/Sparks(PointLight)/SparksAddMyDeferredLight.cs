﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparksAddMyDeferredLight : MonoBehaviour {

    public ParticleSystem sparksLight;
    public DeferredLightMap deferredLightMap;

    List<Light> _origPointLight;
    void Start()
    {
        _origPointLight = deferredLightMap.pointLightList;
    }

    // Update is called once per frame
    void Update()
    {
        deferredLightMap.pointLightList = new List<Light>(_origPointLight);
        Light[] lights = sparksLight.GetComponentsInChildren<Light>();
        foreach (Light l in lights)
            deferredLightMap.AddPointLight(l);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoLightAddMyDeferredLight : MonoBehaviour
{
    public DiscoLight discoLight;
    public DeferredLightMap deferredLightMap;

    List<Light> _origSpotLight;
    void Start()
    {
        _origSpotLight = deferredLightMap.spotLightList;
    }

    // Update is called once per frame
    void Update()
    {
        deferredLightMap.spotLightList = new List<Light>(_origSpotLight);
        Light[] lights = discoLight.GetComponentsInChildren<Light>();
        foreach (Light l in lights)
            deferredLightMap.AddSpotLight(l);
    }
}

﻿Shader "Custom/DeferredPointLightMap"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
            #pragma multi_compile PERSPECTIVE

            #include "UnityCG.cginc"

            //float4 halfPixel;
            //float4x4 invVP;
            static const int PLightPerPass = 4;
            int PointLightAmount;
            float4 PointLightColor[PLightPerPass];
            float4 PointLightPosition[PLightPerPass];
            float4 PointLightDecayRangeStrength[PLightPerPass];

			sampler2D _MainLightMap, _CameraDepthNormal;

           // https://forum.unity.com/threads/recreating-world-position-from-depth-algorithm.263435/
            float4x4 _ViewToWorld;
            float4 _FrustrumPoints;
            float _CameraFar;
            // 1. Take the screen space position uv and find where it lies on the rectangle on the far clip plane. This is a ray from the camera to the far clip plane.
            // 2. Then we multiply the z position by the depth to find where that point lies on a ray. We have to multiple the x and y coordinates by depth if it's perspective.
            // The _FrustrumPoints are calculated in the code
            float3 CalcVS(float2 screenPos, float depth)
            {
                float2 lerpVal = screenPos;
                float3 ray = float3(
                    _FrustrumPoints.x * lerpVal.x + _FrustrumPoints.y * (1 - lerpVal.x),
                    _FrustrumPoints.z * lerpVal.y + _FrustrumPoints.w * (1 - lerpVal.y),
                    _CameraFar); // I think _ProjectionParams.z will also work

            #if defined (PERSPECTIVE)
                float3 posVS = float3(ray.xy * -depth, ray.z * -depth);
            #else
                float3 posVS = float3(ray.xy, ray.z * -depth);
            #endif
                return posVS;
            }

                        //https://answers.unity.com/questions/218333/shader-inversefloat4x4-function.html
                    /*	float4x4 inverse(float4x4 input)
                         {
                             #define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
                             //determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))

                             float4x4 cofactors = float4x4(
                                  minor(_22_23_24, _32_33_34, _42_43_44),
                                 -minor(_21_23_24, _31_33_34, _41_43_44),
                                  minor(_21_22_24, _31_32_34, _41_42_44),
                                 -minor(_21_22_23, _31_32_33, _41_42_43),

                                 -minor(_12_13_14, _32_33_34, _42_43_44),
                                  minor(_11_13_14, _31_33_34, _41_43_44),
                                 -minor(_11_12_14, _31_32_34, _41_42_44),
                                  minor(_11_12_13, _31_32_33, _41_42_43),

                                  minor(_12_13_14, _22_23_24, _42_43_44),
                                 -minor(_11_13_14, _21_23_24, _41_43_44),
                                  minor(_11_12_14, _21_22_24, _41_42_44),
                                 -minor(_11_12_13, _21_22_23, _41_42_43),

                                 -minor(_12_13_14, _22_23_24, _32_33_34),
                                  minor(_11_13_14, _21_23_24, _31_33_34),
                                 -minor(_11_12_14, _21_22_24, _31_32_34),
                                  minor(_11_12_13, _21_22_23, _31_32_33)
                             );
                             #undef minor
                             return transpose(cofactors) / determinant(input);
                         }
                         */
                        float4 frag(v2f_img i) : SV_Target
                        {
                            float4 depthNormal = tex2D(_CameraDepthNormal, float2(1 - i.uv.x, 1 - i.uv.y));
                            float cameraDepth = depthNormal.r;
                            float3 worldSpaceNormal = depthNormal.gba;
                            float2 TexCoord = i.uv;
                            float3 positionVS = CalcVS(TexCoord, cameraDepth);

                            // Convert to worldSpace
                            float4 position = mul(_ViewToWorld, float4(positionVS, 1));

                            float3 final = 0;
                            for (int a = 0; a < PointLightAmount; a++)
                            {
                                float3 PLightDirection = position.xyz - PointLightPosition[a].xyz;
                                //return float4(PointLightPosition[a].xyz, 1);
                                float lightDist = length(PLightDirection);
                                //return float4(lightDist, lightDist, lightDist, 1) / 200;

                                PLightDirection = PLightDirection / lightDist;                  

                                //calculate the intensity of the light with exponential falloff
                                //float decay = PointLightDecayRangeStrength[a].x;
                                float range = PointLightDecayRangeStrength[a].y;
                                float origIntensity = PointLightDecayRangeStrength[a].x;
                                float decay = clamp(30 - origIntensity, 0, 30);
                                float decay01 = decay / 30;
                                float baseIntensity = saturate((range - lightDist) / range);
                                float atten = baseIntensity* (1 - decay01) * 4 +
                                    //   pow(baseIntensity, decay01*8) *
                                    origIntensity*pow(baseIntensity, 4);
                                    
                             /*   float3 toLight = PointLightPosition[a].xyz - position;
                                float lengthSq = dot(toLight, toLight);
                                lengthSq = max(lengthSq, 0.000001);
                                toLight *= rsqrt(lengthSq);
                                float quadratic_attenuation = 1.0 / range*range;
                                atten = 1.0 / (1.0 + lengthSq * 1);
                                */

                                float diffuseIntensity = saturate(dot(worldSpaceNormal, -PLightDirection));
                                final += atten*PointLightColor[a].xyz * diffuseIntensity;
                            }

                            final.xyz += tex2D(_MainLightMap, float2(i.uv)).xyz;
                            return float4(final, 1);
                        }
                        ENDCG
                    }
    }
}

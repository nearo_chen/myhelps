﻿Shader "Custom/DeferredSpotLightMap"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
            #pragma multi_compile PERSPECTIVE

            #include "UnityCG.cginc"

            //float4 halfPixel;
            //float4x4 invVP;
            static const int SLightPerPass = 64;
            int SpotLightAmount;
            float4 SpotLightColor[SLightPerPass];
            float4 SpotLightPosition[SLightPerPass];
            float4 SpotLightDirection[SLightPerPass];
            float4 SLightThetaPhiStrengthRange[SLightPerPass];

			sampler2D _MainLightMap, _CameraDepthNormal;

           // https://forum.unity.com/threads/recreating-world-position-from-depth-algorithm.263435/
            float4x4 _ViewToWorld;
            float4 _FrustrumPoints;
            float _CameraFar;
            // 1. Take the screen space position uv and find where it lies on the rectangle on the far clip plane. This is a ray from the camera to the far clip plane.
            // 2. Then we multiply the z position by the depth to find where that point lies on a ray. We have to multiple the x and y coordinates by depth if it's perspective.
            // The _FrustrumPoints are calculated in the code
            float3 CalcVS(float2 screenPos, float depth)
            {
                float2 lerpVal = screenPos;
                float3 ray = float3(
                    _FrustrumPoints.x * lerpVal.x + _FrustrumPoints.y * (1 - lerpVal.x),
                    _FrustrumPoints.z * lerpVal.y + _FrustrumPoints.w * (1 - lerpVal.y),
                    _CameraFar); // I think _ProjectionParams.z will also work

            #if defined (PERSPECTIVE)
                float3 posVS = float3(ray.xy * -depth, ray.z * -depth);
            #else
                float3 posVS = float3(ray.xy, ray.z * -depth);
            #endif
                return posVS;
            }

            float CalcDegree(float3 v1, float3 v2)
            {
                //https://fredxxx123.wordpress.com/2009/03/03/兩向量的夾角角度/
                //A dot B = | A| |B | cos(θ)
                //θ = Acos((A dot B) / (| A| |B | ))
                float radian = acos(dot(v2,v1 ));//|A| |B| is 1
                    return degrees(radian)*2;
            }

            float4 frag(v2f_img i) : SV_Target
            {
                float4 depthNormal = tex2D(_CameraDepthNormal, float2(1 - i.uv.x, 1 - i.uv.y));
                float cameraDepth = depthNormal.r;
                float3 worldSpaceNormal = depthNormal.gba;
                float2 TexCoord = i.uv;
                float3 positionVS = CalcVS(TexCoord, cameraDepth);

                // Convert to worldSpace
                float4 position = mul(_ViewToWorld, float4(positionVS, 1));

                float3 final = 0;
                for (int a = 0; a<SpotLightAmount; a++)
                {
                    float3 directionToLight = SpotLightPosition[a] - position;
                    float lightDist = length(directionToLight);
                    directionToLight = directionToLight / lightDist;

                    //float coneDot = dot(-directionToLight, SpotLightDirection[a]);
                    float coneDegree = CalcDegree(-directionToLight, SpotLightDirection[a]);

                    float anglePhi = SLightThetaPhiStrengthRange[a].y;
                //    float angleTheta = SLightThetaPhiStrengthRange[a].x;
                    float range = SLightThetaPhiStrengthRange[a].w;
                    float origIntensity = SLightThetaPhiStrengthRange[a].z;                   
                    float coneAttenuation = 0;

                    //越接近內圈數值會與光向dot為1，所以 內圈數值會比外圈大。
                    //if (coneDot > anglePhi)//可以把if 弄掉，內圈的dot一定會比外圈大
                    {   
                       // float baseIntensity = saturate((anglePhi - coneDegree) / anglePhi);
                       // coneAttenuation = pow(baseIntensity, 1);
                        
                        float decay = clamp(30 - origIntensity, 0, 30);
                        float decay01 = decay / 30;
                        float baseIntensity = saturate((anglePhi- coneDegree) / anglePhi);
                        coneAttenuation =
                            baseIntensity* (1 - decay01) * 1 +
                            //   pow(baseIntensity, decay01*8) *
                            origIntensity*pow(baseIntensity, 1.5);
                            
                        coneAttenuation *= pow
                        (
                            saturate
                            (
                            (range - lightDist) / range
                            )
                            , 2
                        );
                        /*
                        float3 toLight = SpotLightPosition[a] - position;
                        float lengthSq = dot(toLight, toLight);
                        lengthSq = max(lengthSq, 0.000001);
                        toLight *= rsqrt(lengthSq);
                        //1.0 / (1.0 + 25.0*r*r)
                        float quadratic_attenuation = 1.0 / (1.0 + 25.0*r*r);
                        float atten = 1.0 / (1.0 + lengthSq * quadratic_attenuation);
                        float rho = max(0, dot(toLight, SpotLightDirection[a]));
                        float unity_LightAtten_x = cos(radians(anglePhi) / 2);
                        float unity_LightAtten_y = 1 / cos(radians(anglePhi) / 4);
                        float spotAtt = (rho - unity_LightAtten_x) * unity_LightAtten_y;
                        atten *= saturate(spotAtt);
                        coneAttenuation = atten;
                        */
                        /*
                        baseIntensity = saturate((range - lightDist) / range);
                        coneAttenuation *= (
                            baseIntensity* (1 - decay01) * 4 +
                            //   pow(baseIntensity, decay01*8) *
                            origIntensity*pow(baseIntensity, 32)
                            );
                            */

                        float diffuseIntensity = saturate(dot(worldSpaceNormal, directionToLight));
                        final += coneAttenuation * SpotLightColor[a].xyz *diffuseIntensity;
                    }
                }

                final.xyz += tex2D(_MainLightMap, float2(i.uv)).xyz;
                return float4(final, 1);
            }
            ENDCG
        }
    }
}

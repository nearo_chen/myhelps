﻿Shader "MyHelp/CopyCameraDepthNormal"
{
    SubShader
    {
        Tags{ "RenderType" = "Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _CameraDepthNormalsTexture;
            float4x4 viewInverse;

            float4 frag(v2f_img i) : SV_Target
            {
                float3 worldSpaceNormal;
                float depthValue;
                float4 color = tex2D(_CameraDepthNormalsTexture, i.uv);
                DecodeDepthNormal(color, depthValue, worldSpaceNormal);

                //view space to world space
                worldSpaceNormal = mul(viewInverse, float4(worldSpaceNormal.xyz, 0)).xyz;
                worldSpaceNormal = normalize(worldSpaceNormal);

                return float4(depthValue, worldSpaceNormal);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}

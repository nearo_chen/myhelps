﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyCameraDepth : MonoBehaviour
{
    Camera renderCamera;
    [HideInInspector]
    public RenderTexture DepthRT;
    public Material CopyCameraDepthMaterial;
    public MeshRenderer debugRender;
    private void Start()
    {
        renderCamera = GetComponent<Camera>();
        renderCamera.depthTextureMode = DepthTextureMode.Depth; ;

        DepthRT = new RenderTexture(renderCamera.pixelWidth, renderCamera.pixelHeight, 0, RenderTextureFormat.RFloat);
        DepthRT.antiAliasing = 1;
        DepthRT.filterMode = FilterMode.Point;
        DepthRT.useMipMap = false;
        DepthRT.anisoLevel = 0;

        if (debugRender != null)
            debugRender.material.mainTexture = DepthRT;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(null, DepthRT, CopyCameraDepthMaterial);//get depth
        Graphics.Blit(source, destination);
    }
    
    public Matrix4x4 GetInvVP()
    {
        //https://forum.unity.com/threads/recreating-world-position-from-depth-algorithm.263435/
        Matrix4x4 viewMat = renderCamera.worldToCameraMatrix;
        //Matrix4x4 projMat = renderCamera.projectionMatrix;
        Matrix4x4 projMat = GL.GetGPUProjectionMatrix(renderCamera.projectionMatrix, false);
        return (viewMat * projMat).inverse;
        //return (projMat * viewMat).inverse;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDIconRenderer : MonoBehaviour
{
    [System.Serializable]
    public class HUDIcon
    {
        public Transform focusObj;
        public Transform iconTran;
        public float depth;
        public float lifeSec = -1;
    }
    public List<HUDIcon> _HUDIcons = new List<HUDIcon>();

    public float BasicDepth = 20;
    Camera _hudCamera;

    private void Start()
    {
        _hudCamera = GetComponent<Camera>();
    }

    private void OnDestroy()
    {
    }

    private void OnPreRender()
    {
        Matrix4x4 viewMatL = _hudCamera.GetStereoViewMatrix(Camera.StereoscopicEye.Left);
        Matrix4x4 viewMatR = _hudCamera.GetStereoViewMatrix(Camera.StereoscopicEye.Right);
        //Vector3 cameraPosition = (viewMatL.inverse.GetColumn(3) + viewMatR.inverse.GetColumn(3)) * 0.5f;
        Vector3 cameraPosition = _hudCamera.transform.position;
        Vector3 cameraDir = _hudCamera.transform.forward;

        List<HUDIcon> removeIconRec = new List<HUDIcon>();

        foreach (HUDIcon icon in _HUDIcons)
        {
            if (icon.lifeSec > 0)
            {
                icon.lifeSec -= Time.deltaTime;
                if (icon.lifeSec <= 0)
                {
                    //RemoveIcon(icon.focusObj);
                    removeIconRec.Add(icon);
                    continue;
                }
            }

            Vector3 onHUDPos, faceCamDir;
            if (_getCamIntersectPos(
                cameraPosition,
                cameraDir,
                icon.focusObj.position,
                BasicDepth + icon.depth,
                out onHUDPos, out faceCamDir))
            {
                icon.iconTran.gameObject.SetActive(true);
                icon.iconTran.transform.position = onHUDPos;
                icon.iconTran.transform.forward = faceCamDir;
            }
            else
            {
                icon.iconTran.gameObject.SetActive(false);
            }
        }

        while(removeIconRec.Count>0)
        {
            HUDIcon remove = removeIconRec[0];
            removeIconRec.RemoveAt(0);
            RemoveIcon(remove.focusObj);            
        }
    }

    static bool _getCamIntersectPos(Vector3 cameraPosition, Vector3 cameraDir,
        Vector3 worldPos, float effectDis, out Vector3 pos, out Vector3 faceCamDir)
    {
        pos = Vector3.zero;

        faceCamDir = cameraPosition - worldPos;
        faceCamDir.Normalize();
        worldPos = worldPos - faceCamDir * 9999f;
        Ray ray = new Ray(worldPos, faceCamDir);
        Plane cameraPlane = new Plane(cameraDir,
            cameraPosition + cameraDir * effectDis);
        float dis;
        if (cameraPlane.Raycast(ray, out dis))
        {
            pos = ray.origin + ray.direction * dis;
            return true;
        }
        return false;
    }

    public HUDIcon AddIcon(Transform focusObj, Transform iconTran, float depth = 0, float lifeSec = -1)
    {
        HUDIcon icon = new HUDIcon();
        icon.focusObj = focusObj;
        icon.depth = depth;
        icon.iconTran = iconTran;
        icon.lifeSec = lifeSec;
        _HUDIcons.Add(icon);
        return icon;
    }

    public void RemoveIcon(Transform focusObj)
    {
        foreach (HUDIcon icon in _HUDIcons)
        {
            if (icon.focusObj == focusObj)
            {
                icon.iconTran.gameObject.SetActive(false);
                _HUDIcons.Remove(icon);
                return;
            }
        }
    }

    public void ClearIcons()
    {
        foreach (HUDIcon icon in _HUDIcons)
        {
            icon.iconTran.gameObject.SetActive(false);
        }
        _HUDIcons.Clear();
    }
}
